<?php
if(!defined('IA')) exit;
?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1" />
    <meta name="robots" content="index, follow" />
    <meta name="author" content="Dat Phan - datphan0110@gmail.com" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <base href="<?php echo $this->site_url; ?>/" />
    <!-- CSS -->
    <link rel="stylesheet" href="<?php echo $this->static_url; ?>/css/base.css" />
</head>
<body>