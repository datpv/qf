<?php if ( ! defined( 'IA' ) ) exit; ?>
<div id="container" class="ltr">
    <h1 id="logo">
        <a href="#">Hoctudau</a>
    </h1>
    <header id="header" class="info">
        <div class="buttons">
        </div>
        <h2 class="notranslate">
            <?php echo __('txt_wrong_answers_of'); ?> <strong><?php echo __('txt_form'); ?>:</strong> <a href="<?php echo $this->url; ?>form/<?php echo $form_uuid; ?>"><?php echo $form_name; ?></a>
        </h2>
        <div class="notranslate">
            
        </div>
    </header>
    <div id="stage" class="clearfix">
        <div id="comments" class="clearfix">
            <h3 class="subject"><?php echo __('txt_cmt_of_wrong_answer'); ?></h3>
            <div>&nbsp;</div>
            <div class="suggestions">
                <ul class="parent">
                    <?php echo $html; ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="hide" id="comment_form_container">
        <div class="comment_wrap">
            <form id="comment_form">
                <textarea id="comment_text" name="comment_text"></textarea>
                <button onclick="addComment(this); return false;" id="submitFormButton" name="submitFormButton"><?php echo __('txt_comment'); ?></button>
            </form>
        </div>
        <div class="wronga_wrap">
            <form id="wronga_form">
                <textarea id="wrongat_text" name="wronga_text"></textarea>
                <button onclick="addWa(this); return false;" id="submitFormButton2" name="submitFormButton2"><?php echo __('txt_add'); ?></button>
            </form>
        </div>
    </div>