<?php
if(!defined('IA')) exit;
?>
<div id="stage">
    <div id="main" class="dg">
        <div class="info">
            <h2>
                <?php echo __('txt_account_manager'); ?> (<?php echo __('txt_total'); ?> <strong><?php echo $total; ?></strong> <?php echo __('txt_accounts'); ?>.)
            </h2>
        </div>
        <form id="search" name="q" class="search" onsubmit="return false;">
            <label>
                Find <?php echo __('txt_activated'); ?> account, <?php echo __('txt_type'); ?>: "A:". Or <?php echo __('txt_type'); ?> "D:" for <?php echo __('txt_disabled'); ?> <?php echo __('txt_account'); ?>: 
            </label>
            <input id="searchBox" class="text" type="search" value="" />
        </form>
        <div class="col2" style="width: 100%;">
            <h3 class="hide" id="searching">
                Filtered Results for :
                <span id="searchTerm">
                </span>
            </h3>
            <h3 class="hide" id="noResults">
                None of your users match your filter.
            </h3>
            <ul class="account_header clearfix">
                <li>
                    <div class="col col-1"><strong><?php echo __('txt_account_detail'); ?></strong></div>
                    <div class="col col-2"><strong><?php echo __('txt_subjects'); ?></strong></div>
                    <div class="col col-3"><strong><?php echo __('txt_other_info'); ?></strong></div>
                    <div class="col col-4"><strong><?php echo __('txt_groups'); ?></strong></div>
                    <div class="col col-5"><strong>&nbsp;</strong></div>
                </li>
            </ul>
            <ul id="groupList-1" class="account_list">
                <?php echo $users; ?>
            </tbody>
            </table>
        </div>
        <?php
        ?>
        <div class="paged">
            <?php echo $paginate; ?>
        </div>
    </div>

</div>
<!--stage-->