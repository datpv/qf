<?php
if(!defined('IA')) exit;
?>
<div id="stage">
    <div class="info">
        <div class="buttons">
            <a class="button positive" href="<?php echo $this->url; ?>build/" title="Create a New Form">
            <img src="template/images/icons/application_form_add.png" alt=""/> New Form Normal!</a>
            <a class="button positive" href="<?php echo $this->url; ?>build&formType=1" title="Create a New Form">
            <img src="template/images/icons/application_form_add.png" alt=""/> New Form Score!</a>
            <a class="button positive" href="<?php echo $this->url; ?>build&formType=3" title="Create a New Form">
            <img src="template/images/icons/application_form_add.png" alt=""/> New Form Nopbai!</a>
            <a class="button positive" href="<?php echo $this->url; ?>build&formType=4" title="Create a New Form">
            <img src="template/images/icons/application_form_add.png" alt=""/> New Form AutoScore!</a>
        </div>
        <!--buttons-->
        <h2>
            Form Manager
        </h2>
        <div class="notranslate">
            O, what men dare do!
        </div>
    </div>
    <!--info-->
    <div id="main" class="forms">
        <form id="" name="" class="search" onsubmit="return false;">
            <label>
                Filter
            </label>
            <input onkeyup="filterBy($F(this));" onsearch="filterBy($F(this));" id="searchBox" class="text" type="search" value="" />
        </form>
        <div class="abuttons">
            <label>
                Form Classify :
            </label>
            <a rel="y" onclick="getApprovedForm(this); return false;" class="approveButton selected">Approved Forms</a> 
            <a rel="n" onclick="getUnApprovedForm(this); return false;" class="approveButton">UnApproved Forms</a>
            </div>
        <div id="sort">
            <label>
                Sort By :
            </label>
            <a id="FormId" class="selected" href="#" onclick="sortBy('FormId'); return false;">Date Created</a>
            <a id="DateUpdated" class="" href="#" onclick="sortBy('DateUpdated'); return false;">Date Edited</a>
            <!--<a class="undone" id="EntryCount" href="#" onclick="sortBy('EntryCount'); return false;">Entries Today</a>
        --></div>
        <div id="stats" style="display:none">
            <table id="graphs" cellspacing="0">
                <tr>
                    <td>
                        <select id="allForms" name="allForms" class="select" onchange="refreshGraph();">
                            <option value="4" class="notranslate">
                                Untitled Form
                            </option>
                        </select>
                    </td>
                    <td>
                        <img src="template/images/paren.gif" alt=""/>
                    </td>
                    <td>
                        <div id="formGraph" class="graph">
                            Loading...
                        </div>
                    </td>
                    <td>
                        <ul id="graphMenu">
                            <li id="todayGraph">
                                <a href="#" onclick="changeType('today'); return false;">Today</a>
                            </li>
                            <li class="selected" id="weekGraph">
                                <a href="#" onclick="changeType('week'); return false;">Week</a>
                            </li>
                            <li id="monthGraph">
                                <a href="#" onclick="changeType('month'); return false;">Month</a>
                            </li>
                            <li id="yearGraph">
                                <a href="#" onclick="changeType('year'); return false;">Year</a>
                            </li>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>
        <div class="group">
            <h3 id="searching" class="hide">
                Filtered Results for :
                <span id="searchTerm">
                </span>
            </h3>
            <h3 id="noResults" class="hide">
                None of your forms match your filter.
            </h3>
            
            <ul id="groupList-1">
            <?php
            if($count != 0) {
                echo $forms;
            } else {
            ?>
                <li class="notice bigMessage">
                    <h2>
                        <a href="<?php echo $this->url; ?>build/"><span class="bigMessageRed">You don't have any forms!</span> <span class="bigMessageGreen">On This Category!</span></a>
                    </h2>
                </li>
            <?php
            }
            ?>
            </ul>
            <?php echo $paginate; ?>
        </div>
        <div class="categories classify">
            <h3>Categories</h3>
            <ul id="category_list">
                <?php echo $categories; ?>
            </ul>
            <div class="forms">
                <form id="category_create">
                    <div>
                        <label for="cat_name">Name:</label> <input type="text" class="text" id="cat_name" />
                    </div>
                </form>
            </div>
        </div>
        <div class="clearfix"></div>
        <!--group-->
    </div>
    <!--main-->
    <div class="footer clear">
        You have
        <strong class="notranslate">
            1000000000000
        </strong>
        forms left.
        <a class="button" href="<?php echo $this->url; ?>account/"><img src="template/images/icons/tinyupgrade.png" /> Degrade for Less</a>
    </div>
    <?php 
    
    ?>
</div>
<!--stage-->