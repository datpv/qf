<?php
if(!defined('IA')) exit;
/**
 * Translate language variable wrapper (for usage in templates and scripts)
 * @param string $str variable to translate
 * @param array $params placeholder replacements
 * @param string $lang_code language code to get variable for
 * @return string variable value
 */
function __($str, $params = array(), $lang_code = SITE_LANGUAGE) {
    global $initiallizer;
    if($lang_code != SITE_LANGUAGE) {
        $lang_code = $initiallizer->getCache('site_language');
        if(empty($lang_code)) $lang_code = SITE_LANGUAGE;
    }
    $values = $initiallizer->getCache('langs|'.$lang_code);
    if(!empty($values[$str])) {
        $str = !empty($values[$str]['msgstr'])?$values[$str]['msgstr']:array($str);
        $str = $str[0];
    }
    if (!empty($params) && is_array($params)) {
        foreach($params as $kp => $param) {
            $str = str_replace($kp, $param, $str);
        }
    }
    return $str;
}
function _e($str, $params = array(), $lang_code = SITE_LANGUAGE) {
    echo __($str, $params, $lang_code);
}