<?php
if(!defined('IA')) exit;
/*
    @Name: Autoloader
    @Description: Autoload all class
    @Docs:
*/
class Autoload {
    private function ALC($class_name) {
        $f = str_replace( '_', DIRECTORY_SEPARATOR, $class_name );
        $pi = pathinfo( $f );
        $p = strtolower( $pi[ 'dirname' ] );
        include_once(SITE_PATH . DIRECTORY_SEPARATOR . $p . DIRECTORY_SEPARATOR . $pi[ 'filename' ] . '.class.php');
    }
    function register() {
        spl_autoload_register(array($this,'ALC'));
    }
}