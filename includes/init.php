<?php
if(!defined('IA')) exit;
/*
    @Name: Initallizer
    @Description: Init the application
    @Docs:
*/
class Initiallizer {
    public static $registry, $init_time;
    function __construct($configs) {
        self::$init_time = microtime(true);
        $this->registerALC();
        $this->init($configs);
    }
    private function registerALC() {
        include(INCLUDE_PATH.'/lib/Autoload.php');
        $autoload = new Autoload();
        $autoload->register();
        // (new Autoload)->register();
    }
    private function init($configs) {
        $registry = new Applications_Registry;
        $registry->site = $this;
        self::$registry = $registry;
        $registry->router = new Applications_Router($registry);
        // Router configs
        $rcs = $registry->router->configRouters();
        if($rcs['do_nothing']) {
            exit('Nothing todo.');
        }
        $registry->caches = array();
        $registry->query_count = 0;
        $registry->configs = $configs;
        $registry->mgqgpc_enabled = get_magic_quotes_gpc();
        if(!$rcs['no_load_db']) {
            $registry->db = new Applications_Model($registry);
        }
        if(!$rcs['no_load_page_methods']) {
            $registry->helpers = new Applications_Helpers($registry);
            $registry->template = new Applications_Template($registry);
            // Handle SEO Url
            if($rcs['is_seo_url']) {
                $urls_model = new Applications_Models_Urls($registry);
                $url = $urls_model->getUrl(array('where' => "`name` = '{$rcs['url_name']}'"));
                if(empty($url['routers'])) {
                    $registry->router->setRouters(array(
                            'controller' => 'Notfound',
                            'action' => 'index',
                        ));
                } else {
                    $registry->router->setRouters($registry->helpers->jd2a($url['routers']));
                }
            }
        }
        include(SITE_PATH.'/includes/global.functions.php');
    }
    public function setCaches($caches) {
        $ocaches = self::$registry->caches;
        $ncaches = array_merge($ocaches,$caches);
        self::$registry->caches = $ncaches;
    }
    public function setCache($index, $value) {
        $caches = self::$registry->caches;
        $caches[$index] = $value;
        $this->setCaches($caches);
    }
    public function getCache($index) {
        if(empty(self::$registry->caches[$index])) {
            $caches = self::$registry->caches;
            $values = '';
            $parts = explode('|', $index);
            if($parts[0] == 'langs') {
                include_once(SITE_PATH.'/includes/lib/PoFilePaser.php');
                $popaser = new PoFilePaser;
                $values = $popaser->read(SITE_PATH.'/includes/langs/'.(isset($parts[1])?$parts[1]:SITE_LANGUAGE).'.po');
            }
            $caches[$index] = $values;
            self::$registry->caches = $caches;
        }
        return self::$registry->caches[$index];
    }
    public function getCaches() {
        return self::$registry->caches;
    }
    public function timeLoad() {
        echo "<br>". (microtime(true) - self::$init_time);
    }
    public function run() {
        self::$registry->router->loader();
    }
}
