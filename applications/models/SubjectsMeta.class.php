<?php
if ( ! defined( 'IA' ) ) exit;
/*
    @Name: Application Metas Model
    @Description:
    @Docs:
*/
class Applications_Models_Metas extends Applications_Model {
    function __construct($registry) {
        $this->setTable('subjects_meta');
        parent::__construct($registry);
    }
    function getMetasRP($qs) {
        return array(
            $this->queryRP($qs),
            $this->getRowsCount(),
        );
    }
    function getMetas($args) {
        return array(
            $this->getEntities($args),
            $this->getRowsCount(),
        );
    }
    function getMeta($args) {
        return $this->getEntity($args);
    }
    function insertMeta($args) {
        return $this->insertEntity($args);
    }
    function updateMeta($args) {
        return $this->updateEntity($args);
    }
    function deleteMeta($args) {
        return $this->deletetEntity($args);
    }
}