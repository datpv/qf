<?php
if ( ! defined( 'IA' ) ) exit;
/*
    @Name: Application Subjects Model
    @Description:
    @Docs:
*/
class Applications_Models_Subjects extends Applications_Model {
    function __construct($registry) {
        $this->setTable('subjects');
        parent::__construct($registry);
    }
    function getSubjectsRP($qs) {
        return array(
            $this->queryRP($qs),
            $this->getRowsCount(),
        );
    }
    function getSubjects($args) {
        return array(
            $this->getEntities($args),
            $this->getRowsCount(),
        );
    }
    function getSubject($args) {
        return $this->getEntity($args);
    }
    function insertSubject($args) {
        return $this->insertEntity($args);
    }
    function updateSubject($args) {
        return $this->updateEntity($args);
    }
    function deleteSubject($args) {
        return $this->deletetEntity($args);
    }
}