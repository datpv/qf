<?php
if ( ! defined( 'IA' ) ) exit;
/*
    @Name: Application FormsLogs Model
    @Description:
    @Docs:
*/
class Applications_Models_FormsLogs extends Applications_Model {
    function __construct($registry) {
        $this->setTable('forms_log');
        parent::__construct($registry);
    }
    function getFLsRP($qs) {
        return array(
            $this->queryRP($qs),
            $this->getRowsCount(),
        );
    }
    function getFLs($args) {
        return array(
            $this->getEntities($args),
            $this->getRowsCount(),
        );
    }
    function getFL($args) {
        return $this->getEntity($args);
    }
    function insertFL($args) {
        return $this->insertEntity($args);
    }
    function updateFL($args) {
        return $this->updateEntity($args);
    }
    function deleteFL($args) {
        return $this->deletetEntity($args);
    }
}