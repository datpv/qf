<?php
if ( ! defined( 'IA' ) ) exit;
/*
    @Name: Application CategoriesForms Model
    @Description:
    @Docs:
*/
class Applications_Models_CategoriesForms extends Applications_Model {
    function __construct($registry) {
        $this->setTable('categories_meta');
        parent::__construct($registry);
    }
    function getCFsRP($qs) {
        return array(
            $this->queryRP($qs),
            $this->getRowsCount(),
        );
    }
    function getCFs($args) {
        return array(
            $this->getEntities($args),
            $this->getRowsCount(),
        );
    }
    function getCF($args) {
        return $this->getEntity($args);
    }
    function insertCF($args) {
        return $this->insertEntity($args);
    }
    function updateCF($args) {
        return $this->updateEntity($args);
    }
    function deleteCF($args) {
        return $this->deletetEntity($args);
    }
}