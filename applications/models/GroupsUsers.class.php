<?php
if ( ! defined( 'IA' ) ) exit;
/*
    @Name: Application GroupsUsers Model
    @Description:
    @Docs:
*/
class Applications_Models_GroupsUsers extends Applications_Model {
    function __construct($registry) {
        $this->setTable('groups_meta');
        parent::__construct($registry);
    }
    function getGUsRP($qs) {
        return array(
            $this->queryRP($qs),
            $this->getRowsCount(),
        );
    }
    function getGUs($args) {
        return array(
            $this->getEntities($args),
            $this->getRowsCount(),
        );
    }
    function getGU($args) {
        return $this->getEntity($args);
    }
    function insertGU($args) {
        return $this->insertEntity($args);
    }
    function updateGU($args) {
        return $this->updateEntity($args);
    }
    function deleteGU($args) {
        return $this->deletetEntity($args);
    }
}