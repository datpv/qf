<?php
if ( ! defined( 'IA' ) ) exit;
/*
    @Name: Application Forms Model
    @Description:
    @Docs:
*/
class Applications_Models_Forms extends Applications_Model {
    private $tbl_categories = 'categories',
            $tbl_categories_forms = 'categories_meta';
    function __construct($registry) {
        $this->setTable('forms');
        parent::__construct($registry);
    }
    function getFormsRP($qs) {
        return array(
            $this->queryRP($qs),
            $this->getRowsCount(),
        );
    }
    function getForms($args) {
        return array(
            $this->getEntities($args),
            $this->getRowsCount(),
        );
    }
    function getForm($args) {
        return $this->getEntity($args);
    }
    function insertForm($args) {
        return $this->insertEntity($args);
    }
    function updateForm($args) {
        return $this->updateEntity($args);
    }
    function deleteForm($args) {
        return $this->deletetEntity($args);
    }
    /**
    * ** PREBUILD QUERIES **
    */

    function getFormInCategory($args) {
        return $this->getFormsRP("SELECT SQL_CALC_FOUND_ROWS * FROM {P}{$this->table} a JOIN {P}{$this->tbl_categories_forms} b ON a.`form_id` = b.`form_id` WHERE b.`category_id` = '{$args['cate_id']}' AND a.`form_approved` = '{$args['approve_type']}' {$args['order']} {$args['dir']} {$args['limit']}");
    }
}