<?php
if ( ! defined( 'IA' ) ) exit;
/*
    @Name: Application SubjectForms Model
    @Description:
    @Docs:
*/
class Applications_Models_SubjectForm extends Applications_Model {
    function __construct($registry) {
        $this->setTable('subject_form');
        parent::__construct($registry);
    }
    function getSFsRP($qs) {
        return array(
            $this->queryRP($qs),
            $this->getRowsCount(),
        );
    }
    function getSFs($args) {
        return array(
            $this->getEntities($args),
            $this->getRowsCount(),
        );
    }
    function getSF($args) {
        return $this->getEntity($args);
    }
    function insertSF($args) {
        return $this->insertEntity($args);
    }
    function updateSF($args) {
        return $this->updateEntity($args);
    }
    function deleteSF($args) {
        return $this->deletetEntity($args);
    }
}