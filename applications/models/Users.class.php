<?php
if ( ! defined( 'IA' ) ) exit;
/*
    @Name: Application Users Model
    @Description: manage users data
    @Docs:
*/
class Applications_Models_Users extends Applications_Model {
    private $tbl_subject_user = 'subject_user',
            $tbl_groups = 'groups',
            $tbl_groups_users = 'groups_meta';
    function __construct($registry) {
        $this->setTable('users');
        parent::__construct($registry);
    }
    function getUsersRP($qs) {
        return array(
            $this->queryRP($qs),
            $this->getRowsCount(),
        );
    }
    function getUsers($args) {
        return array(
            $this->getEntities($args),
            $this->getRowsCount(),
        );
    }
    function getUser($args) {
        return $this->getEntity($args);
    }
    function insertUser($args) {
        return $this->insertEntity($args);
    }
    function updateUser($args) {
        return $this->updateEntity($args);
    }
    function deleteUser($args) {
        return $this->deletetEntity($args);
    }
    /**
    * ** PREBUILD QUERIES **
    */
    function getUsers_countSubjects($limit) {
        return $this->getUsersRP("SELECT SQL_CALC_FOUND_ROWS *, count(b.user_id) as subject_count  FROM `{P}{$this->table}` a LEFT JOIN `{P}{$this->tbl_subject_user}` b ON a.`user_id` = b.`user_id` GROUP BY a.`user_id` LIMIT $limit");
    }
    function getSearchUsers_countSubjects($searchString,$limit) {
        return $this->getUsersRP("SELECT SQL_CALC_FOUND_ROWS *, count(b.user_id) as subject_count  FROM `{P}{$this->table}` a LEFT JOIN `{P}{$this->tbl_subject_user}` b ON a.`user_id` = b.`user_id` WHERE `username` LIKE '%$searchString%' OR `email` LIKE '%$searchString%' OR `fullname` LIKE '%$searchString%' OR `h2d` LIKE '%$searchString%' OR `skype` LIKE '%$searchString%' GROUP BY a.`user_id` LIMIT $limit");
    }
    function getGroups_byUser($user_id) {
        return $this->groups_model->getUsersRP("SELECT SQL_CALC_FOUND_ROWS * FROM `{P}{$this->tbl_groups}` a ,`{P}{$this->tbl_groups_users}` b ON a.`group_id` = b.`group_id` WHERE b.`user_id` = '$user_id'");
    }
}