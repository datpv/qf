<?php
if ( ! defined( 'IA' ) ) exit;
/*
    @Name: Application FormsLogsComments Model
    @Description:
    @Docs:
*/
class Applications_Models_FormsLogsComments extends Applications_Model {
    function __construct($registry) {
        $this->setTable('forms_log_meta');
        parent::__construct($registry);
    }
    function getFLCsRP($qs) {
        return array(
            $this->queryRP($qs),
            $this->getRowsCount(),
        );
    }
    function getFLCs($args) {
        return array(
            $this->getEntities($args),
            $this->getRowsCount(),
        );
    }
    function getFLC($args) {
        return $this->getEntity($args);
    }
    function insertFLC($args) {
        return $this->insertEntity($args);
    }
    function updateFLC($args) {
        return $this->updateEntity($args);
    }
    function deleteFLC($args) {
        return $this->deletetEntity($args);
    }
}