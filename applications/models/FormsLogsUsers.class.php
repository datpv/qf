<?php
if ( ! defined( 'IA' ) ) exit;
/*
    @Name: Application FormsLogsUsers Model
    @Description:
    @Docs:
*/
class Applications_Models_FormsLogsUsers extends Applications_Model {
    function __construct($registry) {
        $this->setTable('forms_log_user');
        parent::__construct($registry);
    }
    function getFLUsRP($qs) {
        return array(
            $this->queryRP($qs),
            $this->getRowsCount(),
        );
    }
    function getFLUs($args) {
        return array(
            $this->getEntities($args),
            $this->getRowsCount(),
        );
    }
    function getFLU($args) {
        return $this->getEntity($args);
    }
    function insertFLU($args) {
        return $this->insertEntity($args);
    }
    function updateFLU($args) {
        return $this->updateEntity($args);
    }
    function deleteFLU($args) {
        return $this->deletetEntity($args);
    }
}