<?php
if ( ! defined( 'IA' ) ) exit;
/*
    @Name: Application Reports Model
    @Description:
    @Docs:
*/
class Applications_Models_Reports extends Applications_Model {
    function __construct($registry) {
        $this->setTable('reports');
        parent::__construct($registry);
    }
    function getReportsRP($qs) {
        return array(
            $this->queryRP($qs),
            $this->getRowsCount(),
        );
    }
    function getReports($args) {
        return array(
            $this->getEntities($args),
            $this->getRowsCount(),
        );
    }
    function getReport($args) {
        return $this->getEntity($args);
    }
    function insertReport($args) {
        return $this->insertEntity($args);
    }
    function updateReport($args) {
        return $this->updateEntity($args);
    }
    function deleteReport($args) {
        return $this->deletetEntity($args);
    }
}