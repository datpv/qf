<?php
if ( ! defined( 'IA' ) ) exit;
/*
    @Name: Application Groups Model
    @Description:
    @Docs:
*/
class Applications_Models_Groups extends Applications_Model {
    function __construct($registry) {
        $this->setTable('groups');
        parent::__construct($registry);
    }
    function getGroupsRP($qs) {
        return array(
            $this->queryRP($qs),
            $this->getRowsCount(),
        );
    }
    function getGroups($args) {
        return array(
            $this->getEntities($args),
            $this->getRowsCount(),
        );
    }
    function getGroup($args) {
        return $this->getEntity($args);
    }
    function insertGroup($args) {
        return $this->insertEntity($args);
    }
    function updateGroup($args) {
        return $this->updateEntity($args);
    }
    function deleteGroup($args) {
        return $this->deletetEntity($args);
    }
}