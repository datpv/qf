<?php
if ( ! defined( 'IA' ) ) exit;
/*
    @Name: Application Comments Model
    @Description:
    @Docs:
*/
class Applications_Models_Comments extends Applications_Model {
    function __construct($registry) {
        $this->setTable('comments');
        parent::__construct($registry);
    }
    function getCommentsRP($qs) {
        return array(
            $this->queryRP($qs),
            $this->getRowsCount(),
        );
    }
    function getComments($args) {
        return array(
            $this->getEntities($args),
            $this->getRowsCount(),
        );
    }
    function getComment($args) {
        return $this->getEntity($args);
    }
    function insertComment($args) {
        return $this->insertEntity($args);
    }
    function updateComment($args) {
        return $this->updateEntity($args);
    }
    function deleteComment($args) {
        return $this->deletetEntity($args);
    }
    function getCommentsOfLog($args) {
        $commented_id = $args['commented_id'];
        $log_id = $args['log_id'];
        $limit = isset($args['limit'])?$args['limit']:'3';
        return $this->getCommentsRP("SELECT SQL_CALC_FOUND_ROWS * FROM {P}comments a JOIN {P}forms_log_meta b ON a.`comment_id` = b.`comment_id` WHERE b.`log_id` = '$log_id' AND b.`comment_id` NOT IN ($commented_id) ORDER BY comment_create ASC LIMIT $limit");
    }
}