<?php
if ( ! defined( 'IA' ) ) exit;
/*
    @Name: Application SubjectUsers Model
    @Description:
    @Docs:
*/
class Applications_Models_SubjectsUsers extends Applications_Model {
    function __construct($registry) {
        $this->setTable('subject_users');
        parent::__construct($registry);
    }
    function getSUsRP($qs) {
        return array(
            $this->queryRP($qs),
            $this->getRowsCount(),
        );
    }
    function getSUs($args) {
        return array(
            $this->getEntities($args),
            $this->getRowsCount(),
        );
    }
    function getSUr($args) {
        return $this->getEntity($args);
    }
    function insertSU($args) {
        return $this->insertEntity($args);
    }
    function updateSU($args) {
        return $this->updateEntity($args);
    }
    function deleteSU($args) {
        return $this->deletetEntity($args);
    }
}