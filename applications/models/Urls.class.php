<?php
if ( ! defined( 'IA' ) ) exit;
/*
    @Name: Application Url Rewrite Model
    @Description: manage url, seo url data
    @Docs:
*/
class Applications_Models_Urls extends Applications_Model {
    function __construct($registry) {
        $this->setTable('urls');
        parent::__construct($registry);
    }
    function getUrlsRP($qs) {
        return array(
            $this->queryRP($qs),
            $this->getRowsCount(),
        );
    }
    function getUrls($args) {
        return array(
            $this->getEntities($args),
            $this->getRowsCount(),
        );
    }
    function getUrl($args) {
        return $this->getEntity($args);
    }
    function insertUrl($args) {
        return $this->insertEntity($args);
    }
    function updateUrl($args) {
        return $this->updateEntity($args);
    }
    function deleteUrl($args) {
        return $this->deletetEntity($args);
    }
}