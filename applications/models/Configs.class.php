<?php
if ( ! defined( 'IA' ) ) exit;
/*
    @Name: Application Configs Model
    @Description: manage site options/configs data
    @Docs:
*/
class Applications_Models_Configs extends Applications_Model {
    function __construct($registry) {
        $this->setTable('configs');
        parent::__construct($registry);
    }
    function getConfigsRP($qs) {
        return array(
            $this->queryRP($qs),
            $this->getRowsCount(),
        );
    }
    function getConfigs($args) {
        return array(
            $this->getEntities($args),
            $this->getRowsCount(),
        );
    }
    function getConfig($args) {
        return $this->getEntity($args);
    }
    function insertConfig($args) {
        return $this->insertEntity($args);
    }
    function updateConfig($args) {
        return $this->updateEntity($args);
    }
    function deleteConfig($args) {
        return $this->deletetEntity($args);
    }
}