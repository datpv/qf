<?php
if ( ! defined( 'IA' ) ) exit;
/*
    @Name: Application Themes Model
    @Description:
    @Docs:
*/
class Applications_Models_Themes extends Applications_Model {
    function __construct($registry) {
        $this->setTable('themes');
        parent::__construct($registry);
    }
    function getThemesRP($qs) {
        return array(
            $this->queryRP($qs),
            $this->getRowsCount(),
        );
    }
    function getThemes($args) {
        return array(
            $this->getEntities($args),
            $this->getRowsCount(),
        );
    }
    function getTheme($args) {
        return $this->getEntity($args);
    }
    function insertTheme($args) {
        return $this->insertEntity($args);
    }
    function updateTheme($args) {
        return $this->updateEntity($args);
    }
    function deleteTheme($args) {
        return $this->deletetEntity($args);
    }
}