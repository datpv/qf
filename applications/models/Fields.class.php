<?php
if ( ! defined( 'IA' ) ) exit;
/*
    @Name: Application Fields Model
    @Description:
    @Docs:
*/
class Applications_Models_Fields extends Applications_Model {
    function __construct($registry) {
        $this->setTable('fields');
        parent::__construct($registry);
    }
    function getFieldsRP($qs) {
        return array(
            $this->queryRP($qs),
            $this->getRowsCount(),
        );
    }
    function getFields($args) {
        return array(
            $this->getEntities($args),
            $this->getRowsCount(),
        );
    }
    function getField($args) {
        return $this->getEntity($args);
    }
    function insertField($args) {
        return $this->insertEntity($args);
    }
    function updateField($args) {
        return $this->updateEntity($args);
    }
    function deleteField($args) {
        return $this->deletetEntity($args);
    }
}