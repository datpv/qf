<?php
if ( ! defined( 'IA' ) ) exit;
/*
    @Name: Application Categories Model
    @Description:
    @Docs:
*/
class Applications_Models_Categories extends Applications_Model {
    function __construct($registry) {
        $this->setTable('categories');
        parent::__construct($registry);
    }
    function getCategoriesRP($qs) {
        return array(
            $this->queryRP($qs),
            $this->getRowsCount(),
        );
    }
    function getCategories($args) {
        return array(
            $this->getEntities($args),
            $this->getRowsCount(),
        );
    }
    function getCategory($args) {
        return $this->getEntity($args);
    }
    function insertCategory($args) {
        return $this->insertEntity($args);
    }
    function updateCategory($args) {
        return $this->updateEntity($args);
    }
    function deleteCategory($args) {
        return $this->deletetEntity($args);
    }
}