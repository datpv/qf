<?php
if ( ! defined( 'IA' ) ) exit;
/*
    @Name: Application EntriesForms Model
    @Description:
    @Docs:
*/
class Applications_Models_EntriesForms extends Applications_Model {
    function __construct($registry) {
        $this->setTable('entries_meta');
        parent::__construct($registry);
    }
    function getEFsRP($qs) {
        return array(
            $this->queryRP($qs),
            $this->getRowsCount(),
        );
    }
    function getEFs($args) {
        return array(
            $this->getEntities($args),
            $this->getRowsCount(),
        );
    }
    function getEF($args) {
        return $this->getEntity($args);
    }
    function insertEF($args) {
        return $this->insertEntity($args);
    }
    function updateEF($args) {
        return $this->updateEntity($args);
    }
    function deleteEF($args) {
        return $this->deletetEntity($args);
    }
}