<?php
if ( ! defined( 'IA' ) ) exit;
/*
    @Name: Application Entries Model
    @Description:
    @Docs:
*/
class Applications_Models_Entries extends Applications_Model {
    private 
        $tbl_entries_forms = 'entries_meta';
    function __construct($registry) {
        $this->setTable('entries');
        parent::__construct($registry);
    }
    function getEntriesRP($qs) {
        return array(
            $this->queryRP($qs),
            $this->getRowsCount(),
        );
    }
    function getEntries($args) {
        return array(
            $this->getEntities($args),
            $this->getRowsCount(),
        );
    }
    function getEntry($args) {
        return $this->getEntity($args);
    }
    function insertEntry($args) {
        return $this->insertEntity($args);
    }
    function updateEntry($args) {
        return $this->updateEntity($args);
    }
    function deleteEntry($args) {
        return $this->deletetEntity($args);
    }
    /**
    * ** PREBUILD QUERIES **
    */

    function getFullEntries($args) {
        return $this->getEntriesRP("SELECT SQL_CALC_FOUND_ROWS * FROM {P}{$this->tbl_entries_forms} a, {P}{$this->table} b WHERE a.`form_id` = '{$args['form_id']}' AND a.`entry_id` = b.`entry_id` ORDER BY b.`entry_id` DESC LIMIT {$args['limit']}");
    }
    function userSubmited($user_id, $form_id) {
        return $this->queryRP("SELECT * FROM {P}entries_meta a, {P}entries b ON a.`entry_id` = b.`entry_id` WHERE b.`entry_user` = '$user_id' AND b.`entry_type` = 'BT' AND a.`form_id` = '$form_id' LIMIT 1");
    }
    function userScored($user_id, $form_id) {
        return $this->queryRP("SELECT * FROM {P}entries_meta a, {P}entries b ON a.`entry_id` = b.`entry_id` WHERE b.`entry_user` = '$user_id' AND b.`entry_type` = 'AS' AND a.`form_id` = '$form_id' LIMIT 1");
    }
    function entryOnce($form_id) {
        return $this->queryRP("SELECT * FROM `{P}entries` a, `{P}entries_meta` b ON a.`entry_id` = b.`entry_id` WHERE b.`form_id` = '$form_id' LIMIT 1");
    }
    function entriesInForm($form_id) {
        return $this->queryRP("SELECT * FROM {P}entries_meta a, {P}entries b WHERE a.`form_id` = '$form_id' AND a.`entry_id` = b.`entry_id` ORDER BY b.`entry_id` DESC");
    }
}