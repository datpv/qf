<?php
if ( ! defined( 'IA' ) ) exit;
/*
    @Name: Application Helpers
    @Description: all helper functions are here
    @Docs:
*/
class Applications_Helpers extends Applications_Application {
    function __construct($registry) {
        parent::__construct($registry);
    }
    /*
        @Name: String replace first
        @Description: Replace first occured sub string of a string
        @Docs:
    */
    function str_replace_first($search, $replace, $subject) {
        return implode($replace, explode($search, $subject, 2));
    }
    /*
        @Name: String cleaner
        @Description: clean a utf8 string to a latin string
        @Docs:
    */
    function str_clean($str, $d = '-') {
        $result = '';
        $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
        $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
        $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
        $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
        $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
        $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
        $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
        $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
        $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
        $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
        $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
        $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
        $str = preg_replace("/(đ)/", 'd', $str);
        $str = preg_replace("/(Đ)/", 'D', $str);
        if (!empty($str)) {
            $str = strtr($str, array("\xc5\xA5" => 't', "\xc4\xBE" => 'l', "\xc5\xA4" => 'T', "\xc4\xBD" => 'L', "\xc4\x84" => 'A', "\xc4\x85" => 'a', "\xc3\xa1" => 'a', "\xc3\x81" => 'A', "\xc3\xa0" => 'a', "\xc3\x80" => 'A', "\xc3\xa2" => 'a', "\xc3\x82" => 'A', "\xc3\xa3" => 'a', "\xc3\x83" => 'A', "\xc2\xaa" => 'a', "\xc4\x8c" => 'C', "\xc4\x8d" => 'c', "\xc3\xa7" => 'c', "\xc3\x87" => 'C', "\xc3\xa9" => 'e', "\xc3\x89" => 'E', "\xc3\xa8" => 'e', "\xc3\x88" => 'E', "\xc3\xaa" => 'e', "\xc3\x8a" => 'E', "\xc3\xab" => 'e', "\xc3\x8b" =>'E', "\xc4\x98" => 'E', "\xc4\x99" => 'e', "\xc4\x9a" => 'E', "\xc4\x9b" => 'e', "\xc4\x8f" => 'd', "\xc3\xad" => 'i', "\xc3\x8d" => 'I', "\xc3\xac" => 'i', "\xc3\x8c" => 'I', "\xc3\xae" => 'i', "\xc3\x8e" => 'I', "\xc3\xaf" => 'i', "\xc3\x8f" => 'I', "\xc4\xb9" => 'L', "\xc4\xba" => 'l', "\xc4\xbe" => 'l', "\xc5\x87" => 'N', "\xc5\x88" => 'n', "\xc3\xb1" => 'n', "\xc3\x91" => 'N', "\xc3\xb3" => 'o', "\xc3\x93" => 'O', "\xc3\xb2" => 'o', "\xc3\x92" => 'O', "\xc3\xb4" => 'o', "\xc3\x94" => 'O', "\xc3\xb5" => 'o', "\xc3\x95" => 'O', "\xd4\xa5" => 'o', "\xc3\x98" => 'O', "\xc2\xba" => 'o', "\xc3\xb0" => 'o', "\xc5\x94" => 'R', "\xc5\x95" => 'r', "\xc5\x98" => 'R', "\xc5\x99" => 'r', "\xc5\xa0" => 'S', "\xc5\xa1" => 's', "\xc5\xa5" => 't', "\xc3\xba" => 'u', "\xc3\x9a" => 'U', "\xc3\xb9" => 'u', "\xc3\x99" => 'U', "\xc3\xbb" => 'u', "\xc3\x9b" => 'U', "\xc3\xbc" => 'u', "\xc3\x9c" => 'U', "\xc5\xae" => 'U', "\xc5\xaf" => 'u', "\xc3\xbd" => 'y', "\xc3\x9d" => 'Y', "\xc3\xbf" => 'y', "\xc3\xa4" => 'a', "\xc3\x84" => 'A', "\xc3\x9f" => 's', "\xc5\xbd" => 'Z', "\xc5\xbe" => 'z', "\xc3\xa5" => 'aa', "\xc3\xa6" => 'ae', "\xc3\xb6" => 'oe', "\xc3\x85" => 'aa', "\xc3\x86" => 'ae', "\xc3\x96" => 'oe', '?' => $d, ' ' => $d, '/' => $d, '\\' => $d, '&' => $d, '(' => $d, ')' => $d, '[' => $d, ']' => $d, '%' => $d, '#' => $d, ',' => $d, ':' => $d, '\'' => $d, '"' => $d));
            $str = strtolower($str); // only lower letters
            $str = preg_replace("/[^a-z0-9-\._]/", '', $str); // URL can contain latin letters, numbers, $d and points only
            $str = preg_replace("/($d){2,}/", $d, $str); // replace double (and more) $d with one dash
            $result = trim($str, $d); // remove trailing $d if exist
        }
        return $result;
    }
    /*
        @Name: String escape
        @Description: escape all string of a single string or array, object
        @Docs:
    */
    function str_escape($input) {
        $input = $this->o2a($input);
        if(is_array($input)) {
            foreach($input as $key => $v) {
                if(is_array($input[$key])) {
                    $input[$key] = $this->str_escape($input[$key]);
                } elseif(is_string($v)) {
                    $input[$key] = $this->mysqli->real_escape_string(trim($v));
                }
            }
        } elseif(is_string($input)) {
            $input = $this->mysqli->real_escape_string(trim($input));
        }
        return $input;
    }
    /*
        @Name: String unescape
        @Description: unescape all string of a single string or array, object
        @Docs:
    */
    function str_unescape($input) {
        $input = $this->o2a($input);
        if(is_array($input)) {
            foreach($input as $key => $v) {
                if(is_array($input[$key])) {
                    $input[$key] = $this->str_unescape($input[$key]);
                } elseif(is_string($v)) {
                    $input[$key] = stripslashes(trim($v));
                } 
            }
        } elseif(is_string($input)) {
            $input = stripslashes(trim($input));
        }
        return $input;
    }
    /*
        @Name: String trim
        @Description: trim all string of a single string or array, object
        @Docs:
    */
    function str_trim($input, $trim_what = ' ') {
        $input = $this->o2a($input);
        if(is_array($input)) {
            foreach($input as $key => $v) {
                if(is_array($input[$key])) {
                    $input[$key] = $this->str_trim($input[$key]);
                } elseif(is_string($v)) {
                    $input[$key] = preg_replace('/\s+/', $trim_what, trim($v));
                } 
            }
        } elseif(is_string($input)) {
            $input = preg_replace('/\s+/', $trim_what, trim($input));
        }
        return $input;
    }
    /*
        @Name: String truncate
        @Description: truncate a string by given length
        @Docs:
            $text: text to be truncate
            $length:
            $deli: delimeter at the end of truncated text
            $num: number of tail of text
    */
    function str_truncate($text, $length, $deli = '...', $num = 4) {
        $l = strlen($text);
        if($l <= $length) return $text;
        return substr($text,0,$length) . $deli . substr($text,-$num);
    }
    /*
        @Name: String starts with
        @Description: is the string starts with $search?
        @Docs:
    */
    function str_startsWith($str, $search) {
        return !strncmp($str, $search, strlen($search));
    }
    /*
        @Name: String ends with
        @Description: is the string ends with $search?
        @Docs:
    */
    function str_endsWith($str, $search) {
        $length = strlen($search);
        if ($length == 0) {
            return true;
        }
        return (substr($str, -$length) === $search);
    }
    /*
        @Name: String Is Valid
        @Description: is the string contain valid utf8 chars
        @Docs:
    */
    function str_isValid($str) {
        return $this->str_str2valid($str) == $str;
    }
    /*
        @Name: String To Valid Utf8
        @Description: is the string contain valid utf8 chars
        @Docs:
    */
    function str_str2valid($str) {
        return preg_replace('/[\x00-\x08\x0B\x0C\x0E-\x1F\x80-\x9F]/u','', $str);
    }
    /*
        @Name: String Encrypt
        @Description: base64_encode with key
        @Docs:
    */
    function encrypt($str, $key) {
        $result = '';
        for($i=0; $i<strlen($str); $i++) {
            $char = substr($str, $i, 1);
            $keychar = substr($key, ($i % strlen($key))-1, 1);
            $char = chr(ord($char)+ord($keychar));
            $result.=$char;
        }
        return base64_encode($result);
    }
    /*
        @Name: String Decrypt
        @Description: base64_decode with key
        @Docs:
    */
    function decrypt($str, $key) {
        $result = '';
        $str = base64_decode($str);
        for($i=0; $i<strlen($str); $i++) {
            $char = substr($str, $i, 1);
            $keychar = substr($key, ($i % strlen($key))-1, 1);
            $char = chr(ord($char)-ord($keychar));
            $result .= $char;
        }
        return $result;
    }
    /*
        @Name: String Is Uft8
        @Description: 
        @Docs:
    */
    function str_isUTF8($str) {
        return (utf8_encode(utf8_decode($str)) == $str);
    }
    function str_isLatin($str) {
        return $str == preg_replace('/[^\00-\255]+/u', '', $str);
    }
    /*
        @Name: String Detect link
        @Description: 
        @Docs:
    */
    function str_detectLink($text,$length = 30) {
        if(!is_string($text)) return $text;
        $text = ' '.trim($text);
        $reg_exUrl = "/([^\"])(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
        preg_match_all($reg_exUrl, $text, $matches);
        $usedPatterns = array();
        $done = false;
        foreach($matches[0] as $pattern){
            if(!array_key_exists($pattern, $usedPatterns)){
                $usedPatterns[$pattern]=true;
                $t = $this->str_truncate($pattern,$length);            
                $text = str_replace($pattern, "<a title=\"$pattern\" href=\"$pattern\" rel=\"nofollow\" target=\"blank\">$t</a>", $text);
                $done = true;
            }
        }
        return $text;
    }
    /*
        @Name: String explode to array
        @Description:
        @Docs:
    */
    function str_ex2a($input, $d = '|') {
        $arr = explode($d, trim($input));
        return array_map('trim',$arr);
    }
    /*
        @Name: UUID Generator
        @Description: generate a unique string
        @Docs:
        @AuthorNote:
            * @brief Generates a Universally Unique IDentifier, version 4.
            *
            * This function generates a truly random UUID. The built in CakePHP String::uuid() function
            * is not cryptographically secure. You should uses this function instead.
            *
            * @see http://tools.ietf.org/html/rfc4122#section-4.4
            * @see http://en.wikipedia.org/wiki/UUID
            * @return string A UUID, made up of 32 hex digits and 4 hyphens.
    */
    function get_uuid() {
        $pr_bits = null;
        $fp = @fopen('/dev/urandom','rb');
        if ($fp !== false) {
            $pr_bits .= @fread($fp, 16);
            @fclose($fp);
        } else {
            // If /dev/urandom isn't available (eg: in non-unix systems), use mt_rand().
            $pr_bits = "";
            for($cnt=0; $cnt < 16; $cnt++){
                $pr_bits .= chr(mt_rand(0, 255));
            }
        }
        $time_low = bin2hex(substr($pr_bits,0, 4));
        $time_mid = bin2hex(substr($pr_bits,4, 2));
        $time_hi_and_version = bin2hex(substr($pr_bits,6, 2));
        $clock_seq_hi_and_reserved = bin2hex(substr($pr_bits,8, 2));
        $node = bin2hex(substr($pr_bits,10, 6));
        /**
            * Set the four most significant bits (bits 12 through 15) of the
            * time_hi_and_version field to the 4-bit version number from
            * Section 4.1.3.
            * @see http://tools.ietf.org/html/rfc4122#section-4.1.3
        */
        $time_hi_and_version = hexdec($time_hi_and_version);
        $time_hi_and_version = $time_hi_and_version >> 4;
        $time_hi_and_version = $time_hi_and_version | 0x4000;
        /**
            * Set the two most significant bits (bits 6 and 7) of the
            * clock_seq_hi_and_reserved to zero and one, respectively.
        */
        $clock_seq_hi_and_reserved = hexdec($clock_seq_hi_and_reserved);
        $clock_seq_hi_and_reserved = $clock_seq_hi_and_reserved >> 2;
        $clock_seq_hi_and_reserved = $clock_seq_hi_and_reserved | 0x8000;
        return sprintf('%08s%04s%04x%04x%012s', 
            $time_low, $time_mid, $time_hi_and_version, $clock_seq_hi_and_reserved, $node);
    }
    /*
        @Name: Get datetime
        @Description: 
        @Docs:
    */
    function get_datetime() {
        return date( 'Y-m-d H:i:s' );
    }
    /*
        @Name: Get current page url
        @Description: 
        @Docs:
    */
    function get_currentPageUrl() {
        $pageURL = 'http';
        $pageURL .= "://";
        if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
        } else {
        $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
        }
        return $pageURL;
    }
    /*
        @Name: Get Gavatar
        @Description: get Gavatar from email
        @Docs:
    */
    function get_gravatar( $email, $s = 80, $d = 'mm', $r = 'g', $img = false, $atts = array() ) {
        $url = 'http://www.gravatar.com/avatar/';
        $url .= md5( strtolower( trim( $email ) ) );
        $url .= "?s=$s&d=$d&r=$r";
        if ( $img ) {
            $url = '<img src="' . $url . '"';
            foreach ( $atts as $key => $val )
                $url .= ' ' . $key . '="' . $val . '"';
            $url .= ' />';
        }
        return $url;
    }
    /*
        @Name: Get Key Count
        @Description: get Gavatar from email
        @Docs:
    */
    function get_key_count($array, $mixed = 'Field') {
        $i = 0;
        foreach($array as $kp => $post) {
            if(strpos($kp, $mixed) !== false) {
                $i++;
            }
        }
        return $i;
    }
    /*
        @Name: Validate Signup
        @Description: check signup infomations
        @Docs:
    */
    function validate_signup($post) {
        $errorFields = array();
        if(!empty($post['username'])) {
            if(!$this->validate_username($post['username'])) {
                $errorFields['username'] = array(
                    'message' => 'Please enter a valid username.'
                );
            }
        } else {
            $errorFields['username'] = array(
                'message' => 'Please enter a username.'
            );
        }
        if(!empty($post['email'])) {
            if(!$this->validate_email($post['email'])) {
                $errorFields['email'] = array(
                    'message' => 'Please enter a valid email address.'
                );
            }
        } else {
            $errorFields['email'] = array(
                'message' => 'Please enter a email address.'
            );
        }
        if(!empty($post['password']) || !empty($post['password1'])) {
            if(!$this->validate_password($post['password'],$post['password1'])) {
                $errorFields['password'] = array(
                    'message' => 'Please enter a valid password.'
                );
            }
        } else {
            $errorFields['password'] = array(
                'message' => 'Please enter passwords.'
            );
        }
        if(!empty($post['fullname'])) {
            if(!$this->validate_fullname($post['fullname'])) {
                $errorFields['fullname'] = array(
                    'message' => 'Please enter a valid name.'
                );
            }
        } else {
            $errorFields['fullname'] = array(
                'message' => 'Please enter a name.'
            );
        }
        if(!empty($post['nickh2d'])) {
            if(!$this->str_isValid($post['nickh2d'])) {
                $errorFields['nickh2d'] = array(
                    'message' => 'Please enter a valid nickh2d.'
                );
            }
        } else {
            $errorFields['nickh2d'] = array(
                'message' => 'Please enter nickh2d.'
            );
        }
        if(!empty($post['nickskype'])) {
            if(!$this->str_isValid($post['nickskype'])) {
                $errorFields['nickskype'] = array(
                    'message' => 'Please enter a valid nickskype.'
                );
            }
        } else {
            $errorFields['nickskype'] = array(
                'message' => 'Please enter nickskype.'
            );
        }
        return $errorFields;
    }
    /*
        @Name: Validate Fullname
        @Description: length > 4
        @Docs:
    */
    function validate_fullname($string) {
        $string = trim($string);
        return is_string($string) && strlen($string) > 4;
    }
    /*
        @Name: Validate Username
        @Description: length > 4, no space allowed
        @Docs:
    */
    function validate_username($string) {
        if(strpos($string, ' ') !== false)  
            return false;
        return $this->str_isUTF8($string) && str_isLatin($string) && strlen($string) > 4 && strlen($string) < 50;
    }
    /*
        @Name: Validate Password
        @Description:
        @Docs:
    */
    function validate_password($pass1, $pass2) {
        if(strpos($pass1, ' ') !== false)  
            return false;
        return $pass1 == $pass2 && strlen($pass1) > 5 && strlen($pass1) < 50;  
    }
    /*
        @Name: Validate Email
        @Description:
        @Docs:
    */
    function validate_email($email) {
        if (!preg_match('/([a-zA-Z0-9_-]+)(\@)([a-zA-Z0-9_-]+)(\.)([a-zA-Z0-9]{2,4})(\.[a-zA-Z0-9]{2,4})?/',$email)) return false; return true;
    }
    /*
        @Name: Validate Url
        @Description:
        @Docs:
    */
    function validate_url($url) {
        if(filter_var($url, FILTER_VALIDATE_URL) === FALSE) return false; return true;
    }
    /*
        @Name: Validate Date
        @Description:
        @Docs:
    */
    function validate_date($date,$type) {
        $date = rtrim($date, '/');
        $ds = explode('/',$date);
        $m = ($type == 'eurodate')?(isset($ds[1])?$ds[1]:''):(isset($ds[0])?$ds[0]:'');
        $d = ($type == 'eurodate')?(isset($ds[0])?$ds[0]:''):(isset($ds[1])?$ds[1]:'');
        $y = (isset($ds[2])?$ds[2]:'');
        if(!is_numeric($m) || !is_numeric($d) || !is_numeric($y)) return false;
        return checkdate($m,$d,$y);
    }
    /*
        @Name: Validate Time
        @Description:
        @Docs:
    */
    function validate_time($time) {
        if(preg_match('/(0?\d|1[0-2]):(0\d|[0-5]\d):(0\d|[0-5]\d) (AM|PM)/i',$time, $matches))
        return $matches[0] == $time;
    }
    /*
        @Name: AASORT
        @Description: sort an array by its key
        @Docs:
    */
    function aasort ($array, $key) {
        $sorter=array();
        $ret=array();
        reset($array);
        foreach ($array as $ii => $va) {
            $sorter[$ii] = $va[$key];
        }
        asort($sorter);
        foreach ($sorter as $ii => $va) {
            $ret[$ii]=$array[$ii];
        }
        return $ret;
    }
    /*
        @Name: Object to array
        @Description: convert object to associate array
        @Docs:
    */
    function o2a($d) {
        if (is_object($d)) {
            $d = get_object_vars($d);
        }
        if (is_array($d)) {
            return array_map(array($this,'o2a'), $d);
        }
        else {
            return $d;
        }
    }
    /*
        @Name: Json encode and escape
        @Description: encode php array and return escaped json string
        @Docs:
    */
    function jeae($arr) {
        return $this->mysqli->real_escape_string(json_encode($arr));
    }
    /*
        @Name: Json decode to array
        @Description: decode json string and return php array
        @Docs:
    */
    function jd2a($str) {
        $jda = $this->o2a(json_decode($str));
        if(!empty($str) && empty($jda)) {
            $jda = $this->o2a(json_decode($this->jd_str_handle($str)));
        }
        return !empty($jda)?$jda:$str;
    }
    /*
        @Name: Json decode string handle
        @Description: use by jd2a if magic quoutes gpc is enabled
        @Docs:
    */
    function jd_str_handle($str) {
        $str = str_replace('{\"','{"',$str);
        $str = str_replace('\"}','"}',$str);
        $str = str_replace('\":','":',$str);
        $str = str_replace(':\"',':"',$str);
        $str = str_replace('\",','",',$str);
        $str = str_replace(',\"',',"',$str);
        $str = str_replace('\\\"','"',$str);
        $str = str_replace('\\\n','\n',$str);
        $str = preg_replace('#(\\\){2,}#','',$str);
        $str = str_replace("\'","'",$str);
        return $str;
    }
    /*
        @Name: Set Cookie
        @Description:
        @Docs:
    */
    function setCookie($name, $value = '', $expire = 0) {
        $url = $this->url;
        if ($url[strlen($url)-1] != '/') $url .= '/';
        $secure = false;
        if(isset($_SERVER['HTTPS'])) $secure = (($_SERVER['HTTPS'] == 'on' OR $_SERVER['HTTPS'] == '1') ? true : false);

        $p = parse_url($url);

        $path = !empty($p['path']) ? $p['path'] : '/';
        $domain = $p['host'];
        if (substr_count($domain, '.') > 1) {
            while (substr_count($domain, '.') > 1) {
                $pos = strpos($domain, '.');
                $domain = substr($domain, $pos + 1);
            }
            
        } else $domain = '';
        setcookie($name, $value, $expire, $path, $domain, $secure);
    }
    /*
        @Name: Remove ALl Cookie
        @Description:
        @Docs:
    */
    function unsetCookies() {
        if (isset($_SERVER['HTTP_COOKIE'])) {
            $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
            if(!empty($cookies))
            foreach($cookies as $cookie) {
                $parts = explode('=', $cookie);
                $name = trim($parts[0]);
                setcookie($name, '', time()-1000);
                setcookie($name, '', time()-1000, '/');
            }
        }
    }
    /*
        @Name: Google Captcha Checker
        @Description:
        @Docs:
            recaptcha_check_answer ($privatekey,
                                    $_SERVER["REMOTE_ADDR"],
                                    $_POST["recaptcha_challenge_field"],
                                    $_POST["recaptcha_response_field"]);
    */
    function isValidCaptcha($challenge, $response) {
        require_once(INCLUDE_PATH.'/lib/recaptchalib.php');
        $privatekey = "6LfNO9wSAAAAAC6Khm2xwF1iTKY2OzwWPpRj3yI8";
        $resp = recaptcha_check_answer ($privatekey,
                                    $_SERVER["REMOTE_ADDR"],
                                    $challenge,
                                    $response);
        return $resp->is_valid;
    }
    /*
        @Name: Send Email
        @Description:
        @Docs:
    */
    function sendMail($subject, $html = '', $address = array(), $replyTo = '', $options = array()) {
        require_once(INCLUDE_PATH.'/lib/PHPMailer/class.phpmailer.php');
        $smtp = $this->registry->configs['smtp'];
        $mail             = new PHPMailer();
        $body             = $html;
        $mail->IsSMTP(); // telling the class to use SMTP
        $mail->SMTPSecure = 'tls';
        //$mail->Host       = $smtp['host']; // SMTP server
        $mail->SMTPDebug  = 1;                     // enables SMTP debug information (for testing)
                                                   // 1 = errors and messages
                                                   // 2 = messages only
        $mail->SMTPAuth   = true;                  // enable SMTP authentication
        $mail->Host       = $smtp['host']; // sets the SMTP server
        $mail->Port       = $smtp['port']; // set the SMTP port for the GMAIL server
        $mail->Username   = $smtp['username']; // SMTP account username
        $mail->Password   = $smtp['password']; // SMTP account password
        
        //$mail->SetFrom('admin@luuledmy.vn', $options[0]);
        $mail->SetFrom($smtp['username'], $options[0]);
        if(!empty($replyTo)) $mail->AddReplyTo($replyTo,$options[0]);
        
        $mail->Subject    = $subject;
        
        $mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
        
        $mail->MsgHTML($body);
        foreach($address as $addr => $name) {
            $mail->AddAddress($addr, $name); 
        }
        
        
        //$mail->AddAttachment("images/phpmailer.gif");      // attachment
        //$mail->AddAttachment("images/phpmailer_mini.gif"); // attachment
        $mail->IsHTML(true); // send as HTML
        $a = true;
        if(!$mail->Send()) {
          echo "Mailer Error: " . $mail->ErrorInfo;
          $a = false;
        } else {
          echo "Message sent!";
        }
        $mail->close();
        return $a;
    }
}