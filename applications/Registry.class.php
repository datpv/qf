<?php
if ( ! defined( 'IA' ) ) exit;
/*
    @Name: Application Registry
    @Description: store all variable
    @Docs:
*/
class Applications_Registry {
    private $_vars = array();
    function __set($index, $val) {
        $this->_vars[$index] = $val;
    }
    function __get($index) {
        return isset( $this->_vars[$index] ) ? $this->_vars[$index] : null;
    }
    function clear($index) {
        unset($this->_vars[$index]);
    }
    function clearAll() {
        $this->_vars = array();
    }
}