<?php
if ( ! defined( 'IA' ) ) exit;
/*
    @Name: Application Files Controller
    @Description: manage dowload files, files render, file redirection
    @Docs:
*/
class Applications_Controllers_Files extends Applications_Controller {
    function __construct($registry) {
        //parent::__construct($registry);
    }
    function initiallize() {
        //parent::initiallize();
    }
    function setConfigs() {}
    function index() {}
    function download() {

    }
    function action1() {}
    function action2() {}
}