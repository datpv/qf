<?php
if ( ! defined( 'IA' ) ) exit;
/*
    @Name: Application Form Builder Controller
    @Description: manage form builder
    @Docs:
*/
class Applications_Controllers_Builder extends Applications_Controllers_Fields {
    public 
        $build_configs = array(
                'field_limit' => 30
            )
        ;
    function initiallize() {
        $this->permission = 'Admin';
        parent::initiallize();
        // models
        $this->fields_model = new Applications_Models_Fields($this->registry);
        $this->forms_model = new Applications_Models_Forms($this->registry);
    }
    function setConfigs() {
        parent::setConfigs();
        if(isset($this->configs['display']['builder'])) {
            $this->build_configs = $this->configs['display']['builder'];
        }
    }
    function index() {
        $formId = $this->routers['pos_1'];
        $field_limit = $this->build_configs['field_limit'];
        $paged = $this->routers['paged'];
        $field_limit = (($paged - 1)*$field_limit).','.$field_limit;
        /*
        * Get Form
        */
        $q = '';
        $html = $formScoreUrl = '';
        $formType = isset($_GET['formType'])?$_GET['formType']:'2';
        $form_content = '';
        if( !empty($formId) && $formId != 'index' ) {
            $form = $this->forms_model->getForm(array('where' => "`form_uuid` = '$formId'"));
            if(empty($form)) {
                $this->notfound();
                exit();
            }
            $form_id = $form['form_id'];
            $q = "AND `form_id` = '$form_id'";
            $form_content = $this->helpers->jd2a($form['form_content']);
            
            $form_content['FormId'] = $formId;
            $form_content['Url'] = $formId;
            $formType = isset($form_content['Formtype'])?$form_content['Formtype']:'normal';
            $formScoreUrl = isset($form_content['FormScoreUrl'])?$form_content['FormScoreUrl']:'normal';
        
            $fields = $form_content['Fields'];
            $html = $this->getFields(array(
                'fields' => $fields,
                'errorFields' => array()));
        }
        $this->view->fields = $this->fields_model->getFields(array('limit' => '0,'.$field_limit));
        list($forms_result, $total) = $this->forms_model->getForms(array('where' => "`form_type` = 'SC' $q"));
        $this->view->formType = $formType;
        $this->view->forms_result = $forms_result;
        $this->view->formScoreUrl = $formScoreUrl;
        $this->view->html = $html;
        $this->view->form_content = $form_content;
        $this->view->title = 'Form Builder';
        $this->view->render();
    }
    /**
    *   *** AJAX FUNCTIONS ***
    */
    function builder_deletefield($req) {
        $fieldId = $this->helpers->str_escape(isset($req['fieldId'])?$req['fieldId']:'');
        if(empty($fieldId)) return;
        if($this->fields_model->deleteField(array('where' => "`field_id` = '$fieldId'"))) {
            $this->json['success'] = true;
        }
    }
    function builder_savefield($req) {
        $fields = $this->helpers->str_escape(isset($req['fields'])?$req['fields']:'');
        if(!empty($fields)) {
            $fields = $this->helpers->jd2a($fields);
            if(is_array($fields)) foreach($fields as $kf => $field) {
                if(!empty($field['SubFields'])) {
                    $subF2 = array();
                    foreach($field['SubFields'] as $k => $subF) {
                        if(empty($subF['ColumnId'])) {
                            $subF['ColumnId'] = $field['SubFields'][$k]['ColumnId'] = ''.((!isset($subF2['ColumnId'])?0:$subF2['ColumnId'])+1);
                        }
                        if(empty($subF['FieldId'])) {
                            $subF['FieldId'] = $field['SubFields'][$k]['FieldId'] = ''.((!isset($subF2['FieldId'])?0:$subF2['FieldId'])+1);
                        }
                        if(empty($subF['Price'])) {
                            $subF['Price'] = $field['SubFields'][$k]['Price'] = '0';
                        }
                        if(empty($subF['Title'])) {
                            $subF['Title'] = $field['SubFields'][$k]['Title'] = ($subF['ChoicesText']);
                        }
                        $subF2 = $subF;
                    }
                    $fields[$kf]['SubFields'] = $field['SubFields'];
                }
                if(!empty($field['Choices'])) {
                    $subF2 = array();
                    foreach($field['Choices'] as $k => $subF) {
                        if(empty($subF['ColumnId'])) {
                            if(isset($subF2['ColumnId'])) {
                                $subF['ColumnId'] = $field['Choices'][$k]['ColumnId'] =  ''.($subF2['ColumnId']+1);
                            }
                        }
                        if(empty($subF['Price'])) {
                            $subF['Price'] = $field['Choices'][$k]['Price'] = '0';
                        }
                        if(empty($subF['FormId'])) {
                            $subF['FormId'] = $field['Choices'][$k]['FormId'] = '0';
                        }
                        if(!isset($subF['Score']) || empty($subF['Score'])) {
                            $subF['Score'] = $field['Choices'][$k]['Score'] = '0';
                        }
                        if(!isset($subF['ChoiceId']) || empty($subF['ChoiceId'])) {
                            $subF['ChoiceId'] = $field['Choices'][$k]['ChoiceId'] = '0';
                        }
                        if(!isset($subF['StaticId']) || empty($subF['StaticId'])) {
                            $subF['StaticId'] = $field['Choices'][$k]['StaticId'] = '0';
                        }
                        $subF2 = $subF;
                    }
                    $fields[$kf]['Choices'] = $field['Choices'];
                }
                if(!empty($field['ExtraFields'])) {
                    foreach($field['ExtraFields'] as $extra_kf => $extra_field) {
                        if(!empty($extra_field['SubFields'])) {
                            $extra_subF2 = array();
                            foreach($extra_field['SubFields'] as $extra_k => $extra_subF) {
                                if(empty($extra_subF['ColumnId'])) {
                                    $extra_subF['ColumnId'] = $extra_field['SubFields'][$extra_k]['ColumnId'] = ''.((!isset($extra_subF2['ColumnId'])?0:$extra_subF2['ColumnId'])+1);
                                }
                                if(empty($extra_subF['FieldId'])) {
                                    $extra_subF['FieldId'] = $extra_field['SubFields'][$extra_k]['FieldId'] = ''.((!isset($extra_subF2['FieldId'])?0:$extra_subF2['FieldId'])+1);
                                }
                                if(empty($extra_subF['Price'])) {
                                    $extra_subF['Price'] = $extra_field['SubFields'][$extra_k]['Price'] = '0';
                                }
                                if(empty($extra_subF['Title'])) {
                                    $extra_subF['Title'] = $extra_field['SubFields'][$extra_k]['Title'] = ($extra_subF['ChoicesText']);
                                }
                                $extra_subF2 = $extra_subF;
                            }
                            $field['ExtraFields'][$extra_kf]['SubFields'] = $extra_field['SubFields'];
                        }
                        if(!empty($extra_field['Choices'])) {
                            $extra_subF2 = array();
                            foreach($extra_field['Choices'] as $extra_k => $extra_subF) {
                                if(empty($extra_subF['ColumnId'])) {
                                    if(isset($extra_subF2['ColumnId'])) {
                                        $extra_subF['ColumnId'] = $extra_field['Choices'][$extra_k]['ColumnId'] =  ''.($extra_subF2['ColumnId']+1);
                                    }
                                }
                                if(empty($extra_subF['Price'])) {
                                    $extra_subF['Price'] = $extra_field['Choices'][$extra_k]['Price'] = '0';
                                }
                                if(empty($extra_subF['FormId'])) {
                                    $extra_subF['FormId'] = $extra_field['Choices'][$extra_k]['FormId'] = '0';
                                }
                                if(empty($extra_subF['Score'])) {
                                    $extra_subF['Score'] = $extra_field['Choices'][$extra_k]['Score'] = '0';
                                }
                                if(empty($extra_subF['ChoiceId'])) {
                                    $extra_subF['ChoiceId'] = $extra_field['Choices'][$extra_k]['ChoiceId'] = '0';
                                }
                                if(empty($extra_subF['StaticId'])) {
                                    $extra_subF['StaticId'] = $extra_field['Choices'][$extra_k]['StaticId'] = '0';
                                }
                                $extra_subF2 = $extra_subF;
                            }
                            $field['ExtraFields'][$extra_kf]['Choices'] = $extra_field['Choices'];
                        }
                    }
                    $fields[$kf]['ExtraFields'] = $field['ExtraFields'];
                }
            }
            /*
            * insert
            */
            $field_content = $this->helpers->jeae($fields);
            $field_name = $this->helpers->str_escape($field_name);
            $field_default = $this->helpers->str_escape($field_default);
            $fieldId = $this->helpers->str_escape($fieldId);
            $field_id = $fieldId;
            $this->json['isUpdate'] = false;
            if($fieldId == 0) {
                if(!$this->fields_model->insertField(array(
                    'fields' => "`field_name`, `field_content`, `field_default`", 
                    'values' => "'$field_name', '$field_content', '$field_default'"))) {
                    echo $this->fields_model->getErrors();
                    exit;
                }
                $field_id = $this->fields_model->getInsertId();
                $this->json['success'] = true;
            } else {
                if(!$this->fields_model->updateField(array(
                    'updates' => "`field_name` = '$field_name', `field_content` = '$field_content', `field_default` = '$field_default'", 
                    'where' => "`field_id` = '$fieldId'"))) {
                    echo $this->fields_model->getErrors();
                    exit;
                }
                $this->json['success'] = true;
                $this->json['isUpdate'] = true;
            }
        }
        $this->json['response']['field_id'] = $field_id;
        $this->json['response']['field_json'] = json_encode($fields);
    }
    function builder_save($req) {
        $formId = '';
        $form = $req['form'];
        $byImpoter = isset($req['byImpoter'])?$req['byImpoter']:false;
        if($byImpoter == true) {
            $user_import_id = $this->user['user_id'];
            if($this->user['user_type'] != 'A') {
                list($iforms, $total) = $this->forms_model->getForms(array('select' => 'SQL_CACL_FOUND_ROWS *','where' => "`form_user` = '$user_import_id'"));
                if($total >= 10) {
                    $this->json['message'] = 'Limit Forms Contribue Allowed';
                    return;
                }
            }
            
        }
        if(empty($form)) {return false;}
        $__form = $this->helpers->jd2a($form);
        if(empty($__form) || is_string($__form)) return;
        if(empty($__form['Name']) || empty($__form['Fields'])) {
            $this->json['message'] = 'No Fields Or Form Name Empty';
            return;
        }
        
        foreach($__form['Fields'] as $kf => $field) {
            if(empty($field['FieldLink'])) {
                $rn = rand(1,99) . '_' . rand(1,9) . '-' . $field['ColumnId'] . '_' . rand(1,99);
                $__form['Fields'][$kf]['FieldLink'] = $this->helpers->encrypt($rn,'dpkey');
            }
            if(!empty($field['SubFields'])) {
                $subF2 = array();
                foreach($field['SubFields'] as $k => $subF) {
                    if(empty($subF['ColumnId'])) {
                        $subF['ColumnId'] = $field['SubFields'][$k]['ColumnId'] = ''.((!isset($subF2['ColumnId'])?0:$subF2['ColumnId'])+1);
                    }
                    if(empty($subF['FieldId'])) {
                        $subF['FieldId'] = $field['SubFields'][$k]['FieldId'] = ''.((!isset($subF2['FieldId'])?0:$subF2['FieldId'])+1);
                    }
                    if(empty($subF['Price'])) {
                        $subF['Price'] = $field['SubFields'][$k]['Price'] = '0';
                    }
                    if(empty($subF['Title'])) {
                        $subF['Title'] = $field['SubFields'][$k]['Title'] = ($subF['ChoicesText']);
                    }
                    $subF2 = $subF;
                }
                $__form['Fields'][$kf]['SubFields'] = $field['SubFields'];
            }
            if(!empty($field['Choices'])) {
                $subF2 = array();
                foreach($field['Choices'] as $k => $subF) {
                    if(!isset($subF['ColumnId']) || empty($subF['ColumnId'])) {
                        if(isset($subF2['ColumnId'])) {
                            $subF['ColumnId'] = $field['Choices'][$k]['ColumnId'] =  ''.($subF2['ColumnId']+1);
                        }
                    }
                    if(!isset($subF['Price']) || empty($subF['Price'])) {
                        $subF['Price'] = $field['Choices'][$k]['Price'] = '0';
                    }
                    if(!isset($subF['FormId']) || empty($subF['FormId'])) {
                        $subF['FormId'] = $field['Choices'][$k]['FormId'] = '0';
                    }
                    if(!isset($subF['Score']) || empty($subF['Score'])) {
                        $subF['Score'] = $field['Choices'][$k]['Score'] = '0';
                    }
                    if(!isset($subF['ChoiceId']) || empty($subF['ChoiceId'])) {
                        $subF['ChoiceId'] = $field['Choices'][$k]['ChoiceId'] = '0';
                    }
                    if(!isset($subF['StaticId']) || empty($subF['StaticId'])) {
                        $subF['StaticId'] = $field['Choices'][$k]['StaticId'] = '0';
                    }
                    $subF2 = $subF;
                }
                $__form['Fields'][$kf]['Choices'] = $field['Choices'];
            }
            if(!empty($field['ExtraFields'])) {
                foreach($field['ExtraFields'] as $extra_kf => $extra_field) {
                    if(!empty($extra_field['SubFields'])) {
                        $extra_subF2 = array();
                        foreach($extra_field['SubFields'] as $extra_k => $extra_subF) {
                            if(empty($extra_subF['ColumnId'])) {
                                $extra_subF['ColumnId'] = $extra_field['SubFields'][$extra_k]['ColumnId'] = ''.((!isset($extra_subF2['ColumnId'])?0:$extra_subF2['ColumnId'])+1);
                            }
                            if(empty($extra_subF['FieldId'])) {
                                $extra_subF['FieldId'] = $extra_field['SubFields'][$extra_k]['FieldId'] = ''.((!isset($extra_subF2['FieldId'])?0:$extra_subF2['FieldId'])+1);
                            }
                            if(!isset($extra_subF['Price']) || empty($extra_subF['Price'])) {
                                $extra_subF['Price'] = $extra_field['SubFields'][$extra_k]['Price'] = '0';
                            }
                            if(!isset($extra_subF['Title']) || empty($extra_subF['Title'])) {
                                $extra_subF['Title'] = $extra_field['SubFields'][$extra_k]['Title'] = ($extra_subF['ChoicesText']);
                            }
                            $extra_subF2 = $extra_subF;
                        }
                        $field['ExtraFields'][$extra_kf]['SubFields'] = $extra_field['SubFields'];
                    }
                    if(!empty($extra_field['Choices'])) {
                        $extra_subF2 = array();
                        foreach($extra_field['Choices'] as $extra_k => $extra_subF) {
                            if(empty($extra_subF['ColumnId'])) {
                                if(isset($extra_subF2['ColumnId'])) {
                                    $extra_subF['ColumnId'] = $extra_field['Choices'][$extra_k]['ColumnId'] =  ''.($extra_subF2['ColumnId']+1);
                                }
                            }
                            if(!isset($extra_subF['Price']) || empty($extra_subF['Price'])) {
                                $extra_subF['Price'] = $extra_field['Choices'][$extra_k]['Price'] = '0';
                            }
                            if(!isset($extra_subF['FormId']) || empty($extra_subF['FormId'])) {
                                $extra_subF['FormId'] = $extra_field['Choices'][$extra_k]['FormId'] = '0';
                            }
                            if(!isset($extra_subF['Score']) || empty($extra_subF['Score'])) {
                                $extra_subF['Score'] = $extra_field['Choices'][$extra_k]['Score'] = '0';
                            }
                            if(!isset($extra_subF['ChoiceId']) || empty($extra_subF['ChoiceId'])) {
                                $extra_subF['ChoiceId'] = $extra_field['Choices'][$extra_k]['ChoiceId'] = '0';
                            }
                            if(!isset($extra_subF['StaticId']) || empty($extra_subF['StaticId'])) {
                                $extra_subF['StaticId'] = $extra_field['Choices'][$extra_k]['StaticId'] = '0';
                            }
                            $extra_subF2 = $extra_subF;
                        }
                        $field['ExtraFields'][$extra_kf]['Choices'] = $extra_field['Choices'];
                    }
                }
                $__form['Fields'][$kf]['ExtraFields'] = $field['ExtraFields'];
            }
        }
        $_form = $this->helpers->jeae($__form);
        $formType = $__form['Formtype'];
        $alt1 = $alt2 = $alt3 = $alt4 = $alt5 = '';
        if($__form['FormId'] == '0') {
            if($formType == 'score') {
                $alt1 = ",`form_type`";
                $alt2 = ",'SC'";
            } elseif($formType == 'nopbai') {
                $alt1 = ",`form_type`";
                $alt2 = ",'NB'";
            } elseif($formType == 'autoscore') {
                $alt1 = ",`form_type`";
                $alt2 = ",'AS'";
            }
            if(isset($byImpoter) && $byImpoter == true)  {
                $alt4 = ",`form_user`, `form_status`, `form_approved`";
                $alt5 = ",'$user_import_id', 'D', 'N'";
            }
            $formId = $this->helpers->str_uuid();
            if(!$this->forms_model->insertForm(array(
                'fields' => "`form_uuid`,`form_content` $alt1 $alt4", 
                'values' => "'$formId','$_form' $alt2 $alt5"))) {
                echo $this->forms_model->getErrors();
                exit;
            }
        } else {
            $formId = $__form['FormId'];
            $formId = $this->helpers->str_escape($formId);
            $form = $this->forms_model->getForm(array('where' => "`form_uuid` = '$formId'"));
            if(!empty($form)) {
                $updatedate = $this->helpers->get_datetime();
                if($formType == 'score') {
                    $alt3 = "`form_type` = 'SC',";
                } elseif($formType == 'nopbai') {
                    $alt3 = "`form_type` = 'NB',";
                } elseif($formType == 'autoscore') {
                    $alt3 = "`form_type` = 'AS',";
                } elseif($formType == 'normal') {
                    $alt3 = "`form_type` = 'NO',";
                }
                if(!$this->forms_model->updateForm(array(
                    'updates' => "$alt3 `form_content` = '$_form', `form_update` = '$updatedate'" , 
                    'where' => "`form_uuid` = '$formId'"))) {
                    echo $forms_obj->getErrors();
                }
            }
        }
        $this->json = array(
            'success' => true,
            'response' => array(
                'firstForm' => '',
                'url' => $formId,
                'id' => $formId
            )
        );  
    }
    function builder_displayfield($req) {
        $type = $this->helpers->str_escape(isset($req['type'])?$req['type']:'');
        $fieldId = $this->helpers->str_escape(isset($req['fieldId'])?$req['fieldId']:'');
        $fieldFormId = $this->helpers->str_escape(isset($req['fieldFormId'])?$req['fieldFormId']:'');
        $field = $this->helpers->str_escape(isset($req['field'])?$req['field']:'');
        $fieldPrefix = $this->helpers->str_escape(isset($req['fieldPrefix'])?$req['fieldPrefix']:'');
        if(!empty($field)) {
            $this->field = $this->helpers->jd2a($field);
        }
        $html = $this->buildField(array(
            'disabled' => true,
            'errorFields' => array(),
            'fieldPrefix' => $fieldPrefix,
            'field' => $this->field, 
            'fieldId' => $fieldId, 
            'fieldFormId' => $fieldFormId, 
            'type' => $type,
            'isCreateField' => true));
        $fieldId = $this->field['ColumnId'];
        $this->json['success'] = true;
        $this->json['response']['id'] = $fieldId;
        $this->json['response']['html'] = $this->helpers->str_trim($html);
        $this->json['response']['json'] = json_encode($this->_json);
    }
    function builder_changefield($req) {
        
    }
    function builder_addextrafield($req) {
        $req['fieldPrefix'] = 'extra_';
        $this->builder_addfield($req);
    }
    function builder_addfield($req) {
        $type = $this->helpers->str_escape(isset($req['type'])?$req['type']:'');
        $fieldId = $this->helpers->str_escape(isset($req['fieldId'])?$req['fieldId']:'');
        $fieldFormId = $this->helpers->str_escape(isset($req['fieldFormId'])?$req['fieldFormId']:'');
        $field = $this->helpers->str_escape(isset($req['field'])?$req['field']:'');
        $fieldPrefix = $this->helpers->str_escape(isset($req['fieldPrefix'])?$req['fieldPrefix']:'');
        if(!empty($field)) {
            $this->field = $this->doubleField($field,$fieldId);
        }
        $html = $this->buildField(array(
            'disabled' => true,
            'errorFields' => array(),
            'fieldPrefix' => $fieldPrefix,
            'field' => $this->field, 
            'fieldId' => $fieldId, 
            'fieldFormId' => $fieldFormId, 
            'type' => $type,
            'isCreateField' => true));
        $this->json['success'] = true;
        $this->json['response']['id'] = $fieldId;
        $this->json['response']['html'] = $this->helpers->str_trim($html);
        $this->json['response']['json'] = json_encode($this->_json);
    }
    /**
    *   *** COMMON FUNCTIONS ***
    */
}