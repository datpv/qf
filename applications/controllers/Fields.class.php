<?php
if ( ! defined( 'IA' ) ) exit;
/*
    @Name: Application Form Fields Builder Controller
    @Description: manage form fields
    @Docs:
*/
class Applications_Controllers_Fields extends Applications_Controller {
    private
        $_json = array(),
        $field = array(
                'FieldId' => '0',
                'Title' => 'Field Tilte',
                'Instructions' => '',
                'RightAnswerDescription' => '',
                'WrongAnswerDescription' => '',
                'InputRightAnswer' => '',
                'Typeof' => '',
                'Size' => 'medium',
                'IsRequired' => '0',
                'IsUnique' => '0',
                'IsPrivate' => '0',
                'Validation' => '0',
                'Pos' => '0',
                'ColumnId' => '0',
                'FormId' => '0',
                'Price' => '0',
                'ClassNames' => '',
                'IsEncrypted' => '0',
                'RangeType' => 'character',
                'RangeMin' => '0',
                'RangeMax' => '',
                'DefaultVal' => '',
                'ChoicesText' => '0',
                'Settings' => array(),
                'Page' => '',
                'MerchantEnabled' => false,
                'doValidateRequired' => true,
                'ExtraFields' => array()
            );
    function __construct($registry) {
        parent::__construct($registry);
    }
    function initiallize() {
        parent::initiallize();
    }
    function setConfigs() {
        parent::setConfigs();
    }
    function index() {
    }
    /**
    *   *** COMMON FUNCTIONS ***
    */
    function mergeJson() {
    }
    function getForm($form_nb, $entry_content, $user, $hideHeadInfo=false, $hideHeader = false) {
        $html2 = '';
        if (!ob_get_level()) ob_start();
        $entry_id = $entry_content['entry_id'];
        $entries = $this->helpers->json_decode_to_array_not_in_mysql($entry_content['entry_content']);
        $form_nb_content = $this->helpers->json_decode_to_array($form_nb['form_content']);
        $form_nb_fields = $form_nb_content['Fields'];
        if(!$hideHeader) {
        ?>
        <table cellspacing="0" style="width:100%;font-family:'Lucida Grande','Lucida Sans Unicode', Tahoma, sans-serif;">
            <?php
            if($entry_content['entry_type'] == 'NB' || $entry_content['entry_type'] == 'AS') :
                $user_content = $this->helpers->json_decode_to_array($user['user_content']);
                if(!$hideHeadInfo) :
                    ?>
                    <tr>
                        <td>Bài Nộp Của: <?php echo $user_content['Name']; ?><br />
                        <?php
                        if($this->options['show_nickh2d']) {
                        ?>
                        Nick H2d: <?php echo $user_content['NickH2d']; ?><br />
                        <?php
                        }
                        ?>
                        </td>
                    </tr>
                    <?php 
                else:
                    ?>
                    <tr>
                        <td><h3><strong>Form:</strong> <?php echo $form_nb_content['Name']; ?></h3><br /></td>
                    </tr>
                    <?php
                endif;
            endif; 
            ?>
        </table>
        <?php
        }
        ?>
        <table class="readonly" cellspacing="0" cellpadding="0" style="width:100%;border-bottom:1px solid #eee;font-size:12px;line-height:135%;font-family:'Lucida Grande','Lucida Sans Unicode', Tahoma, sans-serif">
        <?php
        $i = 0;
        foreach($form_nb_fields as $k => $field) {
            $values =  $titles = array();
            $value = $title = '';
            $validation = isset($field['Validation'])?$field['Validation']:'';
            if(isset($field['SubFields'])) {
                $j = 0;
                foreach($field['SubFields'] as $subF) {
                    if(isset($entries['Field'.$subF['ColumnId']])) {
                        if(!empty($entries['Field'.$subF['ColumnId']])) {
                            if($field['Typeof'] == 'likert') {
                                $titles[] = $subF['ChoicesText'];
                            } else {
                                $titles[] = $subF['Title'];
                            }
                            $values[] = $entries['Field'.$subF['ColumnId']];
                        }
                    }
                    if($field['Typeof'] == 'likert' && $field['Validation'] == 'dc') {
                        $titles[$j] = $subF['Title'];
                        foreach($field['Choices'] as $k => $choiC) {
                            
                            if(isset($entries['Field'.$subF['ColumnId'].'_'.($k+1)])) {
                                if(!empty($entries['Field'.$subF['ColumnId'].'_'.($k+1)])) {
                                    $values[$j][] = $entries['Field'.$subF['ColumnId'].'_'.($k+1)];
                                }
                            } else {
                                $values[$j][] = '';
                            }
                        }
                        $j++;
                    }
                }
            } else {
                if(isset($entries['Field'.$field['ColumnId']])) {
                    $title = $field['Title'];
                    $value = $entries['Field'.$field['ColumnId']];
                    if($field['Typeof'] == 'date' || $field['Typeof'] == 'eurodate') {
                        $value = $entries['Field'.$field['ColumnId'].(($field['Typeof'] == 'eurodate')?'-2':'-1')] .'/'.$entries['Field'.$field['ColumnId'].(($field['Typeof'] == 'eurodate')?'-1':'-2')].'/'.$entries['Field'.$field['ColumnId'].''] . ' (mm/dd/yyy)';
                    } elseif($field['Typeof'] == 'time') {
                        $value = $value . ':' . $entries['Field'.$field['ColumnId'].'-1'] . ':' . $entries['Field'.$field['ColumnId'].'-2'] . ' ' . $entries['Field'.$field['ColumnId'].'-3'];
                    } elseif($field['Typeof'] == 'shortname') {
                        $value = $value . ':' . $entries['Field'.$field['ColumnId'].'-1'] . ':' . $entries['Field'.$field['ColumnId'].'-2'] . ' ' . $entries['Field'.$field['ColumnId'].'-3'];
                    }
                }
            }
            if($field['Typeof'] == 'likert') {
                foreach($titles as $k => $title) {
                    if(is_array($values[$k])) {
                        $values[$k] = array_filter($values[$k]);
                    }
                    if(!empty($values[$k])) {
                        if($i % 2 == 0) { 
                            echo $this->echoTd1($title, $values[$k]);
                        } else {
                            echo $this->echoTd2($title, $values[$k]);
                        }
                    }
                    $i++;
                }
            } elseif($field['Typeof'] == 'radio') {
                foreach($field['Choices'] as $k => $choiC) {
                    if($choiC['Choice'] == 'Other') {
                        $post_value_other = isset($entries['Field'.$field['ColumnId'].'_other_Other'])?$entries['Field'.$field['ColumnId'].'_other_Other']:'';
                        if(!empty($post_value_other)) $value = $post_value_other;
                        break;
                    }
                }
                $title = $this->helpers->detectLink($title);
                $value = $this->helpers->detectLink($value);
                if($i % 2 == 0) { 
                    echo $this->echoTd1($title, $value);
                } else {
                    echo $this->echoTd2($title, $value);
                }
                $i++;
            }  elseif($field['Typeof'] == 'checkbox') {
                if($i % 2 == 0) { 
                    echo $this->echoTd1($field['Title'], $values);
                } else {
                    echo $this->echoTd2($field['Title'], $values);
                }
                $i++;
            } elseif($field['Typeof'] == 'shortname') {
                $value = '';
                foreach($titles as $k => $title) {
                    $value .= ' '.$values[$k];
                }
                if($i % 2 == 0) { 
                    echo $this->echoTd1($title, $value);
                } else {
                    echo $this->echoTd2($title, $value);
                }
                $i++;
            } elseif($field['Typeof'] == 'address') {
                foreach($field['SubFields'] as $subF) {
                    $index = strtolower(str_replace(' ','-',$subF['Title']));
                    $values[] = isset($entries['Field'.$subF['ColumnId']])?$entries['Field'.$subF['ColumnId']]:'';
                }
                $values = array_filter($values);
                $value = '';
                $search = (isset($values[0])?$values[0]:'').'+'.(isset($values[1])?$values[1]:'').'+'.(isset($values[2])?$values[2]:'');
                $value .= '<a href="http://maps.google.com/?q='.$search.'" style="text-decoration:none;" title="Show a Map of this Location" target="_blank">';
                    $value .= '<img width="16" height="16" style="padding:2px 0 0 0;float:left;" alt="" src="../images/icons/map.png" class="mapicon">';
                $value .= '</a>';
                $value .= '<address style="color:#333;font-style:normal;line-height:130%;padding:2px 0 2px 25px;" class="adr">';
                    
                    $value .= '<span class="street-address">'.(isset($values[0])?$values[0]:'').'</span>';
                    $value .= '<span class="extended-address"> '.(isset($values[1])?$values[1]:'').'</span><br>';           
                    $value .= '<span class="locality"> '.(isset($values[2])?$values[2]:'').'</span>';               
                    //$value .= '<span class="region">'.(isset($values[3])?$values[3]:'').'</span>';
                    //$value .= '<span class="postal-code">'.(isset($values[4])?$values[4]:'').'</span>';
                    //$value .= '<br>';             
                    //$value .= '<span class="country-name">'.(isset($values[5])?$values[5]:'').'</span>';
                    
                $value .= '</address>';
                if($i % 2 == 0) { 
                    echo $this->echoTd1($field['Title'], $value);
                } else {
                    echo $this->echoTd2($field['Title'], $value);
                }
                $i++;
            }  else {
                $title = $this->helpers->detectLink($title);
                $value = $this->helpers->detectLink($value);
                if($i % 2 == 0) { 
                    echo $this->echoTd1($title, $value);
                } else {
                    echo $this->echoTd2($title, $value);
                }
                $i++;
            }
        }
        ?>
        </table>
        <?php
        $html2 .= ob_get_contents();
        ob_end_clean();
        return $html2;
    }
    /**
    *   *** SINGLE FIELD FUNCTIONS ***
    */
    protected function print_fieldText($args) {
        $f_id = $args['f_id'];
        $field = $args['field'];
        $fieldId = $args['fieldId'];
        $fieldPrefix = $args['fieldPrefix'];
        $f_req = $args['f_req'];
        $disable = ($args['disabled'])?' readonly="readonly" disabled="disabled"':'';
        ?><div><input id="<?php echo $f_id; ?>" name="<?php echo $f_id; ?>" type="text" class="field text <?php echo $field['Size']; ?>" value="<?php echo $field['DefaultVal']; ?>" maxlength="<?php
            if($field['RangeType'] == 'character') {
                echo empty($field['RangeMax'])?'255':$field['RangeMax']; 
            }
            ?>" tabindex="<?php echo $field['DisplayPos']; ?>" onkeyup="handleInput(this); " onchange="handleInput(this);" <?php echo $f_req; echo $disable; ?> /><?php
            if(!empty($field['RangeMax'])) :
            ?><label for="<?php echo $f_id; ?>">Maximum Allowed: <var id="<?php echo $fieldPrefix; ?>rangeMaxMsg<?php echo $fieldId; ?>"><?php echo $field['RangeMax']; ?></var> <?php echo $field['RangeType']; ?>.&nbsp;&nbsp;&nbsp; <em class="currently" style="display: inline;"></em></label><?php
            endif;
        ?></div><?php
    }
    protected function print_fieldTextarea($args) {
        $f_id = $args['f_id'];
        $field = $args['field'];
        $fieldId = $args['fieldId'];
        $fieldPrefix = $args['fieldPrefix'];
        $f_req = $args['f_req'];
        $disable = ($args['disabled'])?'  readonly="readonly" disabled="disabled"':'';
        ?><div><div class="handle"></div><textarea id="<?php echo $f_id; ?>" name="<?php echo $f_id; ?>" class="field textarea <?php echo $field['Size']; ?>" spellcheck="true" rows="10" cols="50" tabindex="<?php echo $field['DisplayPos']; ?>" onkeyup="handleInput(this); " onchange="handleInput(this);" <?php echo $f_req; echo $disable; ?> ><?php echo $field['DefaultVal']; ?></textarea><?php
            if(!empty($field['RangeMax'])) {
            ?><label for="<?php echo $f_id; ?>">Maximum Allowed: <var id="<?php echo $fieldPrefix; ?>rangeMaxMsg<?php echo $fieldId; ?>"><?php echo $field['RangeMax']; ?></var> <?php echo $field['RangeType']; ?>.&nbsp;&nbsp;&nbsp; <em class="currently" style="display: inline;">Currently Used: <var id="<?php echo $fieldPrefix; ?>rangeUsedMsg<?php echo $fieldId; ?>">0</var> characters.</em></label><?php
            }
        ?></div><?php
    }
    protected function print_fieldRadio($args) {
        $f_id = $args['f_id'];
        $fieldId = $args['fieldId'];
        $fieldPrefix = $args['fieldPrefix'];
        $choices = $args['choices'];
        $disable = ($args['disabled'])?' readonly="readonly" disabled="disabled"':'';
        ?><div><input id="<?php echo $fieldPrefix; ?>radioDefault_<?php echo $fieldId; ?>" name="<?php echo $f_id; ?>" type="hidden" value="" <?php echo $disable; ?> /><?php
            if(!empty($choices)) {
                $i = 0;
                foreach($choices as $v) {
                    ?><span><input id="<?php echo $f_id; ?>_<?php echo $i; ?>" name="<?php echo $f_id; ?>" type="radio" class="field radio" value="<?php echo $v['Choice']; ?>" tabindex="<?php echo $i; ?>" onchange="handleInput(this);" onmouseup="handleInput(this);" <?php echo $v['IsDefault']?'checked="checked"':''; echo $disable; ?> /><label class="choice" for="<?php echo $f_id; ?>_<?php echo $i; ?>"><?php echo $this->helpers->str_detectLink($v['Choice']); ?></label></span><?php if($v['Choice'] == 'Other') { ?>
                        <input id="<?php echo $f_id; ?>_other" name="<?php echo $f_id; ?>_other_Other" type="text" class="field text other" value="" onclick="document.getElementById('Field<?php echo $fieldId; ?>_<?php echo $i; ?>').checked = 'checked';" onkeyup="handleInput(this);" onchange="handleInput(this);"
                        tabindex="<?php echo $i; ?>" <?php echo $disable; ?> /><?php }
                    $i++;
                }
            }
        ?></div><?php
    }
    protected function print_fieldCheckbox($args) {
        $fieldPrefix = $args['fieldPrefix'];
        $subFields = $args['subFields'];
        $disable = ($args['disabled'])?' readonly="readonly" disabled="disabled"':'';
        //
        ?><div><?php
            if(!empty($subFields)) {
                foreach($subFields as $v) {
                    $subFieldId = $v['ColumnId'];
                    $sf_id = $fieldPrefix.'Field'.$subFieldId;
                    ?><span><input id="<?php echo $sf_id; ?>" name="<?php echo $sf_id; ?>" type="checkbox" class="field checkbox" value="<?php echo $v['ChoicesText']; ?>" tabindex="1" onchange="handleInput(this);"  <?php echo $v['DefaultVal']?'checked="checked"':''; echo $disable; ?> /><label class="choice" for="<?php echo $sf_id; ?>"><?php echo $this->helpers->str_detectLink($v['ChoicesText']); ?></label></span><?php
                }
            }
        ?></div><?php
    }
    protected function print_fieldSelect($args) {
        $f_id = $args['f_id'];
        $field = $args['field'];
        $choices = $args['choices'];
        $disable = ($args['disabled'])?' readonly="readonly" disabled="disabled"':'';
        ?><div><select id="<?php echo $f_id; ?>" name="<?php echo $f_id; ?>" class="field select <?php echo $field['Size']; ?>" onclick="handleInput(this);" onkeyup="handleInput(this);" tabindex="<?php echo $field['DisplayPos']; ?>" <?php echo $disable; ?> ><?php
            if(!empty($choices)) {
                foreach($choices as $v) {
        ?><option <?php echo ($v['IsDefault'])?'selected="selected"':''; ?> value="<?php echo $v['Choice']; ?>"><?php 
        echo $v['Choice']; ?></option><?php
                }
            }
        ?></select></div><?php
    }
    protected function print_fieldShortname($args) {
        $subFields = $args['subFields'];
        $fieldPrefix = $args['fieldPrefix'];
        $f_req = $args['f_req'];
        $disable = ($args['disabled'])?' readonly="readonly" disabled="disabled"':'';
        $i=0;
        foreach($subFields as $v) {
            $subFieldId = $v['ColumnId'];
            $sf_id = $fieldPrefix.'Field'.$subFieldId;
            ?><span><input id="<?php echo $sf_id; ?>" name="<?php echo $sf_id; ?>" type="text" class="field text <?php echo ($i==0)?'fn':'ln'; ?>" value="" size="8" tabindex="1" onkeyup="handleInput(this);" onchange="handleInput(this);" <?php echo $f_req; echo $disable; ?> /><label for="<?php echo $sf_id; ?>"><?php echo $v['ChoicesText']; 
            ?></label></span><?php
            $i++;
        }
    }
    protected function print_fieldAddress($args) {
        extract($args);
        $disable = ($args['disabled'])?' readonly="readonly" disabled="disabled"':'';
        ?><div><?php
            if(!empty($subFields)) {
                $i=0;
                foreach($subFields as $v) {
                    $subFieldId = $v['ColumnId'];
                    $eclass = '';
                    if($i==0) $eclass = 'full addr1';
                    elseif($i==1) $eclass = 'full addr2';
                    elseif($i==2) $eclass = 'left city';
                    elseif($i==3) $eclass = 'right state';
                    elseif($i==4) $eclass = 'left zip';
                    elseif($i==5) $eclass = 'right country';
                    $sf_id = $fieldPrefix.'Field'.$subFieldId;
                    ?><span class="<?php echo $eclass; ?>"><?php if($i!=5): 
                    ?><input id="<?php echo $sf_id; ?>" name="<?php echo $sf_id; ?>" type="text" class="field text addr" value="" tabindex="1" onkeyup="handleInput(this);" onchange="handleInput(this);" <?php echo $disable; ?>/><?php else: 
                    ?><select id="<?php echo $sf_id; ?>" name="<?php echo $sf_id; ?>" class="field select addr" tabindex="6" onclick="handleInput(this);" onkeyup="handleInput(this);" <?php echo $disable; ?>><?php
                        if(empty($countries)) $countries = $this->common_listCountries();
                        echo $countries; 
                        ?></select><?php endif; 
                        ?><label for="<?php echo $sf_id; ?>"><?php echo $v['ChoicesText']; 
                        ?></label></span><?php
                    $i++;
                }
            }
        ?></div><?php
    }
    protected function print_fieldEurodate($args) {
        $f_id = $args['f_id'];
        $fieldId = $args['fieldId'];
        $fieldPrefix = $args['fieldPrefix'];
        $disable = ($args['disabled'])?' readonly="readonly" disabled="disabled"':'';
        ?><span><input id="<?php echo $f_id; ?>-2" name="<?php echo $f_id; ?>-2" type="text" class="field text" value="" size="2" maxlength="2" tabindex="2" onkeyup="handleInput(this);" onchange="handleInput(this);" <?php echo $disable; ?> /><label for="<?php echo $f_id; ?>-2">DD</label></span><span class="symbol">/</span><span><input id="<?php echo $f_id; ?>-1" name="<?php echo $f_id; ?>-1" type="text" class="field text" value="" size="2" maxlength="2" tabindex="1" onkeyup="handleInput(this);" onchange="handleInput(this);" <?php echo $disable; ?> /><label for="<?php echo $f_id; ?>-1">MM</label></span><span class="symbol">/</span><span><input id="<?php echo $f_id; ?>" name="<?php echo $f_id; ?>" type="text" class="field text" value="" size="4" maxlength="4" tabindex="3" onkeyup="handleInput(this);" onchange="handleInput(this);" <?php echo $disable; ?> /><label for="<?php echo $f_id; ?>">YYYY</label></span><span id="<?php echo $fieldPrefix; ?>cal<?php echo $fieldId; ?>"><img id="<?php echo $fieldPrefix; ?>pick<?php echo $fieldId; ?>" class="datepicker" src="../images/icons/calendar.png" alt="Pick a date." /></span><?php
    }
    protected function print_fieldDate($args) {
        $f_id = $args['f_id'];
        $fieldId = $args['fieldId'];
        $fieldPrefix = $args['fieldPrefix'];
        $disable = ($args['disabled'])?' readonly="readonly" disabled="disabled"':'';
        ?><span><input id="<?php echo $f_id; ?>-1" name="<?php echo $f_id; ?>-1" type="text" class="field text" value="" size="2" maxlength="2" tabindex="1" onkeyup="handleInput(this);" onchange="handleInput(this);" <?php echo $disable; ?> /><label for="<?php echo $f_id; ?>-1">MM</label></span><span class="symbol">/</span><span><input id="<?php echo $f_id; ?>-2" name="<?php echo $f_id; ?>-2" type="text" class="field text" value="" size="2" maxlength="2" tabindex="2" onkeyup="handleInput(this);" onchange="handleInput(this);" <?php echo $disable; ?> /><label for="<?php echo $f_id; ?>-2">DD</label></span><span class="symbol">/</span><span><input id="<?php echo $f_id; ?>" name="<?php echo $f_id; ?>" type="text" class="field text" value="" size="4" maxlength="4" tabindex="3" onkeyup="handleInput(this);" onchange="handleInput(this);" <?php echo $disable; ?> /><label for="<?php echo $f_id; ?>">YYYY</label></span><span id="<?php echo $fieldPrefix; ?>cal<?php echo $fieldId; ?>"><img id="<?php echo $fieldPrefix; ?>pick<?php echo $fieldId; ?>" class="datepicker" src="../images/icons/calendar.png" alt="Pick a date." /></span><?php
    }
    protected function print_fieldEmail($args) {
        $f_id = $args['f_id'];
        $field = $args['field'];
        $f_req = $args['f_req'];
        $disable = ($args['disabled'])?' readonly="readonly" disabled="disabled"':'';
        ?><div><input id="<?php echo $f_id; ?>" name="<?php echo $f_id; ?>" type="email" spellcheck="false" class="field text medium" value="" maxlength="255" tabindex="<?php echo $field['DisplayPos']; ?>" onkeyup="handleInput(this);" onchange="handleInput(this);" <?php echo $f_req; echo $disable; ?>/></div><?php
    }
    protected function print_fieldTime($args) {
        $f_id = $args['f_id'];
        $disable = ($args['disabled'])?' readonly="readonly" disabled="disabled"':'';
        ?><span class="hours"><input id="<?php echo $f_id; ?>" name="<?php echo $f_id; ?>" type="text" class="field text" value="<?php echo $field['DefaultVal']; ?>" size="2" maxlength="2" tabindex="1" onkeyup="handleInput(this);" onchange="handleInput(this);" <?php echo $disable; ?> /><label for="<?php echo $f_id; ?>">HH</label></span><span class="symbol minutes">:</span><span class="minutes"><input id="<?php echo $f_id; ?>-1" name="<?php echo $f_id; ?>-1" type="text" class="field text" value="" size="2" maxlength="2" tabindex="2" onkeyup="handleInput(this);" onchange="handleInput(this);" <?php echo $disable; ?> /><label for="<?php echo $f_id; ?>-1">MM</label></span><span class="symbol seconds">:</span><span class="seconds"><input id="<?php echo $f_id; ?>-2" name="<?php echo $f_id; ?>-2" type="text" class="field text" value="" size="2" maxlength="2" tabindex="3" onkeyup="handleInput(this);" onchange="handleInput(this);" <?php echo $disable; ?> /><label for="<?php echo $f_id; ?>-2">SS</label></span><span class="ampm"><select id="<?php echo $f_id; ?>-3" name="<?php echo $f_id; ?>-3" class="field select" style="width:4em" tabindex="4" onclick="handleInput(this);" onkeyup="handleInput(this);"><option value="AM" selected="selected">AM</option><option value="PM">PM</option></select><label for="<?php echo $f_id; ?>-3">AM/PM</label></span><?php
    }
    protected function print_fieldPhone($args) {
        $f_id = $args['f_id'];
        $disable = ($args['disabled'])?' readonly="readonly" disabled="disabled"':'';
        $p1 = $p2 = $p3 = '';
        if(!empty($field['DefaultVal'])) {
            $p = $field['DefaultVal'];
            $p1 = substr($p,0,3);
            $p2 = substr($p,3,6);
            $p3 = substr($p,6,9);
        }
        ?><span><input id="<?php echo $f_id; ?>" name="<?php echo $f_id; ?>" type="tel" class="field text" value="<?php echo $p1; ?>" size="3" maxlength="3" tabindex="1" onkeyup="handleInput(this);" onchange="handleInput(this);" <?php echo $disable; ?> /><label for="<?php echo $f_id; ?>">###</label></span><span class="symbol">-</span><span><input id="<?php echo $f_id; ?>-1" name="<?php echo $f_id; ?>-1" type="tel" class="field text" value="<?php echo $p2; ?>" size="3" maxlength="3" tabindex="2" onkeyup="handleInput(this);" onchange="handleInput(this);" <?php echo $disable; ?> /><label for="<?php echo $f_id; ?>-1">###</label></span><span class="symbol">-</span><span><input id="<?php echo $f_id; ?>-2" name="<?php echo $f_id; ?>-2" type="tel" class="field text" value="<?php echo $p3; ?>" size="4" maxlength="4" tabindex="3" onkeyup="handleInput(this);" onchange="handleInput(this);" <?php echo $disable; ?> /><label for="<?php echo $f_id; ?>-2">####</label></span><?php
    }
    protected function print_fieldUrl($args) {
        $f_id = $args['f_id'];
        $field = $args['field'];
        $disable = ($args['disabled'])?' readonly="readonly" disabled="disabled"':'';
        ?><div><input id="<?php echo $f_id; ?>" name="<?php echo $f_id; ?>" type="url" class="field text medium" value="<?php echo $field['DefaultVal']; ?>" maxlength="255" tabindex="1" onkeyup="handleInput(this);" onchange="handleInput(this);" <?php echo $disable; ?> /></div><?php
    }
    protected function print_fieldMoney($args) {
        $f_id = $args['f_id'];
        $disable = ($args['disabled'])?' readonly="readonly" disabled="disabled"':'';
        ?><span class="symbol">$</span><span><input id="<?php echo $f_id; ?>" name="<?php echo $f_id; ?>" type="text" class="field text currency nospin" value="" size="10" tabindex="1" onkeyup="handleInput(this);" onchange="handleInput(this);" <?php echo $disable; ?> /><label for="<?php echo $f_id; ?>">Dollars</label></span><span class="symbol radix">.</span><span class="cents"><input id="<?php echo $f_id; ?>-1" name="<?php echo $f_id; ?>-1" type="text" class="field text nospin" value="" size="2" maxlength="2" tabindex="2" onkeyup="handleInput(this);" onchange="handleInput(this);" <?php echo $disable; ?> /><label for="<?php echo $f_id; ?>-1">Cents</label></span><?php
    }
    protected function print_fieldNumber($args) {
        $f_id = $args['f_id'];
        $field = $args['field'];
        $fieldId = $args['fieldId'];
        $fieldPrefix = $args['fieldPrefix'];
        $f_req = $args['f_req'];
        $disable = ($args['disabled'])?' readonly="readonly" disabled="disabled"':'';
        ?><div><input id="<?php echo $f_id; ?>" name="<?php echo $f_id; ?>" type="text" class="field text nospin <?php echo $field['Size']; ?>" value="<?php echo $field['DefaultVal']; ?>" tabindex="<?php echo $field['DisplayPos']; ?>" onkeyup="handleInput(this); " onchange="handleInput(this);" <?php echo $f_req; echo $disable; ?> /><?php
            if(!empty($field['RangeMax'])) :
            ?><label for="<?php echo $f_id; ?>">Enter a number between <var id="<?php echo $fieldPrefix; ?>rangeMinMsg<?php echo $fieldId; ?>"><?php echo $field['RangeMin']; ?></var> and <var id="<?php echo $fieldPrefix; ?>rangeMaxMsg<?php echo $fieldId; ?>"><?php echo $field['RangeMax']; ?></var>.</label><?php
            endif;
            ?></div><?php
    }
    protected function print_fieldSection($args) {
        $fieldTitle = $args['fieldTitle'];
        $fieldId = $args['fieldId'];
        $fieldPrefix = $args['fieldPrefix'];
        $title_id = $args['title_id'];
        ?><div><section><h3 id="<?php echo $title_id; ?>"><?php echo $fieldTitle; ?></h3><div id="<?php echo $fieldPrefix; ?>instruct<?php echo $fieldId; ?>"><?php echo $this->helpers->str_detectLink($fieldInstruction); ?></div></section></div>
        <?php
    }
    protected function print_fieldLikert($args) {
        $field = $args['field'];
        $choices = $args['choices'];
        $fieldPrefix = $args['fieldPrefix'];
        $subFields = $args['subFields'];
        $disable = ($args['disabled'])?' readonly="readonly" disabled="disabled"':'';
        if(!empty($subFields) && !empty($choices)) {
            $validation = $field['Validation'];
            ?><table cellspacing="0"><caption id="<?php echo $title_id; ?>"><?php echo $fieldTitle; echo $tf_req; ?></caption><thead><tr><th>&nbsp;</th><?php
                    foreach($choices as $choice) {
                        ?><td><?php echo $choice['Choice']; ?></td><?php
                    }
                    ?></tr></thead><tbody><?php
            $i = 0;
            foreach($subFields as $subField) {
                $subFieldid = $subField['ColumnId'];
                $sf_id = $fieldPrefix.'Field'.$subFieldid;
                ?><tr class="statement<?php echo $subFieldid; if($i%2!=0) echo ' alt'; ?>"><?php
                    $j=0;
                    foreach($choices as $choice) {
                        if($j==0) {
                            if($validation == 'na' || empty($validation)) : ?><th><label for="<?php echo $sf_id; ?>"><?php echo $subField['ChoicesText']; ?></label></th><?php elseif($validation == 'dc'): ?><th><label><?php echo $subField['ChoicesText']; ?></label></th><?php endif; ?><?php
                        }
                        ?><td title="<?php echo $choice['Choice']; ?>"><?php if($validation == 'na' || empty($validation)) : 
                        ?><input <?php echo ($choice['IsDefault'])?'checked="checked"':''; ?> class="field" id="<?php echo $sf_id; ?>_<?php echo $j + 1; ?>" name="<?php echo $sf_id; ?>" type="radio" tabindex="<?php echo $j + 1; ?>" value="<?php echo $choice['Choice']; ?>" onchange="handleInput(this);" <?php echo $disable; ?> /><label for="<?php echo $sf_id; ?>_<?php echo $j + 1; ?>"><?php echo isset($choice['Score'])?$choice['Score']:''; ?></label><?php elseif($validation == 'dc'): ?><input <?php echo ($choice['IsDefault'])?'checked="checked"':''; ?> class="field" id="<?php echo $sf_id; ?>_<?php echo $j + 1; ?>" name="<?php echo $sf_id; ?>_<?php echo $j + 1; ?>" type="checkbox" tabindex="<?php echo $j + 1; ?>" value="<?php echo $choice['Choice']; ?>" onchange="handleInput(this);" <?php echo $disable; ?> /><?php endif; ?></td><?php
                        $j++;
                    }
                ?></tr><?php
                $i++;
            }   
            ?></tbody></table><?php
        }
    }
    /**
    *   *** FORM FIELD FUNCTION ***
    */
    protected function getFields($args) {
        $fields = $args['fields'];
        $errorFields = isset($args['errorFields'])?$args['errorFields']:array();
        $fieldPrefix = isset($args['fieldPrefix'])?$args['fieldPrefix']:'';
        $disabled = isset($args['disabled'])?$args['disabled']:true;
        $ajaxEnable = false;
        $posted_ids = array();
        $extra_options = isset($args['extra_options'])?$args['extra_options']:array();
        if(isset($extra_options['ajaxEnable'])) {
            $ajaxEnable = $extra_options['ajaxEnable'];
            $posted_ids = $extra_options['posted_ids'];
        }

        // short by position
        $this->helpers->aasort($fields,'Pos');

        // field args
        $fargs = array();
        $html = '';
        foreach($fields as $field) {
            $html .= $this->buildField(array(
                    'disabled' => $disabled,
                    'field' => $field,
                    'fieldId' => $field['ColumnId'], 
                    'fieldFormId' => '0', 
                    'type' => $field['Typeof'],
                    'isCreateField' => false,
                    'errorFields' => $errorFields,
                    'fieldPrefix' => $fieldPrefix,
                    'ajaxEnable' => $ajaxEnable,
                    'posted_ids' => $posted_ids,
                ));
        }
        return $html;
    }
    protected function fieldHtmlHead($args) {
        extract($args);
        ?><li id="<?php echo $title_id; ?>" class="<?php echo $classes; if(isset($errorFields[$fieldId])): echo ' error'; endif; ?>"><?php
            if($type == 'checkbox' || $type == 'radio') {
                ?><fieldset><legend id="<?php echo $title_id; ?>" class="desc"><?php echo $fieldTitle; echo $tf_req; ?></legend><?php
            } elseif($type == 'section' || $type == 'likert') {
            } else {
                ?><label class="desc" id="<?php echo $title_id; ?>" for="<?php echo $f_id; ?>"><?php echo $fieldTitle; echo $tf_req; ?></label><?php
            }
    }
    protected function fieldHtmlFoot($args) {
        extract($args);
        if(isset($errorFields[$fieldId])) {
                echo '<p class="error">'.$errorFields[$fieldId]['message'].'</p>';
            } 
            if(!empty($field['Instructions'])) {
                ?><p class="instruct" id="<?php echo $fieldPrefix; ?>instruct<?php echo $fieldId; ?>"><small><?php echo $fieldInstruction; ?></small></p><?php 
            }
            if($type == 'checkbox' || $type == 'radio') {
                ?></fieldset><?php
            }
        ?></li><?php
    }
    protected function buildField($args) {
        if(empty($args['type']) || empty($args['fieldId'])) return '';

        $type = $args['type'];
        $fieldId = $args['fieldId'];
        $field = $args['field'];
        $isCreateField = $args['isCreateField'];
        $fieldPrefix = isset($args['fieldPrefix'])?$args['fieldPrefix']:'';
        $errorFields = isset($args['errorFields'])?$args['errorFields']:'';
        $title_id = $fieldPrefix.'foli'.$field['FieldId'];
        $req_id = $fieldPrefix.'req_'.$field['FieldId'];
        $f_id = $fieldPrefix.'Field'.$field['FieldId'];
        $f_req = $field['IsRequired']?'required':'';
        $tf_req = ($field['IsRequired'])?'<span id="'.$req_id.'" class="req"> * </span>':'';
        $disabled = $args['disabled'];
        $ajaxEnable = $args['ajaxEnable'];
        $posted_ids = $args['posted_ids'];

        $classes = 'notranslate ';
        $fieldTitle = $this->helpers->str_detectLink($field['Title']);
        $settings = $field['Settings'];
        $fieldInstruction = $this->helpers->str_detectLink($field['Instructions']);
        $subFields = $this->isset_get($field['SubFields'], array());
        $choices = $this->isset_get($field['Choices'], array());
        if($type == 'section') {
            $classes .= ' section';
        }
        if($type == 'likert') {
            $classes .= ' col4 likert';
        }
        if(isset($settings['hideNumbers']) || in_array('hideNumbers',$settings)) {
            $classes .= ' hideNumbers';
        }
        if(isset($field['ClassNames'])) {
            $classes .= ' ' . $field['ClassNames'];
        }
        if($type == 'date' || $type == 'eurodate') {
            $classes .= ' '.$type;
        } elseif($type == 'time') {
            $classes .= ' time';
        } elseif($type == 'phone') {
            $classes .= ' phone';
        } elseif($type == 'address') {
            $classes .= ' complex';
        }
        // get html content
        if($isCreateField) {
            $field['FieldId'] = $fieldId;
            $field['ColumnId'] = $fieldId;
            $field['Title'] = ucfirst($type);
            $field['Typeof'] = $type;
            if($type == 'text') {
                $field['Excerpt'] = '0';
            } elseif($type == 'number') {
            } elseif($type == 'textarea') {
            } elseif($type == 'checkbox') {
                $field['SubFields'] = array(
                    array (
                        'Title' => 'First Choice',
                        'ColumnId' => $fieldId,
                        'FieldId' => $fieldId,
                        'ChoicesText' => 'First Choice',
                        'DefaultVal' => '0',
                        'IsRight' => '1',
                        'Price' => '0'
                    ), array (
                        'Title' => 'Second Choice',
                        'ColumnId' => ''.($fieldId+1),
                        'FieldId' => ''.($fieldId+1),
                        'ChoicesText' => 'Second Choice',
                        'DefaultVal' => '0',
                        'IsRight' => '1',
                        'Price' => '0'
                    ), array (
                        'Title' => 'Third Choice',
                        'ColumnId' => ''.($fieldId+2),
                        'FieldId' => ''.($fieldId+2),
                        'ChoicesText' => 'Third Choice',
                        'DefaultVal' => '0',
                        'IsRight' => '1',
                        'Price' => '0'
                    )
                );
            } elseif($type == 'radio') {
                $field['Choices'] = array(
                    array(
                        'FormId' => $fieldFormId,
                        'ColumnId' => ''.($fieldId+0),
                        'ChoiceId' => '0',
                        'StaticId' => '0',
                        'Choice' => 'First Choice',
                        'IsDefault' => '1',
                        'IsRight' => '1',
                        'Price' => '0',
                        'Score' => 0
                    ), array(
                        'FormId' => $fieldFormId,
                        'ColumnId' => ''.($fieldId+1),
                        'ChoiceId' => '0',
                        'StaticId' => '0',
                        'Choice' => 'Second Choice',
                        'IsDefault' => '0',
                        'IsRight' => '1',
                        'Price' => '0',
                        'Score' => 0
                    ), array(
                        'FormId' => $fieldFormId,
                        'ColumnId' => ''.($fieldId+2),
                        'ChoiceId' => '0',
                        'StaticId' => '0',
                        'Choice' => 'Third Choice',
                        'IsDefault' => '0',
                        'IsRight' => '1',
                        'Price' => '0',
                        'Score' => 0
                    )
                );
            } elseif($type == 'select') {
                $field['Choices'] = array(
                    array(
                        'FormId' => $fieldFormId,
                        'ColumnId' => '',
                        'ChoiceId' => '0',
                        'StaticId' => '0',
                        'Choice' => '',
                        'IsDefault' => '1',
                        'IsRight' => '1',
                        'Price' => '0',
                        'Score' => 0
                    ), array(
                        'FormId' => $fieldFormId,
                        'ColumnId' => ''.($fieldId+0),
                        'ChoiceId' => '0',
                        'StaticId' => '0',
                        'Choice' => 'First Choice',
                        'IsDefault' => '0',
                        'IsRight' => '1',
                        'Price' => '0',
                        'Score' => 0
                    ), array(
                        'FormId' => $fieldFormId,
                        'ColumnId' => ''.($fieldId+1),
                        'ChoiceId' => '0',
                        'StaticId' => '0',
                        'Choice' => 'Second Choice',
                        'IsDefault' => '0',
                        'IsRight' => '1',
                        'Price' => '0',
                        'Score' => 0
                    ), array(
                        'FormId' => $fieldFormId,
                        'ColumnId' => ''.($fieldId+2),
                        'ChoiceId' => '0',
                        'StaticId' => '0',
                        'Choice' => 'Third Choice',
                        'IsDefault' => '0',
                        'IsRight' => '1',
                        'Price' => '0',
                        'Score' => 0
                    )
                );
            } elseif($type == 'section') {
                $field['DisplayPos'] = '1';
            } elseif($type == 'likert') {
                $field['Choices'] = array(
                    array(
                        'FormId' => $fieldFormId,
                        'ColumnId' => '0',
                        'ChoiceId' => '0',
                        'StaticId' => '0',
                        'Choice' => 'Strongly Disagree',
                        'IsDefault' => '0',
                        'IsRight' => '1',
                        'Price' => '0',
                        'Score' => 1
                    ), array(
                        'FormId' => $fieldFormId,
                        'ColumnId' => '1',
                        'ChoiceId' => '0',
                        'StaticId' => '0',
                        'Choice' => 'Disagree',
                        'IsDefault' => '0',
                        'IsRight' => '1',
                        'Price' => '0',
                        'Score' => 2
                    ), array(
                        'FormId' => $fieldFormId,
                        'ColumnId' => '2',
                        'ChoiceId' => '0',
                        'StaticId' => '0',
                        'Choice' => 'Agree',
                        'IsDefault' => '0',
                        'IsRight' => '1',
                        'Price' => '0',
                        'Score' => 3
                    ), array(
                        'FormId' => $fieldFormId,
                        'ColumnId' => '3',
                        'ChoiceId' => '0',
                        'StaticId' => '0',
                        'Choice' => 'Strongly Agree',
                        'IsDefault' => '0',
                        'IsRight' => '1',
                        'Price' => '0',
                        'Score' => 4
                    )
                );
                $this->json_['SubFields'] = array(
                    array(
                        'Title' => 'Statement One',
                        'ColumnId' => ($fieldId),
                        'FieldId' => '0',
                        'ChoicesText' => 'Statement One',
                        'DefaultVal' => '0',
                        'Price' => '0'
                    ), array(
                        'Title' => 'Statement Two',
                        'ColumnId' => ''.($fieldId+1),
                        'FieldId' => '0',
                        'ChoicesText' => 'Statement Two',
                        'DefaultVal' => '0',
                        'Price' => '0'
                    ), array(
                        'Title' => 'Statement Three',
                        'ColumnId' => ''.($fieldId+2),
                        'FieldId' => '0',
                        'ChoicesText' => 'Statement Three',
                        'DefaultVal' => '0',
                        'Price' => '0'
                    )
                );
            } elseif($type == 'shortname') {
                $field['SubFields'] = array(array(
                    'Title' => 'First',
                    'ColumnId' => ($fieldId),
                    'FieldId' => ($fieldId),
                    'ChoicesText' => 'First',
                    'DefaultVal' => '',
                    'Price' => '0'
                ), array(
                    'Title' => 'Last',
                    'ColumnId' => ''.($fieldId+1),
                    'FieldId' => ''.($fieldId+1),
                    'ChoicesText' => 'Last',
                    'DefaultVal' => '',
                    'Price' => '0'
                ));
            } elseif($type == 'address') {
                $field['SubFields'] = array(array(
                    'Title' => 'Street Address',
                    'ColumnId' => $fieldId,
                    'FieldId' => $fieldId,
                    'ChoicesText' => 'Street Address',
                    'DefaultVal' => '',
                    'Price' => '0'
                ), array(
                    'Title' => 'Address Line 2',
                    'ColumnId' => ''.($fieldId+1),
                    'FieldId' => ''.($fieldId+1),
                    'ChoicesText' => 'Address Line 2',
                    'DefaultVal' => '',
                    'Price' => '0'
                ), array(
                    'Title' => 'City',
                    'ColumnId' => ''.($fieldId+2),
                    'FieldId' => ''.($fieldId+2),
                    'ChoicesText' => 'City',
                    'DefaultVal' => '',
                    'Price' => '0'
                ), array(
                    'Title' => 'State \/ Province \/ Region',
                    'ColumnId' => ''.($fieldId+3),
                    'FieldId' => ''.($fieldId+3),
                    'ChoicesText' => 'State \/ Province \/ Region',
                    'DefaultVal' => '',
                    'Price' => '0'
                ), array(
                    'Title' => 'Postal \/ Zip Code',
                    'ColumnId' => ''.($fieldId+4),
                    'FieldId' => ''.($fieldId+4),
                    'ChoicesText' => 'Postal \/ Zip Code',
                    'DefaultVal' => '',
                    'Price' => '0'
                ), array(
                    'Title' => 'Country',
                    'ColumnId' => ''.($fieldId+5),
                    'FieldId' => ''.($fieldId+5),
                    'ChoicesText' => 'Country',
                    'DefaultVal' => '',
                    'Price' => '0'
                ));
            } elseif($type == 'date') {
            } elseif($type == 'email') {
            } elseif($type == 'time') {
            } elseif($type == 'phone') {
            } elseif($type == 'url') {
            } elseif($type == 'money') {
            }
        }
        $fargs = array(
                'field' => $field,
                'fieldId' => $field['FieldId'],
                'fieldPrefix' => $fieldPrefix,
                'fieldTitle' => $field['Title'],
                'subFields' => $subFields,
                'choices' => $choices,
                'title_id' => $title_id,
                'req_id' => $req_id,
                'f_id' => $f_id,
                'f_req' => $f_req,
                'tf_req' => $tf_req,
                'disabled' => $disabled,
            );
        $notPosted = true;
        $postedValue = '';
        if($ajaxEnable) {
            if(isset($posted_ids['Field'.$fieldId])) {
                $classes .= ' saved';
                $postedValue = $posted_ids['Field'.$fieldId];
                $notPosted = false;
                $disabled_value = true;
            }
            if($type == 'checkbox') {
                $postedValue = array();
                foreach($field['SubFields'] as $v) {
                    if(isset($posted_ids['Field'.($v['ColumnId'])]) && !empty($posted_ids['Field'.($v['ColumnId'])])) {
                        $postedValue[] = $posted_ids['Field'.$v['ColumnId']];
                    }
                }
                $postedValue = implode(' | ', $postedValue);
            }
        }
        ob_start();
        // get html header
        $this->fieldHtmlHead(array(
            'classes' => $classes,
            'type' => $type,
            'fieldId' => $fieldId,
            'fieldTitle' => $fieldTitle,
            'title_id' => $title_id,
            'f_id' => $f_id,
            'tf_req' => $tf_req,
            'errorFields' => $errorFields,
            ));
        if($notPosted) {
            // get html content
            $fieldMethod = 'print_field'.ucfirst($type);
            if(is_callable(array($this, $fieldMethod))) {
                $this->$fieldMethod($fargs);
                if(isset($field['ExtraFields']) && !empty($field['ExtraFields'])) {
                ?>
                <div class="extra_fields">
                    <ul>
                    <?php
                        echo $this->getFields(array(
                            'fields' => $field['ExtraFields'],
                            'errorFields' => array(),
                            'fieldPrefix' => 'extra_',
                            'extra_options' => array()));
                    ?>
                    </ul>
                </div>
                <?php
                }
            }
        }
        if($ajaxEnable) {
            if($notPosted) {
            ?>
                <div class="submit_place">
                    <div class="right">
                        <a href="#" class="send button">Send</a>
                    </div>
                </div>
            <?php
            } else {
            ?>
                <div class="message success"><?php echo $postedValue; ?></div>
            <?php
            }
        }
        // get html footer
        $this->fieldHtmlFoot(array(
            'fieldId' => $fieldId,
            'type' => $type,
            'fieldInstruction' => $fieldInstruction,
            'errorFields' => $errorFields,
            ));

        $html = ob_get_clean();
        if($isCreateField) {
            $this->_json = $field;
        }
        return $html;
    }
    protected function doubleField($field, $fieldId) {
        $field_ = $this->helpers->jd2a($field);
        $field_['ColumnId'] = $fieldId;
        if(isset($field_['ExtraFields'])) $field_['ExtraFields'] = array();
        return $field_;
    }
    protected function scoring($posts, $fields) {
        $rightFieldCount = 0;
        $point = 0;
        $others = array();
        foreach($fields as $field) {
            $type = $field['Typeof'];
            if($type == 'checkbox') {
                $i=$j=0;
                $rightFieldCount++;
                foreach($field['SubFields'] as $subF) {
                    $post_value = isset($posts['Field'.$subF['ColumnId']])?$posts['Field'.$subF['ColumnId']]:'';
                    if(!empty($post_value)) {
                        $j++;
                        if($subF['IsRight']) {
                            $i++;
                        }
                    }
                    if($subF['IsRight'] && !empty($post_value)) {
                        $i++;
                        if(!empty($post_value)) {
                            $j++;
                        }
                    }
                }
                if($i==$j && $i != 0) {
                    $point++;
                }
            } elseif($type == 'likert') {
                $validation = isset($field['Validation'])?$field['Validation']:'';
                $post_values = '';
                if($validation == 'na' || empty($validation)) {
                    foreach($field['SubFields'] as $subF) {
                        $rightFieldCount++;
                        $j=$i=0;
                        $post_value = isset($posts['Field'.$subF['ColumnId']])?$posts['Field'.$subF['ColumnId']]:'';
                        foreach($field['Choices'] as $k => $choiC) {
                            if($choiC['IsRight']) {
                                $j++;
                                if(!empty($post_value) && $post_value == $choiC['Choice']) {
                                    $i++;
                                    break;
                                }
                            }
                        }
                        if($i!=0) {
                            $point++;
                        }
                    }
                } elseif($validation == 'dc') {
                    foreach($field['SubFields'] as $subF) {
                        $rightFieldCount++;
                        $j=$i=0;
                        foreach($field['Choices'] as $k => $choiC) {
                            $post_value = $posts['Field'.$subF['ColumnId'].'_'.($k+1)];
                            if(!empty($post_value)) {
                            
                                $j++;
                                if($subF['IsRight']) {
                                    $i++;
                                }
                            }
                            if($subF['IsRight'] && !empty($post_value)) {
                                $i++;
                                if(!empty($post_value)) {
                                    $j++;
                                }
                            }
                        }
                        if($i==$j) {
                            $point++;
                        }
                    }
                }
            } elseif($type == 'select' || $type == 'radio') {
                $rightFieldCount++;
                $rightInput = isset($field['InputRightAnswer'])?$field['InputRightAnswer']:'';
                $rightInput = $this->analizeInput($rightInput);
                $post_value = isset($posts['Field'.$field['ColumnId']])?$posts['Field'.$field['ColumnId']]:'';
                $i=$h=0;
                foreach($field['Choices'] as $k => $choiC) {
                    if($choiC['IsRight']) {
                        if($type == 'radio') {
                            if($choiC['Choice'] == 'Other') {
                                $post_value_other = isset($posts['Field'.$field['ColumnId'].'_other_Other'])?$posts['Field'.$field['ColumnId'].'_other_Other']:'';
                                $post_value_other = $this->stripslashes_for_old_php_mysql($post_value_other);
                                if(!empty($post_value_other)) {
                                    foreach($rightInput as $ri)  {
                                       if(trim($post_value_other) == $ri) {
                                            $i++;
                                            break;
                                        } 
                                    }
                                }
                            } elseif(!empty($post_value) && $post_value == $choiC['Choice']) {
                                $i++;
                                break;
                            }
                        } elseif($type == 'select') {
                            if(!empty($post_value) && $post_value == $choiC['Choice']) {
                                $i++;
                                break;
                            }
                        }
                    }
                }
                if($i!=0) {
                    $point++;
                }
            } elseif($type == 'text') {
                $rightFieldCount++;
                $i=0;
                $rightInput = isset($field['InputRightAnswer'])?$field['InputRightAnswer']:'';
                if(!empty($rightInput)) {
                    $rightInput = $this->analizeInput($rightInput);
                    
                    $post_value = isset($posts['Field'.$field['ColumnId']])?$posts['Field'.$field['ColumnId']]:'';
                    $post_value = $this->stripslashes_for_old_php_mysql($post_value);
                    if(!empty($post_value)) {
                        foreach($rightInput as $ri)  {
                           if(trim($post_value) == $ri) {
                                $i++;
                                break;
                            } 
                        }
                    }
                }
                if($i!=0) {
                    $point++;
                }
            } else {
                $post_value = isset($posts['Field'.$field['ColumnId']])?$posts['Field'.$field['ColumnId']]:'';
                if(!empty($post_value) || $post_value === '0') {
                    $others[] = array(
                        'MaxValue' => isset($field['RangeMax'])?$field['RangeMax']:'10',
                        'MinValue' => isset($field['RangeMin'])?$field['RangeMin']:'0',
                        'Value' => $post_value
                    );
                }
            }
        }
        return array(
            'RightFieldCount' => $rightFieldCount,
            'point' => $point,
            'other_fields' => $others
        );
    }
    protected function validateFields($fields, $posts) {
        $errorFields = array();
        foreach($fields as $field) {
            $type = $field['Typeof'];
            if($type == 'checkbox') {
                if($field['IsRequired']) {
                    $i=count($field['SubFields']);
                    foreach($field['SubFields'] as $subF) {
                        if(empty($posts['Field'.$subF['ColumnId']])) {
                            $i--;
                        }
                    }
                    if($i==0) {
                        $errorFields[$field['ColumnId']] = array(
                            'message' => 'This field is required. Please enter a value.'
                        );
                    }
                }
            } elseif($type == 'likert' || $type == 'address') {
                $validation = isset($field['Validation'])?$field['Validation']:'';
                $post_values = '';
                if($field['IsRequired']) {
                    if($validation == 'na' || empty($validation)) {
                        foreach($field['SubFields'] as $subF) {
                            $post_value = $posts['Field'.$subF['ColumnId']];
                            if(empty($post_value)) {
                                $errorFields[$field['ColumnId']] = array(
                                    'message' => 'This field is required. Please enter a value.'
                                );
                                break;
                            }
                        }
                    } elseif($validation == 'dc') {
                        $i=count($field['SubFields']);
                        if($field['IsRequired']) {
                            foreach($field['SubFields'] as $subF) {
                                foreach($field['Choices'] as $k => $choiC) {
                                    $post_value = $posts['Field'.$subF['ColumnId'].'_'.($k+1)];
                                    if(!empty($post_value)) {
                                        $i--;
                                        break;
                                    }
                                }
                            }
                        }
                        if($i!=0) {
                            $errorFields[$field['ColumnId']] = array(
                                'message' => 'This field is required. Please enter a value.'
                            );
                        }
                    }
                }
            } else {
                $post_value = isset($posts['Field'.$field['ColumnId']])?$posts['Field'.$field['ColumnId']]:'';
                if($field['IsRequired']) {
                    if(empty($post_value) && $post_value !== '0') {
                        $errorFields[$field['ColumnId']] = array(
                            'message' => 'This field is required. Please enter a value.'
                        );
                    }
                }
                if(!empty($post_value)) {
                    $rangeMax = isset($field['RangeMax'])?$field['RangeMax']:'';
                    $rangeMin = isset($field['RangeMin'])?$field['RangeMin']:'0';
                    $rangeType = isset($field['RangeType'])?$field['RangeType']:'';
                    if($type == 'date' || $type == 'eurodate') {
                        $dates = isset($posts['Field'.$field['ColumnId'].'-1'])?$posts['Field'.$field['ColumnId'].'-1']:'';
                        $dates .= '/';
                        $dates .= isset($posts['Field'.$field['ColumnId'].'-2'])?$posts['Field'.$field['ColumnId'].'-2']:'';
                        $dates .= '/';
                        $dates .= $post_value;
                        if(!$this->helpers->validate_date($dates,$type)) {
                            $errorFields[$field['ColumnId']] = array(
                                'message' => 'Please enter a valid date.'
                            );
                        }
                    } elseif($type == 'email') {
                        if(!$this->helpers->validate_email($post_value)) {
                            $errorFields[$field['ColumnId']] = array(
                                'message' => 'Please enter a valid email address.'
                            );
                        }
                    } elseif($type == 'url') {
                        if(!$this->helpers->validate_url($post_value)) {
                            $errorFields[$field['ColumnId']] = array(
                                'message' => 'Please enter a valid url in http://website.com format.'
                            );
                        }
                    } elseif($type == 'time') {
                        $times = $post_value . ':' . $posts['Field'.$field['ColumnId'].'-1'] . ':' . $posts['Field'.$field['ColumnId'].'-2'] . ' ' . $posts['Field'.$field['ColumnId'].'-3'];
                        if(!$this->helpers->validate_time($times)) {
                            $errorFields[$field['ColumnId']] = array(
                                'message' => 'Please enter a valid time format.'
                            );
                        }
                    } elseif($type == 'phone') {
                        $phone = $post_value . $posts['Field'.$field['ColumnId'].'-1'] . $posts['Field'.$field['ColumnId'].'-2'];
                        if(!is_numeric($phone) || strlen($phone) < 9) {
                            $errorFields[$field['ColumnId']] = array(
                                'message' => 'Please enter a valid phone.'
                            );
                        }
                    } elseif($type == 'number') {
                        if(!is_numeric($post_value)) {
                            $errorFields[$field['ColumnId']] = array(
                                'message' => 'This field is required number.'
                            );
                        } elseif(!empty($rangeMax)) {
                            if($post_value > $rangeMax || $post_value < $rangeMin) {
                                $errorFields[$field['ColumnId']] = array(
                                    'message' => 'Enter a number between '.$rangeMin.' and '.$rangeMax.'.'
                                );
                            }
                        }
                    } elseif($type == 'text') {
                        $count = 0;
                        if($rangeType == 'words') {
                            $words = explode(' ',$post_value);
                            $count = count($words);
                        } elseif($rangeType == 'characters') {
                            $count = strlen($post_value);
                        }
                        if(!empty($rangeMax)) {
                            if($count > $rangeMax) {
                                $errorFields[$field['ColumnId']] = array(
                                    'message' => 'Maximum Allowed: '.$rangeMax.' '.$rangeType.'. Currently Used: '.$count.' '.$rangeType.'.'
                                );
                            }
                        }
                    }
                }
            }
            
        }
        return $errorFields;
    }
    protected function getFieldById($fields, $fieldId) {
        foreach($fields as $field) {
            if($field['ColumnId'] == $fieldId) {
                return array('0' => $field);
            }
        }
        return null;
    }
    protected function getFieldEmailConfirm($fields) {
        foreach($fields as $field) {
            if($field['Typeof'] == 'email') {
                if($field['Validation'] == 'email') {
                    return $field;
                }
            }    
        }
        return null;
    }
    /**
    *   *** COMMON FUNCTIONS ***
    */
    protected function common_listCountries() {
        ob_start();
        ?>
        <option value="" selected="selected"></option><option value="United States"> United States </option><option value="United Kingdom"> United Kingdom </option><option value="Australia"> Australia </option><option value="Canada"> Canada </option><option value="France"> France </option><option value="New Zealand"> New Zealand </option><option value="India"> India </option><option value="Brazil"> Brazil </option><option value="----"> ---- </option><option value="Afghanistan"> Afghanistan </option><option value="Åland Islands"> Åland Islands </option><option value="Albania"> Albania </option><option value="Algeria"> Algeria </option><option value="American Samoa"> American Samoa </option><option value="Andorra"> Andorra </option><option value="Angola"> Angola </option><option value="Anguilla"> Anguilla </option><option value="Antarctica"> Antarctica </option><option value="Antigua and Barbuda"> Antigua and Barbuda </option><option value="Argentina"> Argentina </option><option value="Armenia"> Armenia </option><option value="Aruba"> Aruba </option><option value="Austria"> Austria </option><option value="Azerbaijan"> Azerbaijan </option><option value="Bahamas"> Bahamas </option><option value="Bahrain"> Bahrain </option><option value="Bangladesh"> Bangladesh </option><option value="Barbados"> Barbados </option><option value="Belarus"> Belarus </option><option value="Belgium"> Belgium </option><option value="Belize"> Belize </option><option value="Benin"> Benin </option><option value="Bermuda"> Bermuda </option><option value="Bhutan"> Bhutan </option><option value="Bolivia"> Bolivia </option><option value="Bosnia and Herzegovina"> Bosnia and Herzegovina </option><option value="Botswana"> Botswana </option><option value="British Indian Ocean Territory"> British Indian Ocean Territory </option><option value="Brunei Darussalam"> Brunei Darussalam </option><option value="Bulgaria"> Bulgaria </option><option value="Burkina Faso"> Burkina Faso </option><option value="Burundi"> Burundi </option><option value="Cambodia"> Cambodia </option><option value="Cameroon"> Cameroon </option><option value="Cape Verde"> Cape Verde </option><option value="Cayman Islands"> Cayman Islands </option><option value="Central African Republic"> Central African Republic </option><option value="Chad"> Chad </option><option value="Chile"> Chile </option><option value="China"> China </option><option value="Colombia"> Colombia </option><option value="Comoros"> Comoros </option><option value="Democratic Republic of the Congo"> Democratic Republic of the Congo </option><option value="Republic of the Congo"> Republic of the Congo </option><option value="Cook Islands"> Cook Islands </option><option value="Costa Rica"> Costa Rica </option><option value="Côte dIvoire"> Côte dIvoire </option><option value="Croatia"> Croatia </option><option value="Cuba"> Cuba </option><option value="Cyprus"> Cyprus </option><option value="Czech Republic"> Czech Republic </option><option value="Denmark"> Denmark </option><option value="Djibouti"> Djibouti </option><option value="Dominica"> Dominica </option><option value="Dominican Republic"> Dominican Republic </option><option value="East Timor"> East Timor </option><option value="Ecuador"> Ecuador </option><option value="Egypt"> Egypt </option><option value="El Salvador"> El Salvador </option><option value="Equatorial Guinea"> Equatorial Guinea </option><option value="Eritrea"> Eritrea </option><option value="Estonia"> Estonia </option><option value="Ethiopia"> Ethiopia </option><option value="Faroe Islands"> Faroe Islands </option><option value="Fiji"> Fiji </option><option value="Finland"> Finland </option><option value="Gabon"> Gabon </option><option value="Gambia"> Gambia </option><option value="Georgia"> Georgia </option><option value="Germany"> Germany </option><option value="Ghana"> Ghana </option><option value="Gibraltar"> Gibraltar </option><option value="Greece"> Greece </option><option value="Grenada"> Grenada </option><option value="Guatemala"> Guatemala </option><option value="Guinea"> Guinea </option><option value="Guinea-Bissau"> Guinea-Bissau </option><option value="Guyana"> Guyana </option><option value="Haiti"> Haiti </option><option value="Honduras"> Honduras </option><option value="Hong Kong"> Hong Kong </option><option value="Hungary"> Hungary </option><option value="Iceland"> Iceland </option><option value="Indonesia"> Indonesia </option><option value="Iran"> Iran </option><option value="Iraq"> Iraq </option><option value="Ireland"> Ireland </option><option value="Israel"> Israel </option><option value="Italy"> Italy </option><option value="Jamaica"> Jamaica </option><option value="Japan"> Japan </option><option value="Jordan"> Jordan </option><option value="Kazakhstan"> Kazakhstan </option><option value="Kenya"> Kenya </option><option value="Kiribati"> Kiribati </option><option value="North Korea"> North Korea </option><option value="South Korea"> South Korea </option><option value="Kuwait"> Kuwait </option><option value="Kyrgyzstan"> Kyrgyzstan </option><option value="Laos"> Laos </option><option value="Latvia"> Latvia </option><option value="Lebanon"> Lebanon </option><option value="Lesotho"> Lesotho </option><option value="Liberia"> Liberia </option><option value="Libya"> Libya </option><option value="Liechtenstein"> Liechtenstein </option><option value="Lithuania"> Lithuania </option><option value="Luxembourg"> Luxembourg </option><option value="Macedonia"> Macedonia </option><option value="Madagascar"> Madagascar </option><option value="Malawi"> Malawi </option><option value="Malaysia"> Malaysia </option><option value="Maldives"> Maldives </option><option value="Mali"> Mali </option><option value="Malta"> Malta </option><option value="Marshall Islands"> Marshall Islands </option><option value="Mauritania"> Mauritania </option><option value="Mauritius"> Mauritius </option><option value="Mexico"> Mexico </option><option value="Micronesia"> Micronesia </option><option value="Moldova"> Moldova </option><option value="Monaco"> Monaco </option><option value="Mongolia"> Mongolia </option><option value="Montenegro"> Montenegro </option><option value="Morocco"> Morocco </option><option value="Mozambique"> Mozambique </option><option value="Myanmar"> Myanmar </option><option value="Namibia"> Namibia </option><option value="Nauru"> Nauru </option><option value="Nepal"> Nepal </option><option value="Netherlands"> Netherlands </option><option value="Netherlands Antilles"> Netherlands Antilles </option><option value="Nicaragua"> Nicaragua </option><option value="Niger"> Niger </option><option value="Nigeria"> Nigeria </option><option value="Norway"> Norway </option><option value="Oman"> Oman </option><option value="Pakistan"> Pakistan </option><option value="Palau"> Palau </option><option value="Palestine"> Palestine </option><option value="Panama"> Panama </option><option value="Papua New Guinea"> Papua New Guinea </option><option value="Paraguay"> Paraguay </option><option value="Peru"> Peru </option><option value="Philippines"> Philippines </option><option value="Poland"> Poland </option><option value="Portugal"> Portugal </option><option value="Puerto Rico"> Puerto Rico </option><option value="Qatar"> Qatar </option><option value="Romania"> Romania </option><option value="Russia"> Russia </option><option value="Rwanda"> Rwanda </option><option value="Saint Kitts and Nevis"> Saint Kitts and Nevis </option><option value="Saint Lucia"> Saint Lucia </option><option value="Saint Vincent and the Grenadines"> Saint Vincent and the Grenadines </option><option value="Samoa"> Samoa </option><option value="San Marino"> San Marino </option><option value="Sao Tome and Principe"> Sao Tome and Principe </option><option value="Saudi Arabia"> Saudi Arabia </option><option value="Senegal"> Senegal </option><option value="Serbia and Montenegro"> Serbia and Montenegro </option><option value="Seychelles"> Seychelles </option><option value="Sierra Leone"> Sierra Leone </option><option value="Singapore"> Singapore </option><option value="Slovakia"> Slovakia </option><option value="Slovenia"> Slovenia </option><option value="Solomon Islands"> Solomon Islands </option><option value="Somalia"> Somalia </option><option value="South Africa"> South Africa </option><option value="Spain"> Spain </option><option value="Sri Lanka"> Sri Lanka </option><option value="Sudan"> Sudan </option><option value="Suriname"> Suriname </option><option value="Swaziland"> Swaziland </option><option value="Sweden"> Sweden </option><option value="Switzerland"> Switzerland </option><option value="Syria"> Syria </option><option value="Taiwan"> Taiwan </option><option value="Tajikistan"> Tajikistan </option><option value="Tanzania"> Tanzania </option><option value="Thailand"> Thailand </option><option value="Togo"> Togo </option><option value="Tonga"> Tonga </option><option value="Trinidad and Tobago"> Trinidad and Tobago </option><option value="Tunisia"> Tunisia </option><option value="Turkey"> Turkey </option><option value="Turkmenistan"> Turkmenistan </option><option value="Tuvalu"> Tuvalu </option><option value="Uganda"> Uganda </option><option value="Ukraine"> Ukraine </option><option value="United Arab Emirates"> United Arab Emirates </option><option value="United States Minor Outlying Islands"> United States Minor Outlying Islands </option><option value="Uruguay"> Uruguay </option><option value="Uzbekistan"> Uzbekistan </option><option value="Vanuatu"> Vanuatu </option><option value="Vatican City"> Vatican City </option><option value="Venezuela"> Venezuela </option><option selected="selected" value="Vietnam"> Vietnam </option><option value="Virgin Islands, British"> Virgin Islands, British </option><option value="Virgin Islands, U.S."> Virgin Islands, U.S. </option><option value="Yemen"> Yemen </option><option value="Zambia"> Zambia </option><option value="Zimbabwe"> Zimbabwe </option>
        <?php
        return ob_get_clean();
    }
    protected function common_getGroupsAsOption() {
        $page_limit = 30;
        if(isset($this->configs['display']['groups']['page_limit'])) {
            $page_limit = $this->configs['display']['groups']['page_limit'];
        }
        $paged = $this->routers['paged'];
        $limit = (($paged - 1)*$page_limit).','.$page_limit;
        list($groups, $total) = $this->groups_model->getGroupsRP("SELECT SQL_CALC_FOUND_ROWS *, count(b.user_id) as user_count  FROM `{P}groups` a LEFT JOIN `{P}groups_users` b ON a.`group_id` = b.`group_id` GROUP BY a.`group_id` LIMIT $limit");
        ob_start();
        if(!empty($groups)) while ($group = $groups->fetch_assoc()) {
        ?><option value="<?php echo $group['group_id']; ?>"><?php 
            echo $group['group_name'] . ' ('.$group['user_count'].' users)'; 
        ?></option><?php
        }
        return ob_get_clean();
    }
    protected function common_print_listCwa($args) {
        $field_id = $args['field_id'];
        $field_title = $args['field_title'];
        $logs = $args['logs'];
        $show_child = $args['show_child'];
        $is_first_log = $args['is_first_log'];
        ?><li id="field_<?php echo $field_id; ?>">
            <h4 class="title"><span class="fieldTitle"><?php echo trim($field_title); ?></span><a class="add_wa" href="#">Add New Wrong Answer</a><div class="wronga"></div></h4>
            <ul class="child"><?php
                if($show_child) {
                    foreach($logs as $key => $log) {
                        $type = $log['type'];
                        $log_content = array();
                        $log_content = $this->helpers->jd2a($log['log_content']);
                        $log_value = $log['log_subject'];
                        if($type == 'checkbox') {
                            $field = $log['field'];
                            $field_ids = explode('-',$log_value);
                            $log_values = array();
                            foreach($field['SubFields'] as $subF) {
                                if(in_array($subF['ColumnId'],$field_ids)) {
                                    $log_values[] = $subF['ChoicesText'];
                                }
                            }
                            $log_value = implode(' | ', $log_values);
                        } elseif($log_content['type'] == 'radio' || $log_content['type'] == 'select') {
                            $field = $log_content['field'];
                            $log_value_id = explode('_',$log_value);
                            foreach($field['Choices'] as $ck => $choiC) {
                                if(isset($log_value_id[1])) if($log_value_id[1] == $ck) {
                                    $log_value = $choiC['Choice'];
                                    break;
                                }
                            }
                        }
                        ?>
                        <li id="log_<?php echo $log['log_id']; ?>">
                            <header class="heading"><span class="value"><?php echo $log_value; ?></span> <span class="hits">( <?php echo $log['log_hit']; ?> Hits ) </span><a href="#" class="add_comment">Add comments</a> <a href="#" class="del_wa delete">Delete Answer</a></header>
                            <div class="comments">
                                <ul>
                                    <?php
                                    if($log['comments']) foreach($log['comments'] as $comment) {
                                        $user = $comment['user'];
                                        $args = array(
                                                'comment' => $comment,
                                                'user' => $user,
                                            );
                                        $this->common_print_listComment($args);
                                    }
                                    ?>
                                </ul>
                                <?php
                                if($log['total_comments'] > 3) {
                                    ?>
                                    <div class="view_comments"><a onclick="loadComments(this); return false;" href="#">View More <b>(<?php echo $log['total_comments'] - 3; ?>)</b></a></div>
                                    <?php
                                }
                                if($is_first_log) {
                                    $is_first_log = false;
                                    ?>
                                    <div class="comment_wrap">
                                        <form id="comment_form">
                                            <textarea id="comment_text" name="comment_text"></textarea>
                                            <button onclick="addComment(this); return false;" id="submitFormButton" name="submitFormButton">Comment</button>
                                        </form>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                        </li>
                        <?php
                    }
                }
            ?></ul>
        </li><?php
    }
    protected function common_print_listComment($args) {
        $comment = $args['comment'];
        $user = $args['user'];
        ?>
        <li id="comment_<?php echo $comment['comment_id']; ?>">
            <div class="avatar" href="#">
                <a class="image" target="_blank" href="https://vi.gravatar.com/site/signup/" title="Change Avatar">
                    <img width="50" src="<?php echo $this->helpers->get_gravatar($user['user_email']); ?>" />
                </a>
                <div class="info">
                    <?php
                    if($this->configs['display']['show_h2d']) {
                    ?>
                    <span class="user_h2d"><?php echo $user['h2d']; ?></span><br />
                    <?php
                    }
                    ?>
                    <span class="user_fullname"><?php echo $user['fullname']; ?></span>
                </div>
                
            </div>
            <div class="comment_content">
                <?php
                if($this->user['user_type'] == 'A') {
                ?>
                <div class="actions"><a class="delete" onclick="deleteComment(this); return false;" href="#">Delete</a></div>
                <?php
                }
                ?>
                <p><?php echo nl2br($comment['comment_content']); ?></p>
            </div>
        </li>
        <?php
    }
    protected function common_print_rowResult($args) {
        extract($args);
        $title = trim($title);
        ?>
        <tr class="form-row <?php if($isOdd) echo 'odd'; else echo 'even'; ?>">
            <th class="form-row-head">
                <?php echo $title; ?>
            </th>
            <td class="form-row-content">
                <?php
                if(is_array($value)) {
                    ?>
                    <ul class="form-row-list">
                        <?php foreach($value as $v) { ?>
                        <li><?php echo $v; ?></li>
                        <?php } ?>
                    </ul>
                    <?php
                } else {
                    ?>
                    <div>
                        <?php echo $value; ?>
                    </div>
                    <?php  
                }
                if(!empty($extra_values)) {
                    ?>
                    <div class="extras">
                        <ul>
                            <?php
                            foreach($extra_values as $extra_value) {
                                ?>
                                <li>
                                    <h5><?php echo $extra_value['title']; ?></h5>
                                    <?php
                                        if(is_array($extra_value['values'])) {
                                            echo implode(' | ',$extra_value['values']);
                                        } else {
                                            echo $extra_value['values'];
                                        }
                                    ?>
                                </li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div>
                    <?php
                }
                ?>
            </td>
            <?php
            if(!empty($answer)) {
                ?>
                <td class="<?php echo $atype; ?>">
                    <?php echo $answer; ?>
                </td>
                <?php  
            }
            ?>
        </tr>
        <?php
    }
    protected function common_print_formResult($args) {
        extract($args);
        ?>
        <table class="readonly form-result" cellspacing="0" cellpadding="0">
            <?php
            $i = 0;
            foreach($fields as $k => $field) {
                $values =  $titles = array();
                $extra_fields = isset($field['ExtraFields'])?$field['ExtraFields']:array();
                $extra_values = $this->common_getFieldValues($extra_fields,$entries,'extra_');
                $value = $title = $answer = '';
                $validation = isset($field['Validation'])?$field['Validation']:'';
                if(!empty($field['SubFields'])) {
                    $j = $h = $k = 0;
                    foreach($field['SubFields'] as $subF) {
                        if(isset($entries['Field'.$subF['ColumnId']])) {
                            if(!empty($entries['Field'.$subF['ColumnId']])) {
                                if($field['Typeof'] == 'likert') {
                                    $titles[] = $subF['ChoicesText'];
                                } else {
                                    $titles[] = $subF['Title'];
                                }
                                
                                $values[] = $entries['Field'.$subF['ColumnId']];
                            }
                        }
                        if($field['Typeof'] == 'checkbox') {
                            $post_value = isset($entries['Field'.$subF['ColumnId']])?$entries['Field'.$subF['ColumnId']]:'';
                            if(!empty($post_value)) {
                                $k++;
                                if($subF['IsRight']) {
                                    $h++;
                                }
                            }
                            if($subF['IsRight']) {
                                $h++;
                                if(!empty($post_value)) {
                                    $k++;
                                }
                            }
                        } elseif($field['Typeof'] == 'likert' && $field['Validation'] == 'dc') {
                            $titles[$j] = $subF['Title'];
                            foreach($field['Choices'] as $k1 => $choiC) {
                                
                                if(isset($entries['Field'.$subF['ColumnId'].'_'.($k1+1)])) {
                                    if(!empty($entries['Field'.$subF['ColumnId'].'_'.($k1+1)])) {
                                        $values[$j][] = $entries['Field'.$subF['ColumnId'].'_'.($k1+1)];
                                    }
                                } else {
                                    $values[$j][] = '';
                                }
                            }
                            $j++;
                        }
                    }
                    if($h==$k && $h != 0) {
                        $answer = isset($field['RightAnswerDescription'])?$field['RightAnswerDescription']:'';
                        if(empty($answer)) {
                            $answer = 'Right';
                        }
                        $atype = 'aright';
                    } else {
                        $answer = isset($field['WrongAnswerDescription'])?$field['WrongAnswerDescription']:'';
                        if(empty($answer)) {
                            $answer = 'Wrong';
                        }
                        $atype = 'awrong';
                    }
                } else {
                    $title = $field['Title'];
                    if(isset($entries['Field'.$field['ColumnId']])) {
                        $value = $entries['Field'.$field['ColumnId']];
                        if($field['Typeof'] == 'date' || $field['Typeof'] == 'eurodate') {
                            $value = $entries['Field'.$field['ColumnId'].(($field['Typeof'] == 'eurodate')?'-2':'-1')] .'/'.$entries['Field'.$field['ColumnId'].(($field['Typeof'] == 'eurodate')?'-1':'-2')].'/'.$entries['Field'.$field['ColumnId'].''] . ' (mm/dd/yyy)';
                        } elseif($field['Typeof'] == 'time') {
                            $value = $value . ':' . $entries['Field'.$field['ColumnId'].'-1'] . ':' . $entries['Field'.$field['ColumnId'].'-2'] . ' ' . $entries['Field'.$field['ColumnId'].'-3'];
                        } elseif($field['Typeof'] == 'shortname') {
                            $value = $value . ':' . $entries['Field'.$field['ColumnId'].'-1'] . ':' . $entries['Field'.$field['ColumnId'].'-2'] . ' ' . $entries['Field'.$field['ColumnId'].'-3'];
                        }
                    }
                }
                if($field['Typeof'] == 'select' || $field['Typeof'] == 'radio') {
                    $post_value = isset($entries['Field'.$field['ColumnId']])?$entries['Field'.$field['ColumnId']]:'';
                    $b=0;
                    $rightInput = isset($field['InputRightAnswer'])?$field['InputRightAnswer']:'';
                    $rightInput = $this->helpers->str_ex2a($rightInput,'|');
                    foreach($field['Choices'] as $k => $choiC) {
                        if($choiC['IsRight']) {
                            if($field['Typeof'] == 'radio') {
                                if($choiC['Choice'] == 'Other') {
                                    $post_value_other = isset($entries['Field'.$field['ColumnId'].'_other_Other'])?$entries['Field'.$field['ColumnId'].'_other_Other']:'';
                                    if(!empty($post_value_other)) {
                                        $value = $post_value_other;
                                        foreach($rightInput as $ri)  {
                                           if(trim($post_value_other) == $ri) {
                                                $b++;
                                                break;
                                            } 
                                        }
                                    }
                                } else {
                                    if(!empty($post_value) && $post_value == $choiC['Choice']) {
                                        $b++;
                                    }
                                }
                                
                            } elseif($field['Typeof'] == 'select') {
                                if(!empty($post_value) && $post_value == $choiC['Choice']) {
                                        $b++;
                                    }
                            }
                        }
                    }
                    if($b!=0) {
                        $answer = isset($field['RightAnswerDescription'])?$field['RightAnswerDescription']:'';
                        if(empty($answer)) {
                            $answer = 'Right';
                        }
                        $atype = 'aright';
                    } else {
                        $answer = isset($field['WrongAnswerDescription'])?$field['WrongAnswerDescription']:'';
                        if(empty($answer)) {
                            $answer = 'Wrong';
                        }
                        $atype = 'awrong';
                    }
                    if($review) {
                        if((!isset($_SESSION['admin_user']) && ( isset($_SESSION['user_normal']) && $_SESSION['user_normal'] != $user_id) ) && $atype == 'aright') {
                            $answer = $value = '****';
                        }
                    }
                } elseif($field['Typeof'] == 'likert') {
                    $ik = $i;
                    foreach($titles as $k => $title) {
                        if(is_array($values[$k])) {
                            $values[$k] = array_filter($values[$k]);
                        }
                        if(!empty($values[$k])) {
                            $rrArgs = array(
                                    'title' => $title,
                                    'value' => $value,
                                    'extra_values' => $extra_values,
                                    'answer' => $answer,
                                    'atype' => $atype,
                                    'isOdd' => ($ik % 2 == 0)?true:false
                                );
                            $this->common_print_rowResult($rrArgs);
                        }
                        $ik++;
                    }
                    $i = $ik;
                } elseif($field['Typeof'] == 'checkbox') {
                    if($review) {
                        if((!isset($_SESSION['admin_user']) && ( isset($_SESSION['user_normal']) && $_SESSION['user_normal'] != $user_id) ) && $atype == 'aright') {
                            $answer = '****';
                            $values = '****';
                        }
                    }
                } elseif($field['Typeof'] == 'shortname') {
                    $value = '';
                    foreach($titles as $k => $title) {
                        $value .= ' '.$values[$k];
                    }
                } elseif($field['Typeof'] == 'address') {
                    foreach($field['SubFields'] as $subF) {
                        $index = strtolower(str_replace(' ','-',$subF['Title']));
                        $values[] = isset($entries['Field'.$subF['ColumnId']])?$entries['Field'.$subF['ColumnId']]:'';
                    }
                    $values = array_filter($values);
                    $value = '';
                    $search = (isset($values[0])?$values[0]:'').'+'.(isset($values[1])?$values[1]:'').'+'.(isset($values[2])?$values[2]:'');
                    $value .= '<a href="http://maps.google.com/?q='.$search.'" style="text-decoration:none;" title="Show a Map of this Location" target="_blank">';
                        $value .= '<img width="16" height="16" style="padding:2px 0 0 0;float:left;" alt="" src="../images/icons/map.png" class="mapicon">';
                    $value .= '</a>';
                    $value .= '<address style="color:#333;font-style:normal;line-height:130%;padding:2px 0 2px 25px;" class="adr">';
                        
                        $value .= '<span class="street-address">'.(isset($values[0])?$values[0]:'').'</span>';
                        $value .= '<span class="extended-address"> '.(isset($values[1])?$values[1]:'').'</span><br>';           
                        $value .= '<span class="locality"> '.(isset($values[2])?$values[2]:'').'</span>';               
                        //$value .= '<span class="region">'.(isset($values[3])?$values[3]:'').'</span>';
                        //$value .= '<span class="postal-code">'.(isset($values[4])?$values[4]:'').'</span>';
                        //$value .= '<br>';             
                        //$value .= '<span class="country-name">'.(isset($values[5])?$values[5]:'').'</span>';
                        
                    $value .= '</address>';
                } elseif($field['Typeof'] == 'text') {
                    $b=0;
                    $rightInput = isset($field['InputRightAnswer'])?$field['InputRightAnswer']:'';
                    $rightInput = $this->helpers->str_ex2a($rightInput,'|');
                    if(!empty($value)) {
                        foreach($rightInput as $ri)  {
                           if(trim($value) == $ri) {
                                $b++;
                                break;
                            } 
                        }
                    }
                    if($b!=0) {
                        $answer = isset($field['RightAnswerDescription'])?$field['RightAnswerDescription']:'';
                        if(empty($answer)) {
                            $answer = 'Wrong';
                        }
                        $atype = 'aright';
                    } else {
                        $answer = isset($field['WrongAnswerDescription'])?$field['WrongAnswerDescription']:'';
                        if(empty($answer)) {
                            $answer = 'Wrong';
                        }
                        $atype = 'awrong';
                    }
                    if($review) {
                        if($atype == 'awrong') {
                            $ex = '';$exk = 0;
                            $ex .= '<p style="font-size: 12px; background: #BBF5BB;">';
                            foreach($rightInput as $ri)  {
                                $p  = (strripos($ri,$value));
                                if($p !== false) {
                                    //$wr = preg_replace('#[^'.$value.']#','*',$ri);
                                    $wr = str_replace($value,'````',$ri);
                                    $wr = preg_replace('#[^`]#','*',$wr);
                                    $wr = str_replace('````',$value,$wr);
                                    $ex .= '+ '.$wr . '<br>';
                                    $exk++;
                                }
                            }
                            $ex .= '</p>';
                            if($exk>0) $value .= $ex;
                            else $value .= '<p style="font-size: 12px; background: #FFCDCD;"> <strong>Không có gợi ý nào vì làm sai hoặc sai kí tự đầu </strong> </p>';
                        }
                        if((!isset($_SESSION['admin_user']) && ( isset($_SESSION['user_normal']) && $_SESSION['user_normal'] != $user_id) ) && $atype == 'aright') {
                            $answer = $value = '****';
                        }
                    }
                }
                if($field['Typeof'] != 'likert') {
                    $title = $this->helpers->str_detectLink($title);
                    $value = $this->helpers->str_detectLink($value);
                    $rrArgs = array(
                            'title' => $title,
                            'value' => $value,
                            'extra_values' => $extra_values,
                            'answer' => $answer,
                            'atype' => $atype,
                            'isOdd' => ($i % 2 == 0)?true:false
                        );
                    $this->common_print_rowResult($rrArgs);
                }
                $i++;
            }
            ?>
        </table>
        <?php
    }
    protected function common_getFormResult($args) {
        $default = array(
                'hideHeadInfo' => false,
                'hideHeader' => false,
                'review' => false,
                'entry' => array(),
                'form' => array(),
                'user' => array()
            );
        $args = array_merge($default, $args);
        extract($args);
        ob_start();
        $entry_id = $entry['entry_id'];
        $user_id = $entry['entry_user'];
        $entries = $this->helpers->jd2a($entry['entry_content']);
        $form_content = $this->helpers->jd2a($form['form_content']);
        $fields = $form_content['Fields'];
        if(!$hideHeader) {
            ?>
            <table class="form-result-head" cellspacing="0">
            <tr>
            <?php 
            if($entry['entry_type'] == 'AS') {
                $user_content = $this->helpers->jd2a($user['user_content']);
                if(!$review) {
                    if(!$hideHeadInfo) {
                        ?>
                        <td>Bài Nộp Của: <?php echo $user_content['Name']; ?><br />
                        <?php
                        if($this->options['show_nickh2d']) {
                        ?>
                        Nick H2d: <?php echo $user_content['NickH2d']; ?>
                        <?php
                        }
                        ?>
                        <br /></td>
                        <?php
                    } else {
                        ?>
                        <td><h3><strong>Form:</strong> <?php echo $form_content['Name']; ?></h3><br /></td>
                    
                        <?php
                    }
                } else {
                ?>
                    <td>Bài Nộp Của: <?php echo $user_content['Name']; ?><br />
                    <?php
                    if($this->options['show_nickh2d']) {
                    ?>
                    Nick H2d: <?php echo $user_content['NickH2d']; ?><br />
                    <?php
                    }
                    ?>
                    </td>
                    <td><h3><strong>Form:</strong> <?php echo $form_content['Name']; ?></h3><br /></td>
                <?php 
                }
            } 
            ?>
            </tr>
            </table>
            <?php
        }
        $this->common_print_formResult(array(
                'fields' => $fields,
                'entries' => $entries
            ));
        return ob_get_clean();
    }
    protected function common_getFieldValues($fields, $posts, $extra = '') {
        $fieldValues = array();
        foreach($fields as $field) {
            $type = $field['Typeof'];
            $values = array();
            if($type == 'checkbox') {
                foreach($field['SubFields'] as $subF) {
                    if(!empty($posts[$extra.'Field'.$subF['ColumnId']])) {
                        $values[] = $posts[$extra.'Field'.$subF['ColumnId']];
                    }
                }
                $fieldValues[] = array(
                    'field_id' => $field['ColumnId'],
                    'title' => $field['Title'],
                    'values' => $values
                );
            } elseif($type == 'likert' || $type == 'address') {
                $validation = isset($field['Validation'])?$field['Validation']:'';
                $post_values = '';
                if($validation == 'na' || empty($validation)) {
                    foreach($field['SubFields'] as $subF) {
                        $post_value = '';
                        if(!empty($posts[$extra.'Field'.$subF['ColumnId']])) {
                            $post_value = $posts[$extra.'Field'.$subF['ColumnId']];
                        }
                        $fieldValues[] = array(
                            'field_id' => $subF['ColumnId'],
                            'title' => $subF['Title'],
                            'values' => $post_value
                        );
                    }
                    
                } elseif($validation == 'dc') {
                    foreach($field['SubFields'] as $subF) {
                        foreach($field['Choices'] as $k => $choiC) {
                            $post_value = $posts[$extra.'Field'.$subF['ColumnId'].'_'.($k+1)];
                            if(!empty($post_value)) {
                                $values[] = $post_value;
                            }
                        }
                        $fieldValues[] = array(
                            'field_id' => $subF['ColumnId'],
                            'title' => $subF['Title'],
                            'values' => $values
                        );
                    }
                }
            } else {
                $post_value = isset($posts[$extra.'Field'.$field['ColumnId']])?$posts[$extra.'Field'.$field['ColumnId']]:'';
                if(!empty($post_value)) {
                    $rangeMax = isset($field['RangeMax'])?$field['RangeMax']:'';
                    $rangeMin = isset($field['RangeMin'])?$field['RangeMin']:'0';
                    $rangeType = isset($field['RangeType'])?$field['RangeType']:'';
                    if($type == 'date' || $type == 'eurodate') {
                        $dates = isset($posts[$extra.'Field'.$field['ColumnId'].'-1'])?$posts[$extra.'Field'.$field['ColumnId'].'-1']:'';
                        $dates .= '/';
                        $dates .= isset($posts[$extra.'Field'.$field['ColumnId'].'-2'])?$posts[$extra.'Field'.$field['ColumnId'].'-2']:'';
                        $dates .= '/';
                        $dates .= isset($posts[$extra.'Field'.$field['ColumnId']])?$posts[$extra.'Field'.$field['ColumnId']]:'';
                        $post_value = $dates;
                    } elseif($type == 'email') {
                    } elseif($type == 'url') {
                    } elseif($type == 'time') {
                        $post_values = $post_value . ':' . $posts[$extra.'Field'.$field['ColumnId'].'-1'] . ':' . $posts[$extra.'Field'.$field['ColumnId'].'-2'] . ' ' . $posts[$extra.'Field'.$field['ColumnId'].'-3'];
                        $post_value = $post_values;
                    } elseif($type == 'phone') {
                        $post_values = $post_value . $posts[$extra.'Field'.$field['ColumnId'].'-1'] . $posts[$extra.'Field'.$field['ColumnId'].'-2'];
                        $post_value= $post_values;
                    } elseif($type == 'number') {
                    } elseif($type == 'text') {
                    }
                }
                $fieldValues[] = array(
                    'field_id' => $field['ColumnId'],
                    'title' => $field['Title'],
                    'values' => $post_value
                );
            }
        }
        return $fieldValues;
    }
}