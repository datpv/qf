<?php
if ( ! defined( 'IA' ) ) exit;
/*
    @Name: Application Notfound Controller
    @Description: manage site notfound behaviors
    @Docs:
*/
class Applications_Controllers_Notfound extends Applications_Controller {
    function __construct($registry) {
        parent::__construct($registry);
    }
    function initiallize() {}
    function setConfigs() {}
    function index() {$this->view->render();}

}