<?php
if ( ! defined( 'IA' ) ) exit;
/*
    @Name: Application Groups Controller
    @Description: manage user group
    @Docs:
*/
class Applications_Controllers_Groups extends Applications_Controller {
    public 
        $groups_configs = array(
            'page_limit' => 4,
        )
        ;
    function __construct($registry) {
        parent::__construct($registry);
    }
    function initiallize() {
        $this->permission = 'Admin';
        parent::initiallize();
        // models
        $this->groups_model = new Applications_Models_Groups($this->registry);
        $this->groupsUsers_model = new Applications_Models_GroupsUsers($this->registry);
    }
    function setConfigs() {
        parent::setConfigs();
        if(isset($this->configs['display']['groups'])) {
            $this->groups_configs = $this->configs['display']['groups'];
        }
    }
    function index() {
        $page_limit = $this->fm_configs['page_limit'];
        $paged = $this->routers['paged'];
        $limit = (($paged - 1)*$page_limit).','.$page_limit;
        list($this->objects, $this->total) = $this->groups_model->getObjects(array(
                'select' => 'SQL_CACL_FOUND_ROWS *',
                'limit' => $limit
            ));
        $groups_result = array();
        if(!empty($this->objects)) 
            foreach($this->objects as $k => $group) {
                list($users, $total) = $this->groupsUsers_model->getObjects(array(
                    'where' => "`group_id` = '{$group['group_id']}'"));
                $group['user_group_meta'] = $users;
                $groups_result[$k] = $group;
            }
        $this->view->groups_result = $groups_result;
        $this->view->paginate = $this->view->getPaginate(array(
                'total' => $this->total,
                'paged' => $paged,
                'page_limit' => $page_limit,
                'base_url' => $this->url . 'groups/'
            ));
        $this->view->title = "Groups Manager";
        $this->view->render();
    }
}