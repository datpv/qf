<?php
if ( ! defined( 'IA' ) ) exit;
/*
    @Name: Application Email Confirm Controller
    @Description: manage login, logout
    @Docs:
*/
class Applications_Controllers_Confirm extends Applications_Controller {
    public 
        $confirm_configs = array(
            )
        ;
    function __construct($registry) {
        parent::__construct($registry);
    }
    function initiallize() {
        $this->permission = 'All';
        parent::initiallize();
        // models
        $this->entries_model = new Applications_Models_Entries($this->registry);
        $this->forms_model = new Applications_Models_Forms($this->registry);
    }
    function setConfigs() {
        parent::setConfigs();
        if(isset($this->configs['display']['confirm'])) {
            $this->confirm_configs = $this->configs['display']['confirm'];
        }
    }
    function index() {
        $routers = $this->getRouters();
        $complete = $dupl = false; $_form = array();$message = '';
        $entry_id = isset($_GET['a'])?$_GET['a']:'';
        $formId = $routers['pos_1'];
        if(!empty($entry_id) && strlen($entry_id)>32) {
            $entry_id = str_replace(substr($entry_id,-32),'',$entry_id);
            $entry_id = $this->helpers->str_escape($entry_id);
            $entry = $this->entries_model->getEntry(array(
                'where'=>"`entry_id` = '$entry_id' AND `entry_complete` = '0'"));
            if($entry) {
                $sucess = $entries_obj->updateEntry(array(
                    'updates' => "`entry_complete` = '1'",
                    'where' => "`entry_id` = '$entry_id'",
                    'limit' => '1'));
                if($sucess) $complete = true;
            } else {
                $dupl = true;
            }
        }
        if($dupl) {
            $this->notfound();
            exit;
        } else {
            $formId = $this->helpers->str_escape($formId);
            if(empty($formId) && !empty($entry_id)) {
                $entry = $this->entriesForms_model->getEF(array('where' => "`entry_id` = '$entry_id'"));
                $formId = $entry['form_id'];
                $message = 'Thanks for complete your submission!';
            }
            $form = $this->forms_model->getForm(array('where' => "`form_uuid` = '$formId'"));
            if(!$form) {
                $this->notfound();
                return;
            }
            $_form = $this->helpers->jd2a($form['form_content']);
            $fields = $_form['Fields'];
            $sendConfirm = $this->checkSendConfirm($fields);
            if(!empty($sendConfirm) && empty($message)) {
                $message = 'Please Confirm Your Submission By Check Your Email Inbox!';
            }
            $r_message = isset($_form['RedirectMessage'])?$_form['RedirectMessage']:'<h2>Great! Thanks for filling out my form!</h2>';
            
        }
        $this->view->complete = $complete;
        $this->view->message = $message;
        $this->view->r_message = $r_message;
        $this->view->title = 'Confirmation';
        $this->view->render();
    }
    function checkSendConfirm($fields) {
        foreach($fields as $field) {
            if($field['Typeof'] == 'email') {
                if($field['Validation'] == 'email') {
                    return $field;
                }
            }    
        }
        return '';
    }
}