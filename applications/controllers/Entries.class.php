<?php
if ( ! defined( 'IA' ) ) exit;
/*
    @Name: Application Entries Controller
    @Description: manage form entries
    @Docs:
*/
class Applications_Controllers_Entries extends Applications_Controllers_Fields {
    public 
        $entry_configs = array(
                'page_limit' => 6,
                'show_h2d' = true,
                'show_skype' = true,
            ),
        $category = array(
                    array(
                        'Category' => 'Integration',
                        'Typeof' => 'Amazon',
                        'SubFields' => array(array(
                            'Typeof' => 'text',
                            'ColumnId' => 'HitId',
                            'ChoicesText' => 'MTurk HitId',
                            'DefaultVal' => '0',
                            'Price' => '0'
                        ), array(
                            'Typeof' => 'text',
                            'ColumnId' => 'AssignmentId',
                            'ChoicesText' => 'MTurk AssignmentId',
                            'DefaultVal' => '0',
                            'Price' => '0'
                        ), array(
                            'Typeof' => 'text',
                            'ColumnId' => 'Environment',
                            'ChoicesText' => 'MTurk Environment',
                            'DefaultVal' => '0',
                            'Price' => '0'
                        )),
                        'IsAltField' => true
                    ), array(
                        'Category' => 'Integration',
                        'Typeof' => 'Payments',
                        'SubFields' => array(array(
                            'Typeof' => 'text',
                            'ColumnId' => 'Status',
                            'ChoicesText' => 'Payment Status',
                            'DefaultVal' => '0',
                            'Price' => '0'
                        ), array(
                            'Typeof' => 'text',
                            'ColumnId' => 'PurchaseTotal',
                            'ChoicesText' => 'Payment Total',
                            'DefaultVal' => '0',
                            'Price' => '0'
                        ), array(
                            'Typeof' => 'text',
                            'ColumnId' => 'Currency',
                            'ChoicesText' => 'Payment Currency',
                            'DefaultVal' => '0',
                            'Price' => '0'
                        ), array(
                            'Typeof' => 'text',
                            'ColumnId' => 'TransactionId',
                            'ChoicesText' => 'Payment Confirmation',
                            'DefaultVal' => '0',
                            'Price' => '0'
                        ), array(
                            'Typeof' => 'text',
                            'ColumnId' => 'MerchantType',
                            'ChoicesText' => 'Payment Merchant',
                            'DefaultVal' => '0',
                            'Price' => '0'
                        )),
                        'IsAltField' => true
                    ), array(
                        'Category' => 'System',
                        'Typeof' => 'System',
                        'SubFields' => array(array(
                            'Typeof' => 'text',
                            'ColumnId' => 'EntryId',
                            'ChoicesText' => 'Entry Id',
                            'DefaultVal' => '0',
                            'Price' => '0'
                        ), array(
                            'Typeof' => 'date',
                            'ColumnId' => 'DateCreated',
                            'ChoicesText' => 'Date Created',
                            'DefaultVal' => '0',
                            'Price' => '0'
                        ), array(
                            'Typeof' => 'text',
                            'ColumnId' => 'CreatedBy',
                            'ChoicesText' => 'Created By',
                            'DefaultVal' => '0',
                            'Price' => '0'
                        ), array(
                            'Typeof' => 'date',
                            'ColumnId' => 'DateUpdated',
                            'ChoicesText' => 'Last Updated',
                            'DefaultVal' => '0',
                            'Price' => '0'
                        ), array(
                            'Typeof' => 'text',
                            'ColumnId' => 'UpdatedBy',
                            'ChoicesText' => 'Updated By',
                            'DefaultVal' => '0',
                            'Price' => '0'
                        )),
                        'IsAltField' => true
                    ), array(
                        'Category' => 'Integration',
                        'Typeof' => 'IPAddresses',
                        'SubFields' => array(array(
                            'Typeof' => 'text',
                            'ColumnId' => 'IP',
                            'ChoicesText' => 'IP Address',
                            'DefaultVal' => '0',
                            'Price' => '0'
                        ), array(
                            'Typeof' => 'text',
                            'ColumnId' => 'LastPage',
                            'ChoicesText' => 'Last Page Accessed',
                            'DefaultVal' => '0',
                            'Price' => '0'
                        ), array(
                            'Typeof' => 'text',
                            'ColumnId' => 'CompleteSubmission',
                            'ChoicesText' => 'Completion Status',
                            'DefaultVal' => '0',
                            'Price' => '0'
                        )),
                        'IsAltField' => true
                    )
                    )
        ;
    function __construct($registry) {
        parent::__construct($registry);
    }
    function initiallize() {
        $this->permission = 'Admin';
        parent::initiallize();
        // models
        $this->entries_model = new Applications_Models_Entries($this->registry);
        $this->entriesForms_model = new Applications_Models_EntriesForms($this->registry);
        $this->forms_model = new Applications_Models_Forms($this->registry);
    }
    function setConfigs() {
        parent::setConfigs();
        if(isset($this->configs['display']['entry'])) {
            $this->entry_configs = $this->configs['display']['entry'];
        }
    }
    function index() {
        $formId = $this->helpers->str_escape($this->routers['pos_1']);
        $form = $this->forms_model->getForm(array('where' => "`form_uuid` = '$formId'"));
        if(empty($form)) {
            $this->notfound();
            exit();
        }
        $_form = $this->helpers->jd2a($form['form_content']);
        $fields = $_form['Fields'];
        $form_id = $form['form_id'];

        $page_limit = $this->entry_configs['page_limit'];
        $paged = $this->routers['paged'];
        $limit = (($paged - 1)*$page_limit).','.$page_limit;
        list($entries, $total) = $this->entries_model->getFullEntries(array(
            'form_id' => $form_id,
            'limit' => $limit));

        $this->forms_model->updateForm(array(
            'updates' => "`form_flag` = '$total'",
            'where' => "`form_id` = '$form_id'"));

        $_form['EntryCount'] = $total;
        $_form['FormId'] = $formId;
        $_form['Url'] = $formId;
        $catetory = $this->category;
        if(isset($_form['Entries'])) {
            $i = 0;
            if(!empty($entries)) while($entry = $entries->fetch_assoc()) {
                $_form['Entries'][$i] = array(
                    'EntryId' => $entry['entry_id'],
                    'HitId' => '',
                    'AssignmentId' => '',
                    'Environment' => '',
                    'Status' => '',
                    'PurchaseTotal' => '',
                    'Currency' => '',
                    'TransactionId' => '',
                    'MerchantType' => '',
                    'DateCreated' => $entry['entry_create'],
                    'CreatedBy' => 'datphan',
                    'DateUpdated' => '',
                    'UpdatedBy' => '',
                    'IP' => $entry['entry_ip'],
                    'LastPage' => '1',
                    'CompleteSubmission' => $entry['entry_complete']
                );
                
                $posts = $this->helpers->jd2a($entry['entry_content']);
                foreach($posts as $k => $post) {
                    if(substr($k,0,5) == 'Field' && !strpos($k,'Other') && !empty($post)) {
                        $_form['Entries'][$i][$k] = $post;
                    }
                }
                $i++;
            }
            $_form['Entries'] = array_values($_form['Entries']);
            foreach($catetory as $cat) {
                array_push($_form['Fields'],$cat);
            }
            
        }
        $html = $this->getFields(array(
                'fields' => $fields,
                'errorFields' => array()));
        $this->view->title = $_form['Name'] .' - Entries';
        $this->view->html = $html;
        $this->view->formId = $formId;
        $this->view->_form = $_form;
        $this->view->render();
    }
    function submit() {
        $this->view->render();
    }
    function entries_bulkDelete($req) {
        $formId = $this->routers['pos_1'];
        $formId = $this->helpers->str_escape($formId);
        if(!empty($formId)) {
            $form = $this->forms_model->getForm(array('where' => "`form_uuid` = '$formId'"));
            if(!empty($form)) {
                $form_id = $form['form_id'];
                list($entries, $total) = $this->entriesForms_models->getEFs(array('where' => "`form_id` = '$form_id'"));
                $entry_ids = array();
                if(!empty($entries)) while($entry = $entries->fetch_assoc()) {
                    $entry_ids[] = '\''.$entry['entry_id'].'\'';
                }
                $entry_ids = implode(',',$entry_ids);
                if(empty($entry_ids)) $entry_ids = "'0'";
                if($this->entries_model->deleteEntry(array(
                    'where' => "`entry_id` IN ($entry_ids) OR `entry_of` IN ($entry_ids)"))) {
                    $this->entriesForms_model->deleteEF(array(
                        'where' => "`form_id` = '$form_id'"));
                    $this->caches_model->deleteCachedEntries("`cache_entry_id` IN ($entry_ids)");
                }
                
            }
            if(empty($this->entries_model->getErrors())) {
                $this->json['success'] = true;
            } else {
                $this->json['response']['message'] = $this->entries_model->getErrors(); 
            }
        }
    }
    function entries_delete($req) {
        $entry = $this->helpers->str_escape(isset($req['entry'])?$req['entry']:'');
        $entry = $this->helpers->jd2a($entry);
        $entryId = $entry['EntryId'];
        if(!empty($entryId)) {
            if($this->entries_model->deleteEntry(array(
                'where' => "`entry_id` = '$entryId' OR `entry_of` = '$entryId'"))) {
                $this->entriesForms_model->deleteEF(array(
                        'where' => "`entry_id` = '$entryId'"));
                $this->caches_model->deleteCachedEntries("`cache_entry_id` = '$entryId'");
            }
            if(empty($this->entries_model->getErrors())) {
                $this->json['success'] = true;
            } else {
                $this->json['response']['message'] = $this->entries_model->getErrors(); 
            }
        }
    }
    function entries_fetch($req) {
        // get form
        $formId = $this->helpers->str_escape($this->routers['pos_1']);
        if(empty($formId)) {
            $this->json['response']['message'] = 'Form Id notfound';
            return false;
        } 
        $form = $this->forms_model->getForm(array('where' => "`form_uuid` = '$formId'"));
        if(empty($form)) {
            $this->json['response']['message'] = 'Form notfound';
            return false;
        } 
        $order = $this->helpers->str_escape(isset($req['order'])?$req['order']:'');
        if($order == 'EntryId') $order = 'entry_id';
        if($order == 'DateCreated') $order = 'entry_create';

        $form_id = $form['form_id'];
        $_form = $this->helpers->jd2a($form['form_content']);
        // get entries
        $page_limit = $this->entry_configs['page_limit'];
        $paged = $this->routers['paged'];
        $limit = (($paged - 1)*$page_limit).','.$page_limit;
        list($entries, $total) = $this->entries_model->getFullEntries(array(
            'form_id' => $form_id,
            'limit' => $limit));

        $_form['EntryCount'] = $total;
        if(isset($_form['Entries'])) {
            $i = 0;
            if(!empty($entries)) while($entry = $entries->fetch_assoc()) {
                $_form['Entries'][$i] = array(
                    'EntryId' => $entry['entry_id'],
                    'HitId' => '',
                    'AssignmentId' => '',
                    'Environment' => '',
                    'Status' => '',
                    'PurchaseTotal' => '',
                    'Currency' => '',
                    'TransactionId' => '',
                    'MerchantType' => '',
                    'DateCreated' => $entry['entry_create'],
                    'CreatedBy' => 'datphan',
                    'DateUpdated' => '',
                    'UpdatedBy' => '',
                    'IP' => $entry['entry_ip'],
                    'LastPage' => '1',
                    'CompleteSubmission' => $entry['entry_complete']
                );
                
                $posts = $this->helpers->jd2a($entry['entry_content']);
                foreach($posts as $k => $post) {
                    if(substr($k,0,5) == 'Field' && !strpos($k,'Other') && !empty($post)) {
                        $_form['Entries'][$i][$k] = $post;
                    }
                }
                $i++;
            }
            $_form['Entries'] = array_values($_form['Entries']);
            $this->json['success'] = true;
        }
        $this->json['response']['response'] = json_encode($_form);
    }
    function entries_display() {  
    }
    function entries_loadEntry($req) {
        $formId = $this->helpers->str_escape($this->routers['pos_1']);
        $form = $this->forms_model->getForm(array('where' => "`form_uuid` = '$formId'"));
        if(empty($form)) {
            $this->json['response']['message'] = 'Form notfound';
            return false;
        }
        $_form = $this->helpers->jd2a($form['form_content']);
        $fields = $_form['Fields'];
        $html = $this->getFields(array(
                'fields' => $fields,
                'errorFields' => array()));
        ob_start();
        ?>
        <div class="ltr">
            <form id="entry_form" class="hoctudau topLabel" onsubmit="submitForm(this);" autocomplete="off" enctype="multipart/form-data" method="post" action="<?php echo $this->url; ?>entries/submit/<?php echo $formId; ?>" target="submit_form_here"
            novalidate>
                <div class="info">
                    <div class="var">
                        #
                        <b>
                            <?php echo $form['form_id']; ?>
                        </b>
                    </div>
                    <h2 class="notranslate">
                        <?php echo $_form['Name']; ?>
                    </h2>
                    <div class="notranslate">
                        <?php echo $_form['Description']; ?>
                    </div>
                </div>
                <ul>
                    <?php echo $html; ?>
                    <li class="buttons">
                        <input id="saveForm" name="saveForm" class="btTxt submit" type="submit" tabindex="35" value="Save Changes" />
                    </li>
                </ul>
            </form>
        </div>
        <?php
        $this->json['response']['markup'] = ob_get_clean();
    }
    function entries_readOnly($req) {
        $html = '';
        $entryId = $this->helpers->str_escape(isset($req['entryId'])?$req['entryId']:'');
        $formId = $this->helpers->str_escape(isset($req['formId'])?$req['formId']:'');
        $form = $this->forms_model->getForm(array('where' => "`form_uuid` = '$formId'"));
        if(empty($form)) {
            $this->json['response']['message'] = 'Form notfound';
            return false;
        }
        $_form = $this->helpers->jd2a($form['form_content']);
        $form_id = $form['form_id'];
        $fields = $_form['Fields'];
        $formType = $_form['Formtype'];
        $entry = $this->entries_model->getEntry(array('where' => "`entry_id` = '$entryId'"));
        if(empty($entry)) {
            $this->json['response']['message'] = 'Entry notfound';
            return false;
        }
        $user_id = $entry['entry_user'];
        $user = $this->getCached('users',$user_id);
        if(empty($user)) {
            $this->json['response']['message'] = 'User notfound';
            return false;
        }
        $html = $this->common_getFormResult(array(
            'hideHeadInfo' => false, 
            'hideHeader' => true,
            'review' => false,
            'form' => $form,
            'entry' => $entry,
            'user' => $user));
        $datetime = explode(' ',$entry['entry_create']);
        $date = $datetime[0];
        $time = $datetime[1];
        ob_start();
        ?>
        <table cellspacing="0" class="form-header">
            <tr>
                <td class="h2">
                    <?php echo $_form['Name']; ?>
                </td>
                <td class="var-wrap">
                    <div class="var">
                        #
                        <b>
                            <?php echo $entry['entry_id']; ?>
                        </b>
                        <?php if(!$entry['entry_complete']): echo '<p><b class="red">UnComplete</b></p>'; else: echo '<p><b class="green">Complete</b></p>'; endif; ?>
                    </div>
                </td>
            </tr>
            <?php
                $usered = false;
                if($entry['entry_type'] == 'BT') {
                    $usered_id = $entry['entry_user'];
                    if($usered_id !=0) $usered = true;
                }
                if($entry['entry_type'] != 'BT' || $usered) {
                    $user_id = $entry['entry_user'];
                    if($entry['entry_type'] == 'NB' || $usered) :
            ?>
            <tr>
                <td>Bài Nộp Của: <?php echo $user['fullname']; ?><br />
                    <?php
                    if($this->entry_configs['show_h2d']) {
                    ?>
                    Nick H2d: <a target="_blank" href="http://hoctudau.com/h2d/index.php?qa=user&qa_1=<?php echo $user['h2d']; ?>"><?php echo $user['h2d']; ?></a>
                    <?php
                    }
                    ?><br />
                    <?php
                    if($this->entry_configs['show_skype']) {
                    ?>
                    Nick Skype:  <a href="skype:<?php echo $user['skype']; ?>?chat"><?php echo $user['skype']; ?></a>
                    <?php
                    }
                    ?>
                </td>
                <td class="var-wrap"><a target="_blank" href="<?php echo $this->url; ?>forms/<?php echo $_form['FormScoreUrl']; ?>/<?php echo $entry['entry_uuid']; ?>">Chấm Điểm</a></td>
            </tr>
            <?php 
                elseif($entry['entry_type'] == 'SC') :
                $entry_of = $entry['entry_of'];
                $entry_nb = $this->entries_model->getEntry(array(
                    'where' => "`entry_uuid` = '$entry_of' AND `entry_type` = 'NB'"));
                if(!empty($entry_nb)) {
                    $user_nb_id = $entry_nb['entry_user'];
                    $user_nb = $this->getCached('users',$user_nb_id);
                }
            ?>
            <tr>
                <td>
                    <div class="fl width-45">
                        Bài Chấm Của: <?php echo $user_nb['fullname']; ?><br />
                        <?php
                        if($this->entry_configs['show_h2d']) {
                        ?>
                        Nick H2d: <a target="_blank" href="http://hoctudau.com/h2d/index.php?qa=user&qa_1=<?php echo $user_nb['h2d']; ?>"><?php echo $user_nb['h2d']; ?></a>
                        <?php
                        }
                        ?>
                        <br />
                        <?php
                        if($this->entry_configs['show_skype']) {
                        ?>
                        Nick Skype: <a href="skype:<?php echo $user_nb['skype']; ?>?chat">
                        <?php echo $user_nb['skype']; ?></a>
                        <?php
                        }
                        ?>
                    </div>
                    <div style="fl width-10">
                        <strong>Cho</strong>
                    </div>
                    <div style="fr width-45">
                        <?php if(!empty($entry_nb)) { ?>
                        
                        Bài Nộp Của: <?php echo $user_nb['Name']; ?><br />
                        <?php
                        if($this->entry_configs['show_h2d']) {
                        ?>
                        Nick H2d: <a target="_blank" href="http://hoctudau.com/h2d/index.php?qa=user&qa_1=<?php echo $user_nb['h2d']; ?>"><?php echo $user_nb['h2d']; ?></a>
                        <?php
                        }
                        ?>
                        <br />
                        <?php
                        if($this->entry_configs['show_skype']) {
                        ?>
                        Nick Skype: <a href="skype:<?php echo $user_nb['skype']; ?>?chat">
                        <?php echo $user_nb['skype']; ?></a>
                        
                        <?php } } ?>
                    </div>
                    <div class="clear"></div>
                </td>
                <td><a target="_blank" href="<?php echo $this->url; ?>summary/<?php echo $entry_of; ?>">Xem Tổng Điểm</a></td>
            </tr>
            <?php
                elseif($entry['entry_type'] == 'AS') :
            ?>
            <tr>
                <td>Bài Nộp Của: <?php echo $user['fullname']; ?><br />
                <?php
                if($this->entry_configs['show_h2d']) {
                ?>
                Nick H2d: 
                <a target="_blank" href="http://hoctudau.com/h2d/index.php?qa=user&qa_1=<?php echo $user['h2d']; ?>"><?php echo $user['h2d']; ?></a>
                <?php
                }
                ?><br />
                <?php
                if($this->entry_configs['show_skype']) {
                ?>
                Nick Skype: <a href="skype:<?php echo $user['skype']; ?>?chat">
                <?php echo $user['skype']; ?></a>
                <?php
                }
                ?>
                </td>
                <td class="var-wrap"><a target="_blank" href="<?php echo $this->url; ?>summary/<?php echo $entry['entry_uuid']; ?>">Xem Điểm</a></td>
            </tr>
            <?php
            endif;    
            } 
            
            ?>
        </table>
        <?php
            echo $html;
        ?>
        <table class="form-footer" id="entryInfo" cellspacing="0">
            <tr>
                <td class="createTD">
                    <div class="created">
                        <span class="created-head">
                            Created
                        </span>
                        <div id="_dc" class="dc notranslate">
                            <?php echo $date; ?>
                        </div>
                        <i class="created-i notranslate">
                            <?php echo $time; ?>
                        </i>
                        <cite id="_cb" class="cb notranslate">
                            ...
                        </cite>
                    </div>
                </td>
                <td class="ip">
                    <div id="_ip" class="ip-inner">
                        <a href="http://ip-lookup.net/index.php?ip=<?php echo $entry['entry_ip']; ?>" target="_blank" class="notranslate"><?php echo $entry['entry_ip']; ?></a>
                    </div>
                    <i class="ip-i">
                        IP Address
                    </i>
                </td>
            </tr>
        </table>
        <?php
        $this->json['response']['response'] = ob_get_clean();
        $this->json['success'] = true;
    }
    function entries_submit($req) {
        if(!isset($req['saveForm'])) return;
        $formId = $this->helpers->str_escape($this->routers['pos_2']);
        $s2 = false;
        $form = $this->forms_model->getForm(array(
            'where' => "`form_uuid` = '$formId'"));
        if(!empty($form)) {
            $ip = IP;
            $posts = $this->helpers->jeae($req);
            $_form = $this->helpers->jd2a($form['form_content']);
            $_fields = $_form['Fields'];
            $formType = $_form['Formtype'];
            $normal_login = false;
            if($formType == 'normal') {
                if($_form['UserLogin']) {
                    $normal_login = true;
                }
            }
            $up1 = $alt3 = $alt4 = '';
            $user_id = '1';
            $entry_uuid = $this->helpers->get_uuid();
            if($formType == 'nopbai') {
                $alt3 = ",`entry_uuid`,`entry_type`,`entry_user`";
                $alt4 = ",'$entry_uuid','NB','$user_id'";
            } elseif($formType == 'score') {
                $up1 = ",`entry_scoring` = '$scores'";
            } elseif($formType == 'autoscore') {
                $scores2 = $this->scoring($posts, $_fields);
                $scores = $this->helpers->jeae($scores2);
                $alt3 = ",`entry_uuid`,`entry_type`,`entry_user`,`entry_of`,`entry_scoring`";
                $alt4 = ",'$entry_uuid','AS','$user_id','$entry_uuid','$scores'";
                $up1 = ",`entry_scoring` = '$scores'";
            } elseif($normal_login) {
                $alt3 = ",`entry_user`";
                $alt4 = ",'$user_id'";
            }
            if($req['saveForm'] == 'Save Changes') {
                $entry_id = $this->helpers->str_escape(isset($req['entryId'])?$req['entryId']:'');
                $entd = $this->helpers->get_datetime();
                if($this->entries_model->updateEntry(array(
                    'updates' => "`entry_update` = '$entd',`entry_content` = '$posts' $up1", 
                    'where' => "`entry_id` = '$entry_id'"))) {
                    $s2 = true;
                }
            } elseif($req['saveForm'] == 'Submit') {
                if($this->entries_model->insertEntry(array(
                    'fields' => "`entry_ip`,`entry_content` $alt3",
                    'values' => "'$ip','$posts' $alt4"))) {
                    $entry_id = $this->entries_model->getInsertId();
                    $form_id = $form['form_id'];
                    $this->entriesForms_model->insertEF(array(
                        'fields' => '`form_id`,`entry_id`',
                        'values' => "'$form_id','$entry_id'"));
                    $s2 = true;
                }
            }
        }
        $this->view->s2 = $s2;
        $this->setAction('submit');
        $this->view->render('single');
        exit();
    }
}