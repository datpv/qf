<?php
if ( ! defined( 'IA' ) ) exit;
/*
    @Name: Application Common Wrong Answers Controller
    @Description: 
    @Docs:
*/
class Applications_Controllers_Cwa extends Applications_Controller {
    public 
        $cwa_configs = array(
                'page_limit' => 30
            )
        ;
    function __construct($registry) {
        parent::__construct($registry);
    }
    function initiallize() {
        $this->permission = 'Admin';
        parent::initiallize();
        // models
        $this->forms_model = new Applications_Models_Forms($this->registry);
        $this->comments_model = new Applications_Models_Comments($this->registry);
        $this->logs_model = new Applications_Models_FormsLogs($this->registry);
        $this->logsComments_model = new Applications_Models_FormsLogsComments($this->registry);
        $this->logsUsers_model = new Applications_Models_FormsLogsUsers($this->registry);
    }
    function setConfigs() {
        parent::setConfigs();
        if(isset($this->configs['display']['cwa'])) {
            $this->cwa_configs = $this->configs['display']['cwa'];
        }
    }
    function index() {
        $routers = $this->getRouters();
        $formId = $routers['pos_1'];
        $formId = $this->helpers->str_escape($formId);
        if(empty($formId)) {
            $this->notfound();
            return;
        }
        $form = $this->forms_model->getForm(array(
            'where' => "`form_uuid` = '$formId'"));
        if(empty($form)) {
            $this->notfound();
            exit();
        }

        /*
        * Fields
        */
        $form_id = $form['form_id'];
        $_form = $this->helpers->jd2a($form['form_content']);
        $fields = $_form['Fields'];

        /*
        * GET FIELDS IDS
        */
        $logs2 = $logs3 = $logs4 = $checkBoxFields = $field_ids = $field_cache = array();
        foreach($fields as $field) {
            $field_id = $field['ColumnId'];
            $type = $field['Typeof'];
            $field_cache[$field_id] = $field;
            if($type == 'text' || $type == 'select' || $type == 'radio' || $type == 'checkbox') {
                $field_ids[] = "'".$field_id."'";

            }
        }

        /*
        * GET FIELDS WITH LOG HITs AND COMMENTS
        */
        $field_str = implode(',', $field_ids);
        if(empty($field_str)) $field_str = "'0'";
        list($logs, $total) = $this->logs_model->getFLs(array(
            'select' => 'SQL_CALC_FOUND_ROWS *',
            'where' => "`form_id` = '$form_id' AND `field_id` IN ($field_str)"));

        if(!empty($logs)) {
            $_logs = array();
            while($log = $logs->fetch_assoc()) {
                $log_id = $log['log_id'];
                $field_id = $log['field_id'];
                list($comments, $total_comments) = $this->comments_model->getCommentsOfLog(array(
                        'log_id' => $log_id,
                        'commented_id' => "'0'",
                        'limit' => '3'
                    ));
                $_comments = array();
                if(!empty($comments)) while( $comment = $comments->fetch_assoc() ) {
                    $comment['user'] = $this->getCached('users', $comment['comment_user']);
                    $_comments[] = $comment;
                }
                $log['type'] = $field_cache[$field_id]['Typeof'];
                $log['title'] = $field_cache[$field_id]['Title'];
                $log['comments'] = $_comments;
                $log['total_comments'] = $total_comments;
                $_logs[] = $log;
            }
            $logs2[] = $_logs;
        }

        /*
        * GROUP LOGS WITH SAME FIELD ID
        */
        foreach($logs2 as $logs) {
            if(!empty($logs)) foreach($logs as $k => $log) {
                $field_id = $log['field_id'];
                if(empty($logs4[$field_id])) {
                    //$log_content = $this->helpers->jd2a($log['log_content']);
                    $logs4[$field_id] = $log['title'];
                }
                $logs3[$field_id][] = $log;
            }
        }
        
        $this->view->title = 'Common Wrong Answers';
        $this->view->form_uuid = $formId;
        $this->view->form_id = $form_id;
        $this->view->form_name = $_form['Name'];
        $this->view->html = $this->listCwa(array(
                'logs3' => $logs3,
                'logs4' => $logs4,
                'fields' => $fields,
            ));
        $this->view->render();
    }
    function listCwa($args) {
        $logs3 = $args['logs3'];
        $logs4 = $args['logs4'];
        $fields = $args['fields'];
        $i = 0;
        $displayedFieldId = array();
        ob_start();
        foreach($logs3 as $kl => $logs) {
            $displayedFieldId[$kl] = $kl;
            $is_first_log = false;
            if($i == 0) {
                $is_first_log = true;
            }
            $i++;
            $field_title = $logs4[$kl];
            $args = array(
                    'field_id' => $kl,
                    'field_title' => $field_title,
                    'logs' => $logs,
                    'show_child' => true,
                    'is_first_log' => $is_first_log
                );

            $this->common_print_listCwa($args);
        }
        foreach($fields as $field) {
            if($field['Typeof'] == 'text' || $field['Typeof'] == 'select' || $field['Typeof'] == 'radio' || $field['Typeof'] == 'checkbox') {
                if(!in_array($field['ColumnId'], $displayedFieldId)) {
                    $args = array(
                        'field_id' => $field['ColumnId'],
                        'field_title' => $field['Title'],
                        'logs' => array(),
                        'show_child' => false,
                        'is_first_log' => false
                    );
                    $this->common_print_listCwa($args);
                }
            }
        }
        return ob_get_clean();
    }

    /* ---------------------------------------------------------------------------- */
    /* ------------------------ AJAX ----------------------------- */
    /* ---------------------------------------------------------------------------- */
    function cwa_addWa($req) {
        $formId = $this->helpers->str_escape($this->routers['pos_1']);
        $form = $this->forms_model->getForm(array('where' => "`form_uuid` = '$formId'"));
        if(empty($form)) {
            $this->notfound();
            exit();
        }
        $form_id = $form['form_id'];
        $fieldId = $this->helpers->str_escape(isset($req['fieldId'])?$req['fieldId']:'');
        $fieldTitle = (isset($req['fieldTitle'])?$req['fieldTitle']:'');
        $fieldValue = (isset($req['fieldValue'])?$req['fieldValue']:'');
        $fieldValue2 = $this->helpers->str_escape(trim($fieldValue));
        $log_content = array(
            'FieldId' => $fieldId,
            'Title' => $fieldTitle,
            'Value' => $fieldValue,
            'Answer' => '',
            'AType' => 'awrong'
        );
        $this->json['update'] = false;
        $log_content = $this->helpers->jeae($log_content);
        $log = $this->logs_model->getFL(array(
            'where' => "`form_id` = '$form_id' AND `field_id` = '$fieldId' AND `log_subject` = '$fieldValue2'"));
        if(!empty($log)) {
            $entd = $this->helpers->get_datetime();
            $log_id = $log['log_id'];
            $logs_obj->updateLog(array(
                'updates' => "`log_hit` = `log_hit` + 1, `log_update` = '$entd', `log_content` = '$log_content', `log_subject` = '$fieldValue2'", 
                'where' => "`log_id` = '$log_id'"));
            $this->json['success'] = true;
            $this->json['update'] = $log_id;
            $this->json['update_hits'] = $log['log_hit'] + 1;
        } else {
            if($this->logs_model->insertFL(array(
                'fields' => '`form_id`, `field_id`, `log_subject`, `log_content`', 
                'values' => "'$form_id', '$fieldId', '$fieldValue2', '$log_content'"))) {
                $log_id = $this->logs_model->getInsertId();
                $user_id = $this->user['user_id'];
                if($this->logsUsers_model->insertFLU(array(
                    'fields' => '`user_id`, `log_id`',
                    'where' => "'$user_id', '$log_id'"))) {
                    $this->json['success'] = true;
                    $this->json['response']['json'] = array(
                        $log_id => array(
                            'log_id' => $log_id,
                            'log_value' => $fieldValue
                        )
                    );
                    $this->json['response']['html'] = '<li id="log_'.$log_id.'"><header class="heading"><span class="value">'.$fieldValue.'</span> ( 1 Hits ) <a class="add_comment" href="#">Add comments</a> <a class="del_wa delete" href="#">Delete Answer</a></header><div class="comments"><ul></ul></div></li>';
                }
            }
        }
    }
    function cwa_deleteLog($req) {
        $log_id = $this->helpers->str_escape(isset($req['log_id'])?$req['log_id']:'');
        $args = array(
            'where' => "`log_id` = '$log_id'",
            'limit' => '');
        if($this->logs_model->deleteFL($args)) {
            $this->logsUsers_model->deleteFLU($args);
            $this->logsComments_model->deleteFLC($args);
            $this->json['success'] = true;
        }
    } 
    function cwa_loadComments($req) {
        $this->ajax_loadComments($req);
    }
    function cwa_addComment($req) {
        $this->ajax_addComment($req);
    }
    function cwa_deleteComment($req) {
        $this->ajax_deleteComment($req);
    }
}