<?php
if ( ! defined( 'IA' ) ) exit;
/*
    @Name: Application Authentication Controller
    @Description: manage login, logout
    @Docs:
*/
class Applications_Controllers_Auth extends Applications_Controller {
    public 
        $auth_configs = array(
            )
        ;
    function __construct($registry) {
        parent::__construct($registry);
    }
    function initiallize() {
        $this->permission = 'All';
        parent::initiallize();
        $this->userOnce();
        // models
        
    }
    function setConfigs() {
        parent::setConfigs();
        if(isset($this->configs['display']['auth'])) {
            $this->account_configs = $this->configs['display']['auth'];
        }
    }
    function userOnce() {
        if(!empty($this->user)) {
            header('location: '.$this->url.'');
            exit();
        }
    }
    function index() {
        $this->permissionDenied();
    }
    function login() {
        $this->view->title = 'Login';
        $this->view->render();
    }
    function logout() {
        if(isset($_SESSION['user'])) {
            unset($_SESSION['user']);
        }
        header('location: '.$this->url.'/');
        exit();
    }
    function signup() {
        $this->view->title = 'Register Account';
        $this->view->render();
    }
    function members_login($req) {
        if(!isset($req['username']) || !isset($req['password'])) {
            $this->errors['message'] = 'Some error occured!';
            return;
        }
        if(
            empty($req['username']) ||
            empty($req['password'])
            ) {
            $this->errors['message'] = 'Username Or Password MissMatched';
            return;
        }
        $username = isset($req['username'])?$req['username']:'';
        $password = isset($req['password'])?$req['password']:'';
        $remember = isset($req['remember'])?$req['remember']:'';
        
        $username = htmlspecialchars(stripslashes($username));
        $password = md5(htmlspecialchars(stripslashes($password)));
        $remember = htmlspecialchars(stripslashes($remember));
        $user = $this->users_model->getUser(array(
                'where' => "`username` = '$username' AND `password` = '$password'"
            ));
        if(empty($user)) {
            // other pass
        }
        if(empty($user)) {
            $this->errors['message'] = 'Username Or Password MissMatched';
        } else {
            $ip = IP;
            $user_id = $user['user_id'];
            $salt = md5($this->helpers->get_uuid());
            $timeout = time() + 3*60*60;
            $_SESSION['user'] = $user_id;
            if($remember == '1') {
                // md5('user-cookie') => c39cdfb95071c0392b0b55ec0e9208fc
                $this->helpers->setCookie('c39cdfb95071c0392b0b55ec0e9208fc', $salt, $timeout);
                $success = $this->members_model->updateObject(array(
                        'updates' => "`ip` = '$ip', `cookie` = '$salt', `timeout` = '$timeout'",
                        'where' => "`user_id` = '$user_id'"
                    ));
            } else {
                $this->helpers->setCookie('c39cdfb95071c0392b0b55ec0e9208fc', $salt, time()-1000);
            }
            $this->json['success'] = true;
        }
        $this->json['response']['message'] = !empty($this->errors['message'])?$this->errors['message']:'';
    }
    function doLogin($req) {
        $this->members_login($req);
        $ref = isset($req['ref'])?$req['ref']:'';
        if(!$this->helpers->validate_url($ref)) {
            $ref = $this->url;
        }
        if($this->json['success']) {
            header('location: '.$ref);
            exit();
        }
        $this->view->errors = $this->errors;
    }
    function doSignup($req) {
        $errors = array();
        $this->view->req = $req;
        $next = true;
        $errors = $this->helpers->validate_signup($req);
        if(!$this->helpers->isValidCaptcha($_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"])) {
            $errors['captcha']['message'] = 'Invalid Captcha';
        }
        if(empty($errors)) {
            $username = isset($req['username'])?$req['username']:'';
            $email= isset($req['email'])?$req['email']:'';
            $password = isset($req['password'])?$req['password']:'';
            $nickh2d= isset($req['nickh2d'])?$req['nickh2d']:'';
            $nickskype = isset($req['nickskype'])?$req['nickskype']:'';
            $fullname = isset($req['fullname'])?$req['fullname']:'';
            
            $email = htmlspecialchars($this->helpers->str_escape($email));
            $username = htmlspecialchars($this->helpers->str_escape($username));
            $nickskype = htmlspecialchars($this->helpers->str_escape($nickskype));
            $password = md5(htmlspecialchars($this->helpers->str_escape($password)));
            $fullname = $this->helpers->str_escape($fullname);

            if($this->users_model->getUser(array(
                'where' => "`username` = '$username' OR `email` = '$email' OR `h2d` = '$nickh2d'"))) {
                $errors['message'] = 'Username, Email Or Nick H2d Existed';
            } elseif($this->users_model->insertUser(array(
                'fields' => "`username`,`password`,`email`,`fullname`,`h2d`,`skype`", 
                'values' => "'$username','$password','$email','$fullname','$nickh2d','$nickskype'"))) {
                $this->view->redirect = true;
                $this->view->message('Please Wait For Admin Complete Your Registration.');
                exit();
            }
        }
        $this->view->errors = $errors;
    }
    function postReq() {
        $req = $this->helpers->str_unescape($_POST);
        $routers = $this->routers;
        if($routers['action'] == 'login') {
            $this->doLogin($req);
            $this->setAllowNext(true);
        } elseif($routers['action'] == 'signup') {
            $this->doSignup($req);
            $this->setAllowNext(true);
        } else {
            if(!isset($req['action'])) return;
            if($req['action'] == 'login') {
                $this->members_login($req);
                echo json_encode($this->json);
            }
        }
    }
}