<?php
if ( ! defined( 'IA' ) ) exit;
/*
    @Name: Application Forms Manager Controller
    @Description: manage forms
    @Docs:
*/
class Applications_Controllers_Forms extends Applications_Controller {
    public
        $fm_configs = array(
                'page_limit' => 4,
            )
        ;
    function __construct($registry) {
        parent::__construct($registry);
    }
    function initiallize() {
        $this->permission = 'Admin';
        parent::initiallize();
        // models
        $this->forms_model = new Applications_Models_Forms($this->registry);
        $this->entries_model = new Applications_Models_Entries($this->registry);
        $this->entriesForms_model = new Applications_Models_EntriesForms($this->registry);
        $this->categories_model = new Applications_Models_Categories($this->registry);
        $this->categoriesForms_model = new Applications_Models_CategoriesForms($this->registry);
        $this->themes_model = new Applications_Models_Themes($this->registry);
    }
    function setConfigs() {
        parent::setConfigs();
        if(isset($this->configs['display']['formsmanager'])) {
            $this->fm_configs = $this->configs['display']['formsmanager'];
        }
    }
    function index() {
        /*
        * Get Forms, Themes, New Entries Count
        */
        $cate_id = isset($_COOKIE['cate_id'])?$_COOKIE['cate_id']:'';
        $cate_id = $this->helpers->str_escape($cate_id);
        $page_limit = $this->fm_configs['page_limit'];
        $paged = $this->routers['paged'];
        $limit = (($paged - 1)*$page_limit).','.$page_limit;
        if(empty($cate_id)) {
            list($forms, $total) = $this->forms_model->getForms(array(
                'select' => 'SQL_CALC_FOUND_ROWS *',
                'where' => "`form_approved` = 'Y'",
                'orderby' => 'form_create',
                'dir' => 'DESC',
                'limit' => $limit));
        } else {
            list($forms, $total) = $this->forms_model->getFormInCategory(array(
                'cate_id' => $cate_id,
                'approve_type' => 'Y',
                'orderby' => 'form_create',
                'dir' => 'DESC',
                'limit' => $limit));
        }
        $this->view->forms = $this->listForm(array(
                'forms' => $forms,
            ));
        $this->view->total = $total;
        $this->view->count = $forms->num_rows;
        $this->view->paginate = $this->view->getPaginate(array(
                'total' => $total,
                'paged' => $paged,
                'page_limit' => $page_limit,
                'base_url' => $this->url . 'formsmng/'
            ));
        list($categories, $total)  = $this->categories_model->getCategories(array('select' => 'SQL_CALC_FOUND_ROWS *'));
        $this->view->categories = $this->listCategory(array('categories' => $categories,'cate_id' => $cate_id));
        /*
        *
        */
        $this->view->title = 'Form Manager';
        $this->view->render();
    }
    function listCategory($args) {
        $categories = $args['categories'];
        if(empty($categories)) return '';
        $setJson = isset($args['setJson'])?$args['setJson']:false;
        $i=0;
        $json_ = array();
        $cate_id = $args['cate_id'];
        ob_start();
        ?><li id="cat_0" <?php if($cate_id == '0') echo 'class="active"'; ?>><a href="#">All</a></li><?php
        while($category = $categories->fetch_assoc()) {
            ?><li id="cat_<?php echo $category['category_id']; ?>" <?php if($cate_id == $category['category_id']) echo 'class="active"'; ?>><a href="#"><?php echo $category['category_name']; ?></a><span class="setting">&nbsp;</span><div class="hide setting_content"><span onclick="category_rename(<?php echo $category['category_id']; ?>); return false;">Rename</span><span onclick="category_delete(<?php echo $category['category_id']; ?>); return false;">Delete</span>
            </div></li><?php
        }
        if($setJson) {
            $this->json['response']['json'] = $json_;
        }
        return ob_get_clean();
    }
    function listForm($args) {
        $forms = $args['forms'];
        if(empty($forms)) return '';
        $setJson = isset($args['setJson'])?$args['setJson']:false;
        $i=0;
        $json_ = array();
        list($themes, $total) = $this->themes_model->getThemes(array('select' => 'SQL_CALC_FOUND_ROWS *'));
        ob_start();
        while($form = $forms->fetch_assoc()) {
            if(empty($form['form_content'])) continue;
            $form_content = $this->helpers->jd2a($form['form_content']);
            if(empty($form_content)) continue;
            $form_id = $form['form_id'];
            $form_uuid = $form['form_uuid'];
            $theme_id = $form['theme_id'];
            $form_type = $form['form_type'];
            $form_status = $form['form_status'];
            $entry_total = $this->entriesForms_model->getRows(array('where' => "`form_id` = '$form_id'"));
            $entry_count = $entry_total - $form['form_flag'];
            $create_user = $this->getCached('users',$form['form_user']);
            if($setJson) {
                $json_[$form_id] = array(
                    'FormId' => $form_id,
                    'Url' => $form_uuid,
                    'Name' => $form_content['Name']
                );
            }
            ?><li id="li<?php echo $form_id; ?>" class="<?php echo $form_type.' '; echo ($i==0)?'first':'';$i++; echo ($form['form_status'] == 'D')?' notActive':''; ?> approvedForm" onmouseover="showActions(this)" onmouseout="hideActions(this)"><h4><span class="by_user"><a title="<?php echo $create_user['h2d']; ?>" href="<?php echo $this->url; ?>dashboard/&user_id=<?php echo $create_user['user_id']; ?>" target="_blank"><?php echo $this->helpers->str_truncate($create_user['h2d'],6,' ',3); ?></a></span><a title="<?php echo $form_content['Name']; ?>" id="link<?php echo $form_id; ?>" href="<?php echo $this->url; ?>entries/<?php echo $form_uuid; ?>"><span class="notranslate"> <?php echo $this->helpers->str_truncate($form_content['Name'],46); ?> </span><?php if($entry_count > 0): ?><b title="Hooray!"><span class="notranslate"><?php echo $entry_count; ?></span> New Entries.</b><?php endif; ?></a></h4><span title="Add Categories" onclick="addFormCategories(<?php echo $form_id; ?>); return false;" class="category_select">&nbsp;</span><span class="themeSelect"><select onchange="changeTheme(<?php echo $form_id; ?>, 'Untitled%20Form', this)"><?php echo $this->view->getOptions($themes, $theme_id, 'theme_id'); ?> <option value="create"> ----------- </option><option value="create"> New Theme! </option></select></span><span class="activeCheck"><label for="publicStatus_<?php echo $form_id; ?>"> Public </label><input type="checkbox" id="publicStatus_<?php echo $form_id; ?>" onclick="togglePublicForm(<?php echo $form_id; ?>, 'HTD-FORM', this)" <?php echo ($form_status == 'A')?'checked="checked"':''; ?> /></span><div id="expandThis"><div class="actions"><a class="approve" href="#" onclick="approveForm(this); return false;">Approve</a><a class="unapprove" href="#" onclick="unApproveForm(this); return false;">UnApprove</a><a class="del" href="#" onclick="deleteForm(this); return false;">Delete</a><a class="dup" href="#" onclick="duplicateForm(this); return false;">Duplicate</a><a class="entries" href="#" onclick="viewEntries(this); return false;">Entries</a><a class="edit" href="#" onclick="editForm(this); return false;">Edit</a><a class="view" href="<?php echo $this->url; ?>forms/<?php echo $form_uuid; ?>" target="_blank">View</a><a class="view" href="<?php echo $this->url; ?>cwa/<?php echo $form_uuid; ?>" target="_blank">CWA</a><a class="subscribe" href="#" onclick="viewNotifications(this); return false;">Notifications</a><a class="rules" href="#" onclick="viewRules(this); return false;">Rules</a><span><a class="protect" href="#" onclick="showProtectForm(this);return false;">Protect</a></span><span><a target="_blank" class="protect" href="<?php echo $siteUrl; ?>report/auto_builder/<?php echo $form_uuid; ?>">Create Report</a></span><span id="paidPlanWarningDiv" class="hide"> This feature is only available to our paid plans. </span></div></div></li><?php
        }
        if($setJson) {
            $this->json['response']['json'] = $json_;
        }
        return ob_get_clean();
    }

    /* ---------------------------------------------------------------------------- */
    /* ------------------------ AJAX ----------------------------- */
    /* ---------------------------------------------------------------------------- */

    function forms_categoryRename($req) {
        $category_id = isset($req['category_id'])?$req['category_id']:'';
        $category_name = isset($req['category_name'])?$req['category_name']:'';
        $category_id = $this->helpers->str_escape($category_id);
        $category_name = $this->helpers->str_escape($category_name);
        if(strlen($category_name) > 1 && !empty($category_id)) {
            if(!$this->categories_model->updateCategory(array(
                'updates' => "`category_name` = '$category_name'", 
                'where' => "`category_id` = '$category_id'"))) {
                $this->json['success'] = true;
            }
        }
    }
    function forms_categoryDelete($req) {
        $category_id = isset($req['category_id'])?$req['category_id']:'';
        $category_id = $this->helpers->str_escape($category_id);
        if(!empty($category_id)) {
            $this->categories_model->deleteCategory(array('where' => "`category_id` = '$category_id'"));
            $this->categoriesForms_model->deleteCF(array('where' => "`category_id` = '$category_id'"));
            $error = $this->categories_model->getErrors();
            if(empty($error)) {
                $this->json['success'] = true;
            }
        }
    }
    function forms_categoryAdd($req) {
        $category_name = isset($req['category_name'])?$req['category_name']:'';
        $category_name = $this->helpers->str_escape($category_name);
        if(strlen($category_name) > 1) {
            if($this->categories_model->insertCategory(array(
                'fields' => "`category_name`",
                'values' => "'$category_name'"))) {
                $this->json['success'] = true;
            }
        }
    }
    function forms_addCategory($req) {
        $formId = $this->helpers->str_escape(isset($req['formId'])?$req['formId']:'');
        $s_categories = $this->helpers->str_escape(isset($req['s_categories'])?$req['s_categories']:'');
        list($categories, $total) = $this->categoriesForms_model->geCFs(array(
            'select' => 'SQL_CALC_FOUND_ROWS *',
            'where' => "`form_id` = '$formId'"));
        // get new categoriy ids
        $new_ids = explode(',',$s_categories);
        $new_ids = array_map('trim',$new_ids);
        // get old category ids
        $old_ids = array();
        
        while($category = $categories->fetch_assoc()) {
            $old_ids[] = $category['category_id'];
        }
        // categoriy ids to add
        $add_ids = array_diff($new_ids,$old_ids);
        // tags to delete
        $del_ids = array_diff($old_ids,$new_ids);
        $delete_ids = array();
        while($category = $categories->fetch_assoc()) {
            foreach($del_ids as $del_id) {
                if($del_id == $category['category_id']) {
                    $delete_ids[] = '\''.$category['category_id'].'\'';
                }
            }
        }
        $delete_ids = implode(',',$delete_ids);
        if(empty($delete_ids)) $delete_ids = '\'0\'';
        
        $this->categoriesForms_model->deleteCF(array(
            'where' => "`category_id` IN ($delete_ids) AND `form_id` = '$formId'"));
        foreach($add_ids as $add_id) {
            $add_id = $this->helpers->str_escape($add_id);
            if($this->categories_model->getCategory(array(
                'where' => "`category_id` = '$add_id'"))) {
                $this->categoriesForms_model->insertCF(array(
                    'fields' => "`form_id`, `category_id`",
                    'values' => "'$formId','$add_id'"));
            }
        }
        $error = $this->categories_model->getErrors();
        if(empty($error)) {
            $this->json['success'] = true;   
        } else {
            $this->json['message'] = $this->categories_model->getErrors();
        }
    }
    function forms_getForms($req) {
        $approveType = $this->helpers->str_escape(isset($req['approveType'])?$req['approveType']:'Y');
        $approveType = strtoupper($approveType);
        if($approveType != 'Y' || $approveType != 'N')  $direction = 'Y';

        $direction = $this->helpers->str_escape(isset($req['direction'])?$req['direction']:'DESC');
        $direction = strtoupper($direction);
        if($direction != 'DESC' || $direction != 'ASC')  $direction = 'DESC';

        $sort_by = $this->helpers->str_escape(isset($req['sort_by'])?$req['sort_by']:'form_create');
        if($sort_by == 'FormId') $sort_by = 'form_create';
        elseif($sort_by == 'DateUpdated') $sort_by = 'form_update';
        else $sort_by = 'form_create';

        $cate_id = $this->helpers->str_escape(isset($req['category_id'])?$req['category_id']:'');
        $page_limit = $this->fm_configs['page_limit'];
        $routers = $this->routers;
        $paged = $routers['paged'];
        $limit = (($paged - 1)*$page_limit).','.$page_limit;
        if(empty($cate_id)) {
            list($forms, $total) = $this->forms_model->getForms(array(
                'select' => 'SQL_CALC_FOUND_ROWS *',
                'where' => "`form_approved` = '$approveType'",
                'orderby' => $sort_by,
                'dir' => $direction,
                'limit' => $limit));
        } else {
            list($forms, $total) = $this->forms_model->getFormInCategory(array(
                'cate_id' => $cate_id,
                'approve_type' => $approveType,
                'orderby' => $sort_by,
                'dir' => $direction,
                'limit' => $limit));
        }
        $html = $this->listForm(array(
                'forms' => $forms,
                'setJson' => true
            ));
        if(empty($html)) {
            ob_start();
            ?>
            <li class="notice bigMessage">
                <h2>
                    <a href="<?php echo $this->url; ?>build/"><span class="bigMessageRed">You don't have any forms!</span> <span class="bigMessageGreen">On This Category!</span></a>
                </h2>
            </li>
            <?php
            $html = ob_get_clean();
        }
        $paginate = $this->view->getPaginate(array(
                'total' => $total,
                'paged' => $paged,
                'page_limit' => $page_limit,
                'base_url' => $this->url . 'formsmng/'
            ));
        $this->json['success'] = true;
        $this->json['response']['html'] = $html;
        $this->json['response']['paged'] = $paginate;
    }
    function forms_approveUnapprove($req) {
        $cate_id = $this->helpers->str_escape(isset($req['form_id'])?$req['form_id']:'');
        $form = $this->forms_model->getForm(array('where' => "`form_id` = '$form_id'"));
        if(empty($form)) return false;
        $fapro = $form['form_approved'];
        if($fapro == 'Y') {
            $fapro = 'N';
            $fsta = 'D';
        }  else {
            $fapro = 'Y';
            $fsta = 'A';
        }
        if($this->forms_model->updateForm(array(
            'updates' => "`form_approved` = '$fapro', `form_status` = '$fsta'", 
            'where' => "`form_id` = '$form_id'"))) {
            $this->json['success'] = true;
        }
    }
    function forms_duplicate($req) {
        $formId = $this->helpers->str_escape(isset($req['formId'])?$req['formId']:'');
        if(empty($formId)) return false;
        $form = $this->forms_model->getForm(array('where' => "`form_id` = '$formId'"));
        if(empty($form)) return false;
        $_form = $this->helpers->jd2a($form['form_content']);
        $_form['Name'] = $_form['Name'] . ' Cloned';
        $form_content = $this->helpers->jeae($_form);
        $newFormId = $this->helpers->str_uuid();
        if(!$this->forms_model->insertForm(array(
            'fields' => '`form_uuid`,`form_content`', 
            'values' => "'$newFormId','$form_content'"))) {
            $this->json['response']['message'] = $this->forms_model->getErrors();
        } else {
            $this->json['success'] = true;
        }
    }
    function forms_delete($req) {
        $formId = $this->helpers->str_escape(isset($req['formId'])?$req['formId']:'');
        if(empty($formId)) return false;
        // delete form
        if($this->forms_model->deleteForm(array('where' => "`form_id` = '$formId'"))) {
            // delete form reports
            $this->reports_model->deleteReport(array('where' => "`form_id` = '$formId'"));
            // get related entry ids
            $entries_result = $this->entriesForms_model->getEF(array('where' => "`form_id` = '$formId'"));
            $entry_ids = array();
            if(!empty($entries_result)) while($entry = $entries_result->fetch_assoc()) {
                $entry_ids[] = '\''.$entry['entry_id'].'\'';
            }
            $entry_ids = implode(',',$entry_ids);
            if(empty($entry_ids)) $entry_ids = '\'0\'';
            // delete form entries
            if($this->entries_model->deleteEntry(array('where' => "`entry_id` IN ($entry_ids)"))) {
                $this->entriesForms_model->deleteEF(array('where' => "`form_id` = '$formId'"));
                // delete form entries caches
                $this->caches_model->deleteCachedEntries("`cache_entry_id` IN ($entry_ids)");
                $this->json['success'] = true;
            }
        }
        $this->json['response']['html'] = $this->entries_model->getErrors();
    }
    function forms_protect($req) {
        $formId = $this->helpers->str_escape(isset($req['formId'])?$req['formId']:'');
        $password = md5($this->helpers->str_escape($req['password']));
        if($this->forms_model->updateForm(array(
            'updates' => "`form_password` = '$password'",
            'where' => "`form_id` = '$formId'"))) {
            $this->json['success'] = true;
        }
    }
    function forms_savePublicStatus($req) {
        $formId = $this->helpers->str_escape(isset($req['formId'])?$req['formId']:'');
        $publicStatus = $$req['publicStatus'];
        if($publicStatus == '0') $status = 'D';
        elseif($publicStatus == '1') $status = 'A';
        if($this->forms_model->updateForm(array(
            'updates' => "`form_status` = '$status'",
            'where' => "`form_id` = '$formId'"))) {
            $this->json['success'] = true;
        }
    }
}