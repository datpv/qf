<?php
if ( ! defined( 'IA' ) ) exit;
/*
    @Name: Application Accounts Controller
    @Description: manage user accounts
    @Docs:
*/
class Applications_Controllers_Accounts extends Applications_Controller {
    public 
        $account_configs = array(
                'page_limit' => 4,
                'show_h2d' = true,
                'show_skype' = true,
                'user_per_group' => 5.
            )
        ;
    function initiallize() {
        $this->permission = 'Admin';
        parent::initiallize();
        // models
        $this->groups_model = new Applications_Models_Groups($this->registry);
        $this->groupsUsers_model = new Applications_Models_GroupsUsers($this->registry);
        $this->subjects_model = new Applications_Models_Subjects($this->registry);
        $this->subjectsUsers_model = new Applications_Models_SubjectsUsers($this->registry);
    }
    function setConfigs() {
        parent::setConfigs();
        if(isset($this->configs['display']['accounts'])) {
            $this->account_configs = $this->configs['display']['accounts'];
        }
    }
    function index() {
        $page_limit = $this->account_configs['page_limit'];
        $paged = $this->routers['paged'];
        $limit = (($paged - 1)*$page_limit).','.$page_limit;
        list($users, $total) = $this->users_model->getUsers_countSubjects($limit);

        $this->view->users = $this->listUser(array(
                'users' => $users,
            ));
        $this->view->total = $total;
        $this->view->paginate = $this->view->getPaginate(array(
                'total' => $total,
                'paged' => $paged,
                'page_limit' => $page_limit,
                'base_url' => $this->url . 'accounts/'
            ));
        $this->view->title = 'Accounts Manager';
        $this->view->render();
    }
    function listUser($args) {
        $users = $args['users'];
        if(empty($users)) return '';
        $setJson = isset($args['setJson'])?$args['setJson']:false;
        $i=0;
        $json_ = array();
        ob_start();
        while($user = $users->fetch_assoc()) {
            $i++;
            $user_id = $user['user_id'];
            $username = $user['username'];
            $email = $user['user_email'];
            $fullname = $user['fullname'];
            $h2d = $user['h2d'];
            $skype = $user['skype'];
            $status = $user['user_status'];
            $subject_count = $user['subject_count'];
            list($groups, $total) = $this->groups_model->getGroups_byUser($user_id);
            if($setJson) {
                $json_[$user_id]['Name'] = $username;
                $json_[$user_id]['Email'] = $email;
                $json_[$user_id]['Fullname'] = $fullname;
                $json_[$user_id]['NickH2d'] = $h2d;
                $json_[$user_id]['NickSkype'] = $skype;
                $json_[$user_id]['IsActivated'] = ($status=='A')?'A:':'D:';
            }
            ?>
            <li class="<?php echo ($i%2==0)?'even':''; ?>" id="user_<?php echo $user_id; ?>">
                <div class="col col-1">
                    <div>
                        <strong><?php echo __('txt_account_name') ?>:</strong> <a id="link<?php echo $user_id; ?>" class="view account_name" target="_blank" href="<?php echo $siteUrl ?>dashboard/&user_id=<?php echo $user_id; ?>"><?php echo $username; ?></a>
                    </div>
                    <div>
                        <strong><?php echo __('txt_email') ?>:</strong> <?php echo $email; ?>
                    </div>
                    <div>
                        <strong><?php echo __('txt_fullname') ?>:</strong> <?php echo $fullname; ?>
                    </div>
                    <?php
                    if($this->account_configs['show_h2d']) {
                    ?>
                    <div>
                        <strong><?php echo __('txt_nick_h2d') ?>:</strong> <a target="_blank" href="http://hoctudau.com/h2d/index.php?qa=user&qa_1=<?php echo $h2d; ?>"><?php echo $h2d; ?></a>
                    </div>
                    <?php
                    }
                    if($this->options['show_skype']) {
                    ?>
                    <div>
                        <strong><?php echo __('txt_nick_skype') ?>:</strong> <a href="skype:<?php echo $skype; ?>?chat"><?php echo $skype; ?></a>
                    </div>
                    <?php
                    }
                    ?>
                </div>
                <div class="col col-2">
                    <?php
                    echo '<a onclick="subject_popup(this); return false;" href="#">'.__('txt_subject_registered'). ': ' . $subject_count . '</a>';
                    ?>
                </div>
                <div class="col col-3">
                    &nbsp;
                </div>
                <div class="col col-4">
                    <div class="group">
                        <form action="#">
                            <ul>
                            <?php
                            if(!empty($groups)) foreach($groups as $group) {
                                ?>
                                <li id="group_<?php echo $group['group_id']; ?>">
                                    <strong><?php echo $group['group_name']; ?></strong>
                                    <a style="color: #221989;" onclick="changeGroup(this); return false;" class="addgroup button" href="#"><?php echo __('txt_change',array('[what]' => __('txt_group'))); ?></a>&nbsp;&nbsp;
                                    <a style="color: #FF2A2A;" onclick="deleteGroup(this); return false;" class="button" href="#"><?php echo __('txt_delete',array('[what]' => __('txt_group'))); ?></a>
                                </li>
                                <?php
                            }
                            ?>
                            </ul>
                            <div>
                            <a onclick="addGroup(this); return false;" class="addgroup button" href="#"><?php echo __('txt_add',array('[what]' => __('txt_group'))); ?></a>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col col-5">
                    <strong>
                        <a class="button" rel="<?php echo $user_id; ?>" onclick="deleteAccount(this); return false;" href="#" style="color: red;"><?php echo __('txt_delete',array('[what]' => __('txt_account'))); ?></a>
                    </strong><br /><br />
                    <?php if($status == 'A'): ?>
                        <a class="button" rel="<?php echo $user_id; ?>" onclick="disableAccount(this); return false;" style="color: green;" href="#"><?php echo __('txt_activated'); ?></a>
                    <?php else: ?>
                        <a class="button" href="#" rel="<?php echo $user_id; ?>" onclick="activeAccount(this); return false;" style="color: red;"><?php echo __('txt_disabled'); ?></a>
                    <?php endif; ?>
                </div>
            </li>
            <?php
        }
        if($setJson) {
            $this->json['response']['json'] = $json_;
        }
        return ob_get_clean();
    }
    /* ---------------------------------------------------------------------------- */
    /* ------------------------ AJAX ----------------------------- */
    /* ---------------------------------------------------------------------------- */

    function accounts_search($req) {
        $searchString = isset($req['searchString'])?$req['searchString']:'';
        $searchString = $this->helpers->str_escape($searchString);
        $page_limit = $this->account_configs['page_limit'];
        $routers = $this->routers;
        $paged = $routers['paged'];
        $limit = (($paged - 1)*$page_limit).','.$page_limit;
        list($users, $total) = $this->users_model->getSearchUsers_countSubjects($searchString,$limit);
        if(empty($users)) {
            return false;
        }
        $html = $this->listUser(array(
                'users' => $users,
                'setJson' => true,
            ));
        $this->json['success'] = true;
        $this->json['response']['html'] = $html;
    }
    function accounts_activeDisable($req) {
        $user_id = isset($req['user_id'])?$req['user_id']:'';
        $user_id = $this->helpers->str_escape($user_id);
        if($user_id != 1) {
            $user = $this->users_model->getUser(array('where' => "`user_id` = '$user_id'"));
            if(empty($user)) return false;
            $status = $user['status'];
            if($status == 'A') $status = 'D';
            else $status = 'A';
            if($this->users_model->updateUser(array(
                'updates'=>"`user_status` = '$status'", 
                'where' => "`user_id` = '$user_id'"))) {
                $this->json['success'] = 'true';
            }
        }
    }
    function accounts_delete($req) {
        $user_id = isset($req['user_id'])?$req['user_id']:'';
        $user_id = $this->helpers->str_escape($user_id);
        if($user_id != 1) {
            if($this->users_model->deleteUser(array('where' => "`user_id` = '$user_id'"))) {
                $this->entries_model->deleteEntry(array('where' => "`entry_user` = '$user_id'"));
                $this->caches_model->deleteCachedEntries("`cache_user_id` = '$user_id'");
            }
            if(empty($this->users_model->getErrors())) $this->json['success'] = 'true';
            else $this->json['response']['html'] = $this->users_model->getErrors();
        }
    }
    function accounts_addGroup($req) {
        $user_id = isset($req['user_id'])?$req['user_id']:'';
        $group_id = isset($req['group_id'])?$req['group_id']:'';
        $user_id = $this->helpers->str_escape($user_id);
        $group_id = $this->helpers->str_escape($group_id);
        $user_per_group = $this->account_configs['user_per_group'];
        if($user_id != 1) {
            $user_count = $this->groupsUsers_model->getRows(array(
                'where' => "`group_id` = '$group_id'"));
            if($user_count < $user_per_group) {
                $group_user = $this->groupsUsers_model->getGU(array(
                    'where' => "`group_id` = '$group_id' AND `user_id` = '$user_id'"));
                if(empty($group_user)) {
                    if($this->groupsUsers_model->insertGU(array(
                        'fields' => '`group_id`,`user_id`',
                        'values' => "'$group_id','$user_id'"))) {
                        $this->json['success'] = true;
                    }
                } else {
                    $this->json['response']['message'] = 'Duplicated Group For This User';
                }
            } else {
                $this->json['response']['message'] = 'Maximum User On This Group Allowed';
            }
        } else {
            $this->json['response']['message'] = 'Admin User Should Not Join Group';
        }
    }
    function accounts_changeGroup($req) {
        $user_id = isset($req['user_id'])?$req['user_id']:'';
        $group_id = isset($req['group_id'])?$req['group_id']:'';
        $old_group_id = isset($req['old_group_id'])?$req['old_group_id']:'';
        $user_id = $this->helpers->str_escape($user_id);
        $group_id = $this->helpers->str_escape($group_id);
        $old_group_id = $this->helpers->str_escape($old_group_id);
        $user_per_group = $this->account_configs['user_per_group'];
        if($user_id != 1) {
            $user_count = $this->groupsUsers_model->getRows(array(
                'where' => "`group_id` = '$group_id'"));
            if($user_count <= $user_per_group) {
                $group_user = $this->groupsUsers_model->getGU(array(
                    'where' => "`group_id` = '$group_id' AND `user_id` = '$user_id'"));
                if(empty($group_user)) {
                    if($this->groupsUsers_model->updateGU(array(
                        'updates' => "`group_id` = '$group_id'",
                        'where' => "`user_id` = '$user_id' AND `group_id` = '$old_group_id'",
                        'limit' => '1'))) {
                        $this->json['success'] = true;
                    }
                } else {
                    $this->json['response']['message'] = 'Duplicated Group For This User';
                }
            } else {
                $this->json['response']['message'] = 'Maximum User On This Group Allowed';
            }
        } else {
            $this->json['response']['message'] = 'Admin User Should Not Join Group';
        }
    }
    function accounts_deleteGroup($req) {
        $user_id = isset($req['user_id'])?$req['user_id']:'';
        $group_id = isset($req['group_id'])?$req['group_id']:'';
        $user_id = $this->helpers->str_escape($user_id);
        $group_id = $this->helpers->str_escape($group_id);
        if($user_id != 1) {
            if($this->groupsUsers_model->deleteGU(array(
                'where' => "`user_id` = '$user_id' AND `group_id` = '$group_id'"))) {
                $this->json['success'] = true;
            }
        }
    }
    function accounts_lockUnlockSubject($req) {
        $subject_id = isset($req['subject_id'])?$req['subject_id']:'';
        if(empty($subject_id)) {
            return false;
        }
        $subject_id = $this->helpers->str_escape($subject_id);
        $subject_result = $this->subjects_model->getSubject(array('where'=>"`subject_id` = '$subject_id'"));
        if(empty($subject_result)) {
            return false;
        }
        $user_id = isset($req['user_id'])?$req['user_id']:'';
        $user_id = $this->helpers->str_escape($user_id);
        $user = $this->users_model->getUser(array('where' => "`user_id` = '$user_id'"));
        if(empty($user)) {
            return;
        }
        if($subject_result['subject_unlocked'] == 'Y') $l = 'N';
        else $l = 'Y';
        if($this->subjectsUsers_model->updateSU(array(
            'updates' => "`subject_unlocked` = '$l'",
            'where' => "`subject_id` = '$subject_id' AND `user_id` = '$user_id'"))) {
            $this->json['success'] = true;
        } else {
            $this->json['response']['message'] = 'Some Error Occured';
        }
    }
}