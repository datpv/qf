<?php
if ( ! defined( 'IA' ) ) exit;
/*
    @Name: Application Forms Controller
    @Description: display forms
    @Docs:
    LINE 477
*/
class Applications_Controllers_Forms extends Applications_Controllers_Fields {
    public
        $fv_configs = array(
                'page_limit' => 4,
            )
        ;
    function __construct($registry) {
        parent::__construct($registry);
    }
    
    function initiallize() {
        $this->permission = 'All';
        parent::initiallize();
        // models
        $this->forms_model = new Applications_Models_Forms($this->registry);
        $this->entries_model = new Applications_Models_Entries($this->registry);
        $this->entriesForms_model = new Applications_Models_EntriesForms($this->registry);
        $this->categories_model = new Applications_Models_Categories($this->registry);
        $this->categoriesForms_model = new Applications_Models_CategoriesForms($this->registry);
        $this->themes_model = new Applications_Models_Themes($this->registry);
        $this->subjects_model = new Applications_Models_Subjects($this->registry);
        $this->subjectsForms_model = new Applications_Models_SubjectsForms($this->registry);
        $this->subjectsUsers_model = new Applications_Models_SubjectsUsers($this->registry);
    }
    function setConfigs() {
        parent::setConfigs();
        if(isset($this->configs['display']['formsview'])) {
            $this->fv_configs = $this->configs['display']['formsview'];
        }
    }
    /**
    *   *** FORM HELPER FUNCTIONS ***
    */
    function checkSubject($args) {
        extract($args);
        $form_id = $form['form_id'];
        // subject forms
        list($sfs, $total) = $this->subjectsForms_model->getSFs(array(
            'where' => "`form_id` = '$form_id'"));
        if(!empty($sfs)) {
            $unlocked = $uncompleted = false;

            if(!empty($this->user)) {
                if($this->user['user_type'] == 'A') {
                    return;
                }
                $user_id = $this->user['user_id'];
                // subject ids
                $sids = array();
                foreach($sfs as $sf) {
                    $sids[] = $sf['subject_id'];
                }
                $sids = implode(',',$sids);
                if(empty($sids)) $sids = "'0'";

                list($sus, $total) = $this->subjectsUsers_model->getSUsRP("SELECT * FROM {P}subject_users a , {P}subjects b ON a.`subject_id` = b.`subject_id` WHERE a.`user_id` = '$user_id' AND a.`subject_id` IN ($sids)");
                if(!empty($sus)) {
                    foreach($sus as $su) {
                        if($su['subject_unlocked'] == 'N') {
                            $unlocked = false;
                        } else {
                            $unlocked = true;
                            $subject_settings = $this->helpers->jd2a($su['subject_settings']);
                            $form_order = $subject_settings['FormOrder'];
                            foreach($form_order as $kfo => $fo) {
                                if($fo == $form_id) {
                                    if($kfo > 0) {
                                        $form_id_check = $form_order[$kfo-1];
                                        $entry = $this->entries_model->queryRP("SELECT a.`entry_id` FROM {P}entries_meta a, {P}entries b ON a.`entry_id` = b.`entry_id` WHERE  b.`entry_user` = '$user_id' AND a.`form_id` = '$form_id_check' LIMIT 1");
                                        if(empty($entry)) {
                                            $uncompleted = true;
                                        }
                                    }
                                    break;
                                }
                            }
                            break;
                        }
                    }
                }
            }
            if(!$unlocked) {
                $this->message('You Need To Unlock This Form First');
                exit();
            }
            if($uncompleted) {
                $this->message('You Must Completed Previous Form');
                exit();
            }
            
        }
    }
    function checkPassword($args) {
        extract($args);
        $form_pass = $form['form_password'];
        if(!$isPost) {
            if(!empty($form_pass)) {
                if(!isset($_SESSION['form_'.$formId])) {
                    $this->view->title = ' Unauthorized access.';
                    $this->setAction('password_protect');
                    $this->view->render('single');
                    exit();
                }
            }
        } else {
            if(!empty($form_pass)) {
                if(!isset($_SESSION['form_'.$formId])) {
                    $post_pass = isset($_POST['passKey'])?$_POST['passKey']:'';
                    if(empty($post_pass)) {
                        $this->view->errors['protect_password'] = 'Wrong password!';
                    }
                    $md5_post_pass = md5($this->helpers->str_escape($post_pass));
                    if( $md5_post_pass == $form_pass ) {
                        $_SESSION['form_'.$formId] = $formId;
                    }
                    $this->setAllowNext(true);
                    exit();
                }
            }
        }
    }
    function checkDatetime($args) {
        extract($args);
        $start_date = $form_content['StartDate'];
        $end_date = $form_content['EndDate'];
        $current_date = $this->helpers->get_datetime();
        if((strtotime($end_date) < strtotime($current_date)) || (strtotime($start_date) > strtotime($current_date))) {
            $this->message('This form has expired!');
            exit();
        }
    }
    function checkFields($args) {
        extract($args);
        if($formType == 'autoscore' || $normal_login) {
            if(!empty($f)) {
                $part = explode('_',$this->helpers->decrypt($f,'dpkey'));
                if(!isset($part[1])) {
                    $this->notfound();
                    exit();
                }
                $part2 = explode('-',$part[1]);
                if(!isset($part2[1]) || !is_numeric($part2[1])) {
                    $this->notfound();
                    exit();
                }
                $field_id = $part2[1];
            }
        }
        $fields = $_form['Fields'];
        $title = $_form['Name'];
        $singleField = false;
        if($field_id) {
            $fields = $this->getFieldById($fields, $field_id);
            if(!$fields) {
                $this->notfound();
                exit();
            }
            $title = $fields[0]['Title'];
            $singleField = true;
        }
        return array($title, $fields, $field_id, $singleField);
    }
    function checkEntryLimit($args) {
        extract($args);
        if($_form['UniqueIP'] || $_form['EntryLimit'] > 0) {
            $ents = $this->entries_model->entryOnce($form_id);
            if($_form['UniqueIP']) {
                if(!empty($ents)) {
                    while($ent = $ents->fetch_assoc()) {
                        if(IP == $ent['entry_ip']) {
                            $this->message('Sorry, but this form is limited to one submission per user.');
                            exit();
                        }
                    }
                }
            } elseif(count($ents) > $_form['EntryLimit']) {
                $this->message('Limited submission for this form. Sory!');
                exit();
            }
        }
    }
    function checkLogin($args) {
        extract($args);
        $normal_login = false;
        if($formType == 'normal') {
            if(isset($form_content['UserLogin']) && $form_content['UserLogin']) {
                $normal_login = true;
            }
        }
        // check login
        if($formType == 'nopbai' || $formType == 'score' || $formType == 'autoscore' || $normal_login) {
            if(empty($this->user)) {
                $this->redirect();
                exit();
            }
        }
        return $normal_login;
    }
    function checkSubmited($args) {
        extract($args);
        $userScored = $entry_content = '';
        if($submit_type == 'score') {
            if(empty($entryId)) {
                $this->notfound();
                exit();
            }
            $entry_content = $this->entries_model->getEntry(array(
                    'where' => "`entry_uuid` = '$entryId'"
                ));
            if(empty($entry_content)) {
                $this->notfound();
                exit();
            }
            // check scoring 1
            if($entry_content['entry_user'] == $user_scoring_id) {
                $this->message('Bạn Không Được Phép Chấm Bài Của Mình.');
                exit();
            }
            // check scoring 2
            $entry = $this->entries_model->getEntries(array(
                'where' => "`entry_of` = '$entryId' AND `entry_user` = '$user_scoring_id'"));
            if(!empty($entry)) {
                $this->message('Bạn Không Được Phép Chấm Bài Cho Bạn Mình 2 Lần.');
                exit();
            }
        } elseif($submit_type == 'autoscore') {
            $userScored = $this->entries_model->userScored($user_scoring_id, $form_id);
        } elseif($submit_type == 'normal_login') {
            $userScored = $this->entries_model->userSubmited($user_scoring_id, $form_id);
        }
        if($formUserSubmitLimit != 0 && $userScored['entry_flag'] >= $formUserSubmitLimit) {
            $this->message('Bạn Không Được Phép Gửi Bài Này Quá Nhiều Lần. (' . $formUserSubmitLimit . ' Tiny Times)');
            exit();
        }
        return ($userScored, true, $userScored['entry_id'], $userScored['entry_uuid'], $entry_content);
    }
    /**
    *   ***  ***
    */
    function index() {
        $formId = $this->helpers->str_escape($this->routers['pos_1']);
        $entryId = $this->helpers->str_escape($this->routers['pos_2']);
        $f = $this->helpers->str_escape(isset($_GET['f'])?$_GET['f']:'');
        if(empty($formId)) {
            $this->notfound();
            exit();
        }
        $form = $this->forms_model->getForm(array(
            'where' => "`form_uuid` = '$formId' AND `form_status` = 'A' AND `form_approved` = 'Y'"));
        if(empty($form)) {
            $this->notfound();
            exit();
        }
        $field_id = false;
        $errorFields = (array) $this->view->errorFields;
        $entry = '[]';
        $form_id = $form['form_id'];
        $_form = $this->helpers->jd2a($form['form_content']);
        $_form['FormId'] = $formId;
        $_form['Url'] = $formId;
        $formType = $_form['Formtype'];
        $ajaxEnable = isset($_form['AjaxEnable'])?$_form['AjaxEnable']:false;
        $ajaxLiveResponse = isset($_form['AjaxLiveResponse'])?$_form['AjaxLiveResponse']:false;
        $formUserSubmitLimit = isset($_form['UserSubmitLimit'])?$_form['UserSubmitLimit']:1;
        if(!is_numeric($formUserSubmitLimit) || $formUserSubmitLimit < 0) $formUserSubmitLimit = 1;

        // check form in subject
        $this->checkSubject(array(
                'form' => $form
            ));

        // check form password
        $this->checkPassword(array(
                'isPost' => false,
                'form' => $form
            ));

        // check form datetime
        $this->checkDatetime(array(
            'form_content' => $_form,
            ));

        // check login
        $normal_login = $this->checkLogin(array(
                'form_content' => $_form,
                'formType' => $formType
            ));

        // check fields
        list($title, $fields, $field_id, $singleField) = $this->checkFields(array(
                'formType' => $formType,
                'normal_login' => $normal_login,
                'f' => $f,
                'field_id' => $field_id,
                '_form' => $_form,
            ));

        // check user submited
        $submit_type = false;
        if($formType == 'autoscore') {
            $submit_type = 'autoscore';
        } elseif($formType == 'score') {
            $submit_type = 'score';
        } elseif($normal_login) {
            $submit_type = 'normal_login';
        }
        if($submit_type) {
            $user_scoring_id = $this->user['user_id'];
            list($userScored, $updateEntry, $entry_upd_id, $entry_upd_uuid, $entry_content) = $this->checkSubmited(array(
                    'submit_type' => $submit_type,
                    'user_scoring_id' => $user_scoring_id,
                    'form_id' => $form_id,
                    'formUserSubmitLimit' => $formUserSubmitLimit,
                    'entryId' => $entryId
                ));
            if($formType == 'score') {
                $entry_id = $entry_content['entry_id'];
                $user_id = $entry_content['entry_user'];
                if($entries_meta_content = $entries_obj->getEntryMeta("`entry_id` = '$entry_id'")) {
                    $form_nb_id = $entries_meta_content['form_id'];
                    if($form_nb = $forms_obj->getForm('',$form_nb_id)) {
                        $caches_obj = new Controllers_cachesController($this->registry);
                        $users_obj = new Models_UsersModel($this->registry);
                        $user_nb = $users_obj->getUser("`user_id` = '$user_id'");
                        $this->view->formNBDisplayResult = $caches_obj->getFormNBDisplayResult($form_nb,$entry_content,$user_nb);
                    }
                }
            }
        }
        $extra_options = array();
        if($formType == 'autoscore' || $normal_login) {
            if($ajaxEnable) {
                $extra_options['ajaxEnable'] = true;
                $posted_contents = $this->helpers->json_decode_to_array($userScored['entry_content']);
                $posted_ids = array();
                if($posted_contents) foreach($posted_contents as $pk => $posted_content) {
                    if( strpos($pk, 'Field') === 0 ) {
                        $posted_ids[$pk] = $posted_content;
                    }
                }
                $extra_options['posted_ids'] = $posted_ids;
            }
        }
        /*
        * Fields
        */
        if(isset($_form['Fields']['HasntBeenSaved'])) unset($_form['Fields']['HasntBeenSaved']);
        
        
        $hasFieldClass = '';
        $buttonClass = 'hide';
        $html = $this->getDisplayFields($fields,$errorFields,'',$extra_options);
        
        /*
        *
        */
        /*
        * Theme
        */
        $theme_id = $form['theme_id'];
        if($theme = $themes_obj->getTheme('',$theme_id)) {
            $_theme = $this->helpers->json_decode_to_array($theme['theme_content']);
        }
        $this->view->formId = $formId;
        $this->view->entryId = $entryId;
        $this->view->formType = $formType;
        $this->view->_form = $_form;
        $this->view->title = $title;
        $this->view->html = $html;
        $this->view->ajaxEnable = $ajaxEnable;
        $this->view->f = $f;
        $this->view->render('form');
    }
    // submit form
    function forms_submit($req) {
        $formId = $this->helpers->str_escape($this->routers['pos_1']);
        $entryId = $this->helpers->str_escape($this->routers['pos_2']);
        $f = $this->helpers->str_escape(isset($_GET['f'])?$_GET['f']:'');
        if(empty($formId)) {
            $this->notfound();
            exit();
        }
        $form = $this->forms_model->getForm(array(
            'where' => "`form_uuid` = '$formId' AND `form_status` = 'A' AND `form_approved` = 'Y'"));
        if(empty($form)) {
            $this->notfound();
            exit();
        }
        $field_id = false;
        $errorFields = (array) $this->view->errorFields;
        $entry = '[]';
        $form_id = $form['form_id'];
        $_form = $this->helpers->jd2a($form['form_content']);
        $formType = $_form['Formtype'];
        $ajaxEnable = isset($_form['AjaxEnable'])?$_form['AjaxEnable']:false;
        $ajaxLiveResponse = isset($_form['AjaxLiveResponse'])?$_form['AjaxLiveResponse']:false;
        $formUserSubmitLimit = isset($_form['UserSubmitLimit'])?$_form['UserSubmitLimit']:1;
        if(!is_numeric($formUserSubmitLimit) || $formUserSubmitLimit < 0) $formUserSubmitLimit = 1;
        $updateEntry = false;
        $captcha_err = array();

        // check form in subject
        $this->checkSubject(array(
                'form' => $form
            ));

        // check password
        $this->checkPassword(array(
                'isPost' => true,
                'form' => $form
            ));

        // check form datetime
        $this->checkDatetime(array(
            'form_content' => $_form,
            ));

        // check login
        $normal_login = $this->checkLogin(array(
                'form_content' => $_form,
                'formType' => $formType
            ));

        // check fields
        list($title, $fields, $field_id, $singleField) = $this->checkFields(array(
                'formType' => $formType,
                'normal_login' => $normal_login,
                'f' => $f,
                'field_id' => $field_id,
                '_form' => $_form,
            ));

        // Captcha check
        if($_form['UseCaptcha']) {
            if(!$this->helpers->isValidCaptcha($_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"])) {
                $captcha_err['recaptcha_response_field'] = array(
                    'message' => 'The CAPTCHA wasn\'t entered correctly.'
                );
            }
        }

        // IP check
        $this->checkEntryLimit(array(
                'form_content' => $_form,
                'form_id' => $form_id
            ));

        // check user submited
        $submit_type = false;
        if($formType == 'autoscore') {
            $submit_type = 'autoscore';
        } elseif($formType == 'score') {
            $submit_type = 'score';
        } elseif($normal_login) {
            $submit_type = 'normal_login';
        }
        if($submit_type) {
            $user_scoring_id = $this->user['user_id'];
            list($userScored, $updateEntry, $entry_upd_id, $entry_upd_uuid, $entry_content) = $this->checkSubmited(array(
                    'submit_type' => $submit_type,
                    'user_scoring_id' => $user_scoring_id,
                    'form_id' => $form_id,
                    'formUserSubmitLimit' => $formUserSubmitLimit
                    'entryId' => $entryId
                ));
        }
        // posting content
        $posted = 
        // user posted content
        $posted1 = array();
        // validate fields
        $errorFields = $this->validateFields($fields,$posts);
        // get email confirm fields
        $sendConfirm = $this->getFieldEmailConfirm($fields);
        // group errors
        $errorFields = $errorFields + $captcha_err;
        if(empty($errorFields)) {
            if($updateEntry) {
                $posted1 = $this->helpers->jd2a($userScored['entry_content']);
                if($ajaxEnable && !$singleField) {
                    $posts = array_merge($posts, $posted1);
                } else {
                    $posts = array_merge($posted1, $posts);
                }
            }
            $posted = $this->helpers->jeae($posts);
            $alt1 = $alt2 = $alt3 = $alt4 = '';
            $comp = 1;
            if(!empty($sendConfirm)) {
               $alt1 = ",`entry_complete`";
               $alt2 = ",'0'";
               $comp = 0;   
            }
            /*
            * Params To Insert To DB
            */
            $user_id = null;
            if(!empty($this->user)) {
                $user_id = $this->user['user_id'];
            }
            $entry_uuid = $this->helpers->get_uuid();
            if($formType == 'nopbai') {
                $alt3 = ",`entry_uuid`,`entry_type`,`entry_user`";
                $alt4 = ",'$entry_uuid','NB','$user_id'";
            } elseif($formType == 'score') {
                $scored = $this->scoring($posts, $fields);
                $scores = $this->helpers->jeae($scored);
                $alt3 = ",`entry_uuid`,`entry_type`,`entry_user`,`entry_of`,`entry_scoring`";
                $alt4 = ",'$entry_uuid','SC','$user_id','$entryId','$scores'";
            } elseif($formType == 'autoscore') {
                $scores2 = $this->scoring($posts, $fields);
                $flag = 1;
                if($ajaxEnable && !$singleField) {
                    $flag = count($fields) - $this->helpers->get_key_count($posted1, 'Field');
                    if($flag <= 0) $flag = 1;
                }
                $scores = $this->helpers->jeae($scores2);
                $alt3 = ",`entry_uuid`,`entry_type`,`entry_user`,`entry_of`,`entry_scoring`,`entry_flag`";
                $alt4 = ",'$entry_uuid','AS','$user_id','$entry_uuid','$scores','$flag'";
                $upd1 = ",`entry_scoring` = '$scores', `entry_flag` = `entry_flag` + $flag";
            } elseif($normal_login) {
                $flag = 1;
                if($ajaxEnable && !$singleField) {
                    $flag = count($fields) - $this->helpers->get_key_count($posted1, 'Field');
                    if($flag <= 0) $flag = 1;
                }
                $alt3 = ",`entry_user`,`entry_flag`";
                $alt4 = ",'$user_id','$flag'";
                $upd1 = ",`entry_flag` = `entry_flag` + $flag";
            }
            $entd = $this->helpers->get_datetime();
            if($updateEntry) {
                if($this->entries_model->updateEntry(array(
                    'updates' => "`entry_ip` = '$ip', `entry_update` = '$entd', `entry_content` = '$posted' $upd1",
                    'where' => "`entry_id` = '$entry_upd_id'",
                    'limit' => '1'))) {
                    if($formType == 'autoscore') {
                        $entry_content = $entries_obj->getEntry('',$entry_upd_id);
                        
                        $user_id2 = $entry_content['entry_user'];
                        $user_nb = $users_obj->getUser("`user_id` = '$user_id2'");
                        $display_ = $caches_obj->getFormASDisplayResult($result,$entry_content,$user_nb);
                        $this->view->display_ = $display_;
                        $this->view->entry_uuid = $entry_upd_uuid;
                        // Wrong Answers Suggestions
                        $scoring_detail = $this->helpers->scoring_detail($fields,$entry_content);
                        
                        $logs = array();
                        foreach($scoring_detail as $sd) {
                            $field_id = $sd['FieldId'];
                            
                            $log_subject = $sd['Title'];
                            if(is_string($sd['Value'])) $log_subject = $sd['Value'];
                            $log_subject = $this->helpers->escape_string(trim($log_subject));
                            $log = $logs_obj->getLog("`form_id` = '$form_id' AND `field_id` = '$field_id' AND `log_subject` = '$log_subject'");
                            $old_logconten = $this->helpers->json_decode_to_array($log['log_content']);
                            $old_logconten = is_array($old_logconten)?$old_logconten:array();
                            if($ajaxEnable && !$singleField) {
                                $sd = array_merge($sd, $old_logconten);
                            } else {
                                $sd = array_merge($old_logconten, $sd);
                            }
                            $log_content = $this->helpers->json_encode_then_escape($sd);
                            
                            if($log) {
                                $log_id = $log['log_id'];
                                $logs_obj->updateLog("`log_hit` = `log_hit` + 1, `log_update` = '$entd', `log_content` = '$log_content', `log_subject` = '$log_subject'", "`log_id` = '$log_id'");
                            } else {
                                $logs_obj->insertLog('`form_id`, `field_id`, `log_subject`, `log_content`', "'$form_id', '$field_id', '$log_subject', '$log_content'");
                                $log_id = $logs_obj->registry->mysqli->insert_id;
                                
                                $logs_obj->insertLogUser('`user_id`, `log_id`', "'$user_id', '$log_id'");
                                
                                $log = array(
                                    'log_id' => $log_id,
                                    'field_id' => $field_id,
                                    'log_subject' => $sd['Value'],
                                    'log_content2' => $sd,
                                    'log_hit' => 1
                                );
                            }
                            // $log_user = $logs_obj->getLogUser("`user_id` = '$user_id' AND `log_id` = '$log_id'");
                            // if(!$log_user) {
                            // $logs_obj->insertLogUser('`user_id`, `log_id`', "'$user_id', '$log_id'");
                            // }
                            $logs[] = $log;
                        }
                        $this->view->scoring = $scoring_detail;
                        $this->view->logs = $logs;
                        
                        $this->view->render('yourscore','only');
                        return;
                    }
                    header('location: '.$siteUrl.'confirm/'.$formId);
                    exit();
                }
            } else {
                $_notify = $this->helpers->json_decode_to_array($form['form_notify']);
                if($entries_obj->insertEntry("`entry_ip`,`entry_content` $alt1 $alt3", "'$ip','$posted' $alt2 $alt4")) {
                    $entry_id = $entries_obj->registry->mysqli->insert_id;
                    if($entries_obj->insertEntryMeta('`form_id`,`entry_id`', "'$form_id','$entry_id'")) {
                        if(!empty($_notify['Email'])) {
                            $html = 'Click <a href="'.$siteUrl.'entries/'.$formId.'">here</a> for details';
                            $address[$_notify['Email']] = $_notify['ConfirmationFromAddress'];
                            $replyTo = isset($posts['Field'.$_notify['ReplyTo']])?$posts['Field'.$_notify['ReplyTo']]:'';
                            $options[] = $_notify['ConfirmationFromAddress'];
                            //$_notify['ConfirmationSubject'];
                            $this->helpers->sendMail('New Submission from: '.$_form['Name'],$html, $address, $replyTo, $options);
                        }
                        if(!empty($sendConfirm)) {
                            if(!empty($posts['Field'.$sendConfirm['ColumnId']])) {
                                $str = $entry_id . '' . md5($entry_id);
                                $html = 'Click <a href="'.$siteUrl.'confirm/&a='.$str.'">here</a> to confirm your submission.';
                                $address[$posts['Field'.$sendConfirm['ColumnId']]] = $_form['FromAddress'];
                                $replyTo = $_form['ReceiptReplyTo'];
                                $options[] = $_form['FromAddress'];
                                //$_notify['ConfirmationSubject'];
                                $this->helpers->sendMail('Confirm submission from '.$_form['Name'],$html, $address, $replyTo, $options);
                            }
                        }
                        if($formType == 'autoscore') {
                            $caches_obj = new Controllers_cachesController($this->registry);
                            $entry_content = $entries_obj->getEntry('',$entry_id);
                            $user_id2 = $entry_content['entry_user'];
                            $user_nb = $users_obj->getUser("`user_id` = '$user_id2'");
                            $display_ = $caches_obj->getFormASDisplayResult($result,$entry_content,$user_nb);
                            $this->view->display_ = $display_;
                            $this->view->entry_uuid = $entry_uuid;
                            
                            // Wrong Answers Suggestions
                            $scoring_detail = $this->helpers->scoring_detail($fields,$entry_content);
                            $logs = array();
                            foreach($scoring_detail as $sd) {
                                $field_id = $sd['FieldId'];
                                
                                $log_subject = $sd['Title'];
                                if(is_string($sd['Value'])) $log_subject = $sd['Value'];
                                $log = $logs_obj->getLog("`form_id` = '$form_id' AND `field_id` = '$field_id'");
                                
                                $old_logconten = $this->helpers->json_decode_to_array($log['log_content']);
                                $old_logconten = is_array($old_logconten)?$old_logconten:array();
                                if($ajaxEnable && !$singleField) {
                                    $sd = array_merge($sd, $old_logconten);
                                } else {
                                    $sd = array_merge($old_logconten, $sd);
                                }
                                $log_content = $this->helpers->json_encode_then_escape($sd);
                                
                                if($log) {
                                    $log_id = $log['log_id'];
                                    $logs_obj->updateLog("`log_hit` = `log_hit` + 1, `log_update` = '$entd', `log_content` = '$log_content', `log_subject` = '$log_subject'", "`log_id` = '$log_id'");
                                } else {
                                    $logs_obj->insertLog('`form_id`, `field_id`, `log_subject`, `log_content`', "'$form_id', '$field_id', '$log_subject', '$log_content'");
                                    $log_id = $logs_obj->registry->mysqli->insert_id;
                                    
                                    $logs_obj->insertLogUser('`user_id`, `log_id`', "'$user_id', '$log_id'");
                                    
                                    $log = array(
                                        'log_id' => $log_id,
                                        'field_id' => $field_id,
                                        'log_subject' => $log_subject,
                                        'log_content2' => $sd,
                                        'log_hit' => 1
                                    );
                                }
                                $logs[] = $log;
                            }
                            $this->view->scoring = $scoring_detail;
                            $this->view->logs = $logs;
                            
                            
                            $this->view->render('yourscore','only');
                            return;   
                        }
                        header('location: '.$siteUrl.'confirm/'.$formId);
                        exit();
                    }
                }
            }
            if(!empty($this->registry->mysqli->error)) {
                $errors['field'] = 'Some errors occured!';
            }
        } else {
            $errors['field'] = 'Some errors occured!';
        }
        foreach($posts as $k => $post) {
            if(substr($k,0,5) == 'Field') {
                $posted[$k] = $post;
            }
        }
        $entry = json_encode($posted);
        $this->view->entry = $entry;
        if(isset($errorFields)) $this->view->errorFields = $errorFields;
        $this->view->errors = $errors;
        $this->setAllowNext(true);
        
        /*
        *
        */
    }
}