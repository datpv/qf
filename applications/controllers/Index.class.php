<?php
if ( ! defined( 'IA' ) ) exit;
/*
    @Name: Application Index Controller
    @Description: manage homepage logic
    @Docs:
*/
class Applications_Controllers_Index extends Applications_Controller {
    function __construct($registry) {
        parent::__construct($registry);
    }
    function initiallize() {
        parent::initiallize();
    }
    function setConfigs() {
        parent::setConfigs();
    }
    function index() {
        $this->view->render();
    }

}