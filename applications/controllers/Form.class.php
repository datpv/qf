<?php
if ( ! defined( 'IA' ) ) exit;
/*
    @Name: Application Forms Controller
    @Description: display forms
    @Docs:
    LINE 339
*/
class Applications_Controllers_Form extends Applications_Controllers_Fields {
    public
        $fv_configs = array(
            'page_limit' => 4,
        ),
        $ajax_enable,
        $ajax_live_response,
        $form_content,
            $form_type,
            $form_user_submit_limit,
        $normal_login = false,
        $form_id,
        $form_uuid,
        $entry_uuid,
        $fields,
            $is_single_field = false,
            $field_id,
            $field_uuid,
        $entry,
        $userScored = false,
        $title,
        $theme;

    function __construct($registry) {
        parent::__construct($registry);
    }
    function initiallize() {
        $this->permission = 'All';
        parent::initiallize();
        // models
        $this->forms_model = new Applications_Models_Forms($this->registry);
        $this->entries_model = new Applications_Models_Entries($this->registry);
        $this->entriesForms_model = new Applications_Models_EntriesForms($this->registry);
        $this->categories_model = new Applications_Models_Categories($this->registry);
        $this->categoriesForms_model = new Applications_Models_CategoriesForms($this->registry);
        $this->themes_model = new Applications_Models_Themes($this->registry);
        $this->subjects_model = new Applications_Models_Subjects($this->registry);
        $this->subjectForm_model = new Applications_Models_SubjectForm($this->registry);
        $this->subjectsUsers_model = new Applications_Models_SubjectsUsers($this->registry);
    }
    function setConfigs() {
        parent::setConfigs();
        if(isset($this->configs['display']['formsview'])) {
            $this->fv_configs = $this->configs['display']['formsview'];
        }
    }
    function getData() {
        $this->form_uuid = $this->getRouterParam('pos_1');
        $this->entry_uuid = $this->getRouterParam('pos_2');
        $this->field_uuid = $this->getUrlParam('f');

        $this->empty_then_notfound($this->form_uuid);

        $this->object = $this->forms_model->getObject(array(
            'where' => "`form_uuid` = '{$this->form_uuid}' AND `form_status` = 'A' AND `form_approved` = 'Y'"));

        $this->empty_then_notfound($this->object);

        $this->form_id = $this->object['form_id'];
        $this->form_content = $this->helpers->jd2a($this->object['form_content']);
        $this->form_type = $this->form_content['Formtype'];
        $this->fields = $this->form_content['Fields'];
        $this->title = $this->form_content['Name'];
        $this->ajax_enable = 
                    $this->isset_get($this->form_content['AjaxEnable']);
        $this->ajax_live_response = 
                    $this->isset_get($this->form_content['AjaxLiveResponse']);
        $this->form_user_submit_limit = 
                    $this->isset_get($this->form_content['UserSubmitLimit'], 1);
        if(!is_numeric($this->form_user_submit_limit) || $this->form_user_submit_limit < 0) 
            $this->form_user_submit_limit = 1;

        // check login
        $this->checkLogin();

        // check password
        $this->checkPassword();

        // check form datetime
        $this->checkDatetime();

        // check form in subject
        $this->checkSubject();

        // check fields
        $this->checkFields();

        // IP, Limit check
        $this->checkEntryLimit();

        // check user submited
        $this->isUpdate = false;
        if($this->form_type == 'autoscore') {
            $this->isUpdate = true;
            $this->userScored = $this->entries_model->userScored($this->user['user_id'], $this->form_id);

        } elseif($this->normal_login) {
            $this->isUpdate = true;
            $this->userScored = $this->entries_model->userSubmited($this->user['user_id'], $this->form_id);

        } elseif($this->form_type == 'score') {
            $this->isUpdate = true;

            $this->empty_then_notfound($this->entry_uuid);

            $this->entry = $this->entries_model->getObject(array(
                    'where' => "`entry_uuid` = '{$this->entry_uuid}'"
                ));

            $this->empty_then_notfound($this->entry);

            // check scoring 1
            if($this->entry['entry_user'] == $this->user['user_id']) {
                $this->message('Bạn Không Được Phép Chấm Bài Của Mình.');
            }

            // check scoring 2
            $entries = $this->entries_model->getObjects(array(
                'where' => "`entry_of` = '{$this->entry_uuid}' AND `entry_user` = '{$this->user['user_id']}'"));

            if(!empty($entries)) {
                $this->message('Bạn Không Được Phép Chấm Bài Cho Bạn Mình 2 Lần.');
            }

        }
        if($this->isUpdate) {
            if($this->form_user_submit_limit != 0 && $this->userScored && $this->userScored['entry_flag'] >= $this->form_user_submit_limit) {
                $this->message('Bạn Không Được Phép Gửi Bài Này Quá Nhiều Lần. (' . $this->form_user_submit_limit . ' Tiny Times)');
            }
        }
        $this->view->formId = $this->form_uuid;
        $this->view->entryId = $this->entry_uuid;
        $this->view->_form = $this->form_content;
        $this->view->title = $this->title;
        $this->view->ajaxEnable = $this->ajax_enable;
        $this->view->f = $this->field_uuid;
    }
    /**
    *   *** FORM HELPER FUNCTIONS ***
    */
    function checkSubject() {
        // subject forms
        list($objects, $total) = $this->subjectForm_model->getObjects(array(
            'where' => "`form_id` = '{$this->form_id}'"));
        if(!empty($objects)) {
            $unlocked = $uncompleted = false;

            if(!empty($this->user)) {
                if($this->user['user_type'] == 'A') {
                    return;
                }
                $user_id = $this->user['user_id'];
                // subject ids
                $sids = array();
                foreach($objects as $object) {
                    $sids[] = $object['subject_id'];
                }
                $sids = implode(',',$sids);
                if(empty($sids)) $sids = "'0'";

                list($sus, $total) = $this->subjectsUsers_model->getObjectsRP("SELECT * FROM {P}subject_users a , {P}subjects b ON a.`subject_id` = b.`subject_id` WHERE a.`user_id` = '$user_id' AND a.`subject_id` IN ($sids)");
                if(!empty($sus)) {
                    foreach($sus as $su) {
                        if($su['subject_unlocked'] == 'N') {
                            $unlocked = false;
                        } else {
                            $unlocked = true;
                            $subject_settings = $this->helpers->jd2a($su['subject_settings']);
                            $form_order = $subject_settings['FormOrder'];
                            foreach($form_order as $kfo => $fo) {
                                if($fo == $form_id) {
                                    if($kfo > 0) {
                                        $form_id_check = $form_order[$kfo-1];
                                        $entry = $this->entries_model->queryRP("SELECT a.`entry_id` FROM {P}entries_meta a, {P}entries b ON a.`entry_id` = b.`entry_id` WHERE  b.`entry_user` = '$user_id' AND a.`form_id` = '$form_id_check' LIMIT 1");
                                        if(empty($entry)) {
                                            $uncompleted = true;
                                        }
                                    }
                                    break;
                                }
                            }
                            break;
                        }
                    }
                }
            }
            if(!$unlocked) {
                $this->message('You Need To Unlock This Form First');
                exit();
            }
            if($uncompleted) {
                $this->message('You Must Completed Previous Form');
                exit();
            }
            
        }
    }
    function checkPassword() {
        $form_pass = $this->object['form_password'];
        
        if(!empty($form_pass)) {
            if(!isset($_SESSION['form_'.$this->form_id])) {
                if($this->isPost) {
                    $post_pass = isset($_POST['passKey'])?$_POST['passKey']:'';
                    if(empty($post_pass)) {
                        $this->view->errors['protect_password'] = 'Wrong password!';
                    }
                    $md5_post_pass = md5($this->helpers->str_escape($post_pass));
                    if( $md5_post_pass == $form_pass ) {
                        $_SESSION['form_'.$this->form_id] = $this->form_id;
                        return;
                    }
                }
                $this->view->title = ' Unauthorized access.';
                $this->setAction('password_protect');
                $this->view->render('single');
                exit();
                }
            }
    }
    function checkDatetime() {
        $start_date = $this->form_content['StartDate'];
        $end_date = $this->form_content['EndDate'];
        $current_date = $this->helpers->get_datetime();
        if((strtotime($end_date) < strtotime($current_date)) || (strtotime($start_date) > strtotime($current_date))) {
            $this->message('This form has expired!');
        }
    }
    function checkFields() {
        if($this->form_type == 'autoscore' || $this->normal_login) {
            if(!empty($f)) {
                $part = explode('_',$this->helpers->decrypt($this->field_uuid,'dpkey'));
                if(!isset($part[1])) {
                    $this->notfound();
                    exit();
                }
                $part2 = explode('-',$part[1]);
                if(!isset($part2[1]) || !is_numeric($part2[1])) {
                    $this->notfound();
                    exit();
                }
                $this->field_id = $part2[1];
            }
        }
        if($this->field_id) {
            $this->fields = $this->getFieldById($this->fields, $this->field_id);

            $this->empty_then_notfound($this->fields);

            $this->title = $this->fields[0]['Title'];
            $this->is_single_field = true;
        }
    }
    function checkEntryLimit() {
        if($this->form_content['UniqueIP'] || $this->form_content['EntryLimit'] > 0) {
            $ents = $this->entries_model->entryOnce($this->form_id);
            if($this->form_content['UniqueIP']) {
                if(!empty($ents)) {
                    while($ent = $ents->fetch_assoc()) {
                        if(IP == $ent['entry_ip']) {
                            $this->message('Sorry, but this form is limited to one submission per user.');
                        }
                    }
                }
            } elseif(count($ents) > $this->form_content['EntryLimit']) {
                $this->message('Limited submission for this form. Sory!');
            }
        }
    }
    function checkLogin() {
        $this->normal_login = false;
        if($this->form_type == 'normal') {
            if((isset($this->form_content['UserLogin']) && $this->form_content['UserLogin']) ) {
                $this->normal_login = true;
            } else {
                return;
            }
        }
        if(empty($this->user)) {
            $this->redirect();
            exit();
        }
    }
    /**
    *   ***  ***
    */
    function index() {
        if($this->form_type == 'score') {
            $entry_id = $this->entry['entry_id'];
            $user_id = $this->entry['entry_user'];
            if($entryForm = $this->entriesForms_model->getObject(array(
                'where' => "`entry_id` = '$entry_id'"))) {
                $form_nb_id = $entryForm['form_id'];
                if($form_nb = $this->forms_model->getObject(array(
                                        'where' => "`form_id` = '$form_nb_id'"))) {
                    $user_nb = $this->users_model->getObject(array(
                        'where' => "`user_id` = '$user_id'"));
                    $this->view->formNBDisplayResult = $this->common_getFormResult(array(
                                'hideHeadInfo' => false, 
                                'hideHeader' => true,
                                'review' => false,
                                'form' => $form_nb,
                                'entry' => $this->entry,
                                'user' => $user_nb));
                }
            }
        }
        $extra_options = array();
        //
        if($this->form_type == 'autoscore' || $this->normal_login) {
            if($this->ajax_enable) {
                $extra_options['ajaxEnable'] = true;
                $posted_contents = $this->helpers->jd2a($this->userScored['entry_content']);
                $posted_ids = array();
                if($posted_contents) foreach($posted_contents as $pk => $posted_content) {
                    if( strpos($pk, 'Field') === 0 ) {
                        $posted_ids[$pk] = $posted_content;
                    }
                }
                $extra_options['posted_ids'] = $posted_ids;
            }
        }
        /*
        * Fields
        */

        $html = $this->getFields(array(
                'fields' => $this->fields,
                'errorFields' => $this->view->errorFields,
                'extra_options' => $extra_options,
                'disabled' => false));

        /*
        * Theme
        */
        $theme_id = $this->object['theme_id'];
        if($this->theme = $this->themes_model->getObject(array(
            'where' => "`theme_id` = '$theme_id'"))) {
            $this->view->theme_content = $this->helpers->jd2a($this->theme['theme_content']);
        }
        $this->view->html = $html;
        $this->view->render();
    }
    // submit form
    function forms_submit($posts) {
        $captcha_err = $messages = array();
        // Captcha check
        if($this->form_content['UseCaptcha']) {
            if(!$this->helpers->isValidCaptcha($posts["recaptcha_challenge_field"], $posts["recaptcha_response_field"])) {
                $captcha_err['recaptcha_response_field'] = array(
                    'message' => 'The CAPTCHA wasn\'t entered correctly.'
                );
            }
        }
        $fields = $this->fields;
        // posting content
        $posted = array();
        // user posted content
        $posted1 = array();
        // validate fields
        $errorFields = $this->validateFields($fields,$posts);
        // get email confirm fields
        $emailField = $this->getFieldEmailConfirm($fields);
        // group errors
        $errorFields = $errorFields + $captcha_err;

        if(empty($errorFields)) {
            if($this->isUpdate) {
                $posted1 = $this->helpers->jd2a($this->userScored['entry_content']);
                if($this->ajax_enable && !$this->is_single_field) {
                    $posts = array_merge($posts, $posted1);
                } else {
                    $posts = array_merge($posted1, $posts);
                }
            }
            $posted = $this->helpers->jeae($posts);
            $alt1 = $alt2 = $alt3 = $alt4 = $upd1 = '';
            $comp = 1;
            if(!empty($emailField)) {
               $alt1 = ",`entry_complete`";
               $alt2 = ",'0'";
               $comp = 0;   
            }
            $user_id = null;
            if(!empty($this->user)) {
                $user_id = $this->user['user_id'];
            }
            $flag = 1;
            if($this->ajax_enable && !$this->is_single_field) {
                $flag = count($fields) - $this->helpers->get_key_count($posted1, 'Field');
                if($flag <= 0) $flag = 1;
            }
            $entry_uuid = $this->helpers->get_uuid();
            $entd = $this->helpers->get_datetime();
            $ip = IP;
            if($this->form_type == 'nopbai') {
                $alt3 = ",`entry_uuid`,`entry_type`,`entry_user`";
                $alt4 = ",'$entry_uuid','NB','$user_id'";
            } elseif($this->form_type == 'score') {
                $scored = $this->scoring($posts, $fields);
                $scores = $this->helpers->jeae($scored);
                $alt3 = ",`entry_uuid`,`entry_type`,`entry_user`,`entry_of`,`entry_scoring`";
                $alt4 = ",'$entry_uuid','SC','$user_id','{$this->entry_uuid}','$scores'";
            } elseif($this->form_type == 'autoscore') {
                $scored = $this->scoring($posts, $fields);
                $scores = $this->helpers->jeae($scored);
                $alt3 = ",`entry_uuid`,`entry_type`,`entry_user`,`entry_of`,`entry_scoring`,`entry_flag`";
                $alt4 = ",'$entry_uuid','AS','$user_id','$entry_uuid','$scores','$flag'";
                $upd1 = ",`entry_scoring` = '$scores', `entry_flag` = `entry_flag` + $flag";
            } elseif($normal_login) {
                $alt3 = ",`entry_user`,`entry_flag`";
                $alt4 = ",'$user_id','$flag'";
                $upd1 = ",`entry_flag` = `entry_flag` + $flag";
            }
            if($this->isUpdate) {
                $entry_upd_id = $this->isset_get($this->userScored['entry_id']);
                $entry_upd_uuid = $this->isset_get($this->userScored['entry_uuid']);
                if($this->entries_model->updateObject(array(
                    'updates' => "`entry_ip` = '$ip', `entry_update` = '$entd', `entry_content` = '$posted' $upd1",
                    'where' => "`entry_id` = '$entry_upd_id'",
                    'limit' => '1'))) {
                    if($this->form_type == 'autoscore') {
                        $this->redirect($this->url.'review/'.$entry_upd_uuid.'/')
                    }
                    $this->redirect($this->url.'confirm/'.$this->form_uuid.'/');
                    exit();
                }
            } else {
                
                if($this->entries_model->insertObject(array(
                    'fields' => "`entry_ip`,`entry_content` $alt1 $alt3", 
                    'values' => "'$ip','$posted' $alt2 $alt4"))) {
                    $entry_id = $this->entries_model->getInsertId();
                    if($this->entriesForms_model->insertObject(array(
                        'fields' => '`form_id`,`entry_id`', 
                        'values' => "'{$this->form_id}','$entry_id'"))) {
                        $_notify = $this->helpers->jd2a($this->object['form_notify']);
                        /* * notify to superuser
                        if(!empty($_notify['Email'])) {
                            $html = 'Click <a href="'.$siteUrl.'entries/'.$formId.'">here</a> for details';
                            $address[$_notify['Email']] = $_notify['ConfirmationFromAddress'];
                            $replyTo = isset($posts['Field'.$_notify['ReplyTo']])?$posts['Field'.$_notify['ReplyTo']]:'';
                            $options[] = $_notify['ConfirmationFromAddress'];
                            //$_notify['ConfirmationSubject'];
                            $this->helpers->sendMail('New Submission from: '.$_form['Name'],$html, $address, $replyTo, $options);
                        }

                        */
                        // send confirm email
                        if(!empty($emailField)) {
                            if(!empty($posts['Field'.$emailField['ColumnId']])) {
                                $str = $entry_id . '' . md5($entry_id);
                                $html = 'Click <a href="'.$this->url.'confirm/&a='.$str.'">here</a> to confirm your submission.';
                                $address[$posts['Field'.$emailField['ColumnId']]] = $this->form_content['FromAddress'];
                                $replyTo = $this->form_content['ReceiptReplyTo'];
                                $options[] = $this->form_content['FromAddress'];
                                //$_notify['ConfirmationSubject'];
                                $this->helpers->sendMail('Confirm submission from '.$this->form_content['Name'],$html, $address, $replyTo, $options);
                            }
                        }
                        if($this->form_type == 'autoscore') {
                            $this->redirect($this->url.'review/'.$entry_uuid.'/')
                        }
                        $this->redirect($this->url.'confirm/'.$this->form_uuid.'/');
                        exit();
                    }
                }
            }
            if(!empty($this->entries_model->getErrors())) {
                $messages['error'] = 'Some errors occured!';
            }
        } else {
            $messages['error'] = 'Some errors occured!';
        }
        foreach($posts as $k => $post) {
            if(substr($k,0,5) == 'Field') {
                $posted[$k] = $post;
            }
        }
        $this->view->entry = json_encode($posted);
        $this->view->errorFields = $errorFields;
        $this->view->messages = $messages;
        $this->setAllowNext(true);
    }
}