<?php
if ( ! defined( 'IA' ) ) exit;
/*
    @Name: Application Model
    @Description: connect db, mange query, base model
    @Docs:
*/
class Applications_Model extends Applications_Application {
    protected
        $table;
    private 
        $default = array(
                'select' => '*',
                'updates' => '',
                'fields' => '',
                'values' => '',
                'where' => '',
                'orderby' => '',
                'groupby' => '',
                'dir' => '',
                'limit' => ''
            );
    private static
        $db = false;
    public function __construct($registry) {
        parent::__construct($registry);
        if(!self::$db) {
            self::$db = $this->connect();
        }
    }
    private function __clone() {}
    public function connect() {
        $db = $this->registry->configs['db'];
        if($db['db_host'] == '{DB_HOST}') {
            if(file_exists("{$this->site_path}/install/index.php")) {
                header("Location: {$this->site_url}/install/index.php?step=1");
            }
            exit('DB ERROR!');
        }
        $mysqli = new mysqli($db['db_host'], $db['db_user'], $db['db_pass'], $db['db_name']);
        if($mysqli->connect_error) {
            die('Database Connection Error!');
        }
        $mysqli->query("SET NAMES 'utf8'");
        $mysqli->query("SET time_zone = 'Etc/GMT-7'");
        date_default_timezone_set('Etc/GMT-7');
        $this->registry->mysqli = $mysqli;
        return true;
    }
    public function setTable($table) {
        $this->table = $table;
    }
    public function getTable($table) {
        return $this->table;
    }
    public function getMysqli() {
        return $this->mysqli;
    }
    public function getErrors() {
        return $this->registry->mysqli->error;
    }
    public function getInsertId() {
        return $this->registry->mysqli->insert_id;
    }
    public function getRows($args) {
        $args['select'] = 'SQL_CACL_FOUND_ROWS *';
        $args['limit'] = '0,1';
        $args = $this->argsMerge($args);
        $this->getEntities($args);
        return $this->getRowsCount();
    }
    public function insertEntity($args) {
        $args = $this->argsMerge($args);
        return $this->insert($this->table,$args['fields'],$args['values']);
    }
    public function deleteEntity($args) {
        $args = $this->argsMerge($args);
        return $this->delete($this->table,$args['where'],$args['limit']);
    }
    public function updateEntity($args) {
        $args = $this->argsMerge($args);
        return $this->update($this->table,$args['updates'],$args['where']);
    }
    public function getEntity($args) {
        $args = $this->argsMerge($args);
        return $this->select($args['select'],$this->table,$args['where']);
    }
    public function getEntities($args) {
        $args = $this->argsMerge($args);
        return $this->selectAll($args['select'],$this->table,$args['where'],$args['groupby'],$args['orderby'],$args['dir'],$args['limit']);
    }
    public function getRowsCount() {
        $result = $this->fetch("SELECT FOUND_ROWS() as num_rows");
        return $result['num_rows'];
    }
    
    private function insert($table, $fields, $values) {
        $table = $this->db_prefix.$table;
        return $this->query("INSERT INTO $table
            ($fields) VALUES ($values)
        ");
    }
    private function update($table, $updates, $where, $limit = '') {
        $table = $this->db_prefix.$table;
        if(!empty($where)) {
            $where = 'WHERE '.$where;
        }
        if(!empty($limit)) {
            $limit = 'LIMIT '.$limit;
        }
        return $this->query("UPDATE $table
            SET $updates $where $limit
        ");
    }
    private function delete($table, $where, $limit = '1') {
        $table = $this->db_prefix.$table;
        if(!empty($where)) {
            $where = 'WHERE '.$where;
        }
        if(!empty($limit)) {
            $limit = 'LIMIT '.$limit;
        }
        return $this->query("DELETE FROM $table
            $where $limit
        ");
    }
    private function select($select = '*', $table, $where = '') {
        $table = $this->db_prefix.$table;
        if(!empty($where)) {
            $where = 'WHERE '.$where;
        }
        return $this->fetch("SELECT $select FROM $table $where LIMIT 1");
    }
    private function selectAll($select = '*', $table, $where = '', $groupby = '', $orderby = '', $dir = '', $limit = '') {
        $table = $this->db_prefix.$table;
        if(!empty($where)) {
            $where = 'WHERE '.$where;
        }
        if(!empty($groupby)) {
            $groupby = 'GROUP BY '.$groupby;
        }
        if(!empty($orderby)) {
            $orderby = 'ORDER BY '.$orderby;
        }
        if(!empty($limit)) {
            $limit = 'LIMIT '.$limit;
        }
        return $this->fetchAll("SELECT $select FROM $table $where $groupby $orderby $dir $limit");
    }
    /*
        @Name:
        @Description:
        @Docs:
            {A} -> alias, ex: tableA a -> !@ = a
            {P} -> table prefix
            {T_J} -> table and join, tableA a1 join_type tableB a2
            array(
                    'query' => "SELECT {A1}id, {A2}id FROM {T_J} ON {A1}id = {A2}id WHERE {A1}name = '123' AND 
                                {A1}email = 'abc' AND {A2}name = '456'",
                    'tables' => array(
                            '{P}tableA',
                            '{P}tableB',
                        ),
                    'join_type' => 'INNER JOIN',
                )
    */
    public function selectJoin($args) {
        $helpers = $this->registry->helpers;
        $db_prefix = $this->db_prefix;
        if(!empty($args['tables'])) {
            foreach($args['tables'] as $key => $value) {
                $k = $key + 1;
                $value = str_replace('{P}',$db_prefix,$value);
                $args['tables'][$key] = $value . ' a'.$k;
                $args['query'] = str_replace('{A'.$k.'}','a'.$k.'.',$args['query']);
            }
        }
        $tj = implode(' '.$args['join_type'].' ', $args['tables']);
        $qs = str_replace('{T_J}', $tj, $args['query']);
        return $this->fetchAll($qs);
    }
    /*
        @Name:
        @Description:
        @Docs:
            SELECT * FROM `{P}users` a, (SELECT count(*) FROM `subject_user` b WHERE b.`user_id` = '21') c 
                                                                                WHERE a.`user_id` = '21'

    */
    public function queryRP($qs) {
        $helpers = $this->registry->helpers;
        $db_prefix = $this->db_prefix;
        $qs = str_replace('{P}', $db_prefix, $qs);
        return $this->fetchAll($qs);
    }
    private function fetch($qs) {
        $q = $this->query($qs);
        if(!$q || $q->num_rows == 0) return array();
        return $q->fetch_assoc();
    }
    private function fetchAll($qs) {
        $res = $this->query($qs);
        if(!$res || $res->num_rows == 0) return array();
        return $res;
    }
    private function query($qs) {
        $this->registry->query_count++;
        return $this->registry->mysqli->query($qs);
    }
    private function argsMerge($args) {
        return array_merge($this->default, $args);
    }
    /*
        Shortcut functions
    */
    function getObjectsRP($qs) {
        return array(
            $this->queryRP($qs),
            $this->getRowsCount(),
        );
    }
    function getObjects($args) {
        return array(
            $this->getEntities($args),
            $this->getRowsCount(),
        );
    }
    function getObject($args) {
        return $this->getEntity($args);
    }
    function insertObject($args) {
        return $this->insertEntity($args);
    }
    function updateObject($args) {
        return $this->updateEntity($args);
    }
    function deleteObject($args) {
        return $this->deletetEntity($args);
    }
}