<?php
if ( ! defined( 'IA' ) ) exit;
/*
    @Name: Application Controller
    @Description:
    @Docs:
*/
    /*

    Continue: Entries controller, line 488

    */
abstract class Applications_Controller extends Applications_ControllerBase {
    protected
        $user,
        $configs,
        $configs_model,
        $users_model,
        $groups_model,
        $categories_model,
        $comments_model,
        $entries_model,
        $forms_model,
        $logs_model,
        $reports_model,
        $subjects_model,
        $theme_model,
        $url_model,
        $caches_model,
        $subjectForm_model,
        $routers,
        /*
            @Name: User Permissions
            @Description:
            @Docs: 
                All
                Admin
                Customer
                
        */
        $permission = 'All',
        $object,
        $objects,
        $total,
        $isUpdate = false
        ;
    function __construct($registry) {
        parent::__construct($registry);
    }
    public function initiallize() {
        $this->routers = $this->getRouters();
        // models
        $this->configs_model = new Applications_Models_Configs($this->registry);
        $this->users_model = new Applications_Models_Users($this->registry);
        $this->caches_model = new Applications_Models_Caches($this->registry);
        $this->permission();
    }
    public function setConfigs($configs = array()) {
        $default = array(
                "'template_name'",
                "'site_language'",
                "'site_configs'",
            );
        $config_str = implode(',', array_merge($default,$configs));
        list($configs, $total) = $this->configs_model->getConfigs(array(
            'where' => "`name` IN($config_str)"));
        $options = array();
        if(!empty($configs)) while($config = $configs->fetch_assoc()) {
            $options[$config['name']] = $this->helpers->jd2a($config['value']);
        }
        $default = array(
                'template_name' => 'classic',
                'site_language' => 'en',
                'display' => array(
                        'show_h2d' => true,
                        'show_skype' => true
                    ),
            );
        $options = array_merge($default, $options);
        $this->site->setCaches($options);
        $this->configs = $options;
    }
    public function getData() {
        return $this->object;
    }
    protected function permission() {
        $this->detectUser();
        if($this->permission == 'All') return;
        elseif($this->permission == 'Admin') {
            if(empty($this->user)) {
                $this->redirect();
                return;
            }
            $user_type = $this->user['user_type'];
            if($user_type!='A') {
                $this->permissionDenied();
                return;
            }
        } elseif($this->permission == 'Customer') {
            if(empty($this->user)) {
                $this->redirect();
                return;
            }
        }
    }
    protected function detectUser() {
        $user_id = isset($_SESSION['user'])?$_SESSION['user']:'';
        if(!empty($user_id)) {
            if(!isset(self::$caches['user'][$user_id])) {
                self::$caches['user'][$user_id] = $this->users_model->getObject(array('where' => "`user_id` = '$user_id'"));
            }
            $this->user = self::$caches['user'][$user_id];
        }
    }
    protected function permissionDenied() {
        exit('permission denied!');
    }
    protected function redirect($redirect = '') {
        if(empty($redirect)) $redirect = $this->url . 'auth/login/?ref='.$this->helpers->get_currentPageUrl();
        header("Location: $redirect");
        exit;
    }
    protected function getCached($cache_name, $index) {
        if(!isset(self::$caches[$cache_name][$index])) {
            if($cache_name == 'users') {
                $user = $this->users_model->getUser(array('where' => "`user_id` = '$index'"));
                self::$caches[$cache_name][$index] = $user;
            }
        }
        return self::$caches[$cache_name][$index];
    }
    protected function notfound() {
        $this->controller = 'Notfound';
        $this->action = 'index';
        $this->view->render();
        exit();
    }
    protected function empty_then_notfound($object) {
        if(empty($object)) {
            $this->notfound();
            exit();
        }
    }
    protected function message($message) {
        $this->setController('message');
        $this->setAction('index');
        $this->view->messages[] = $message;
        $this->view->render('single');
        exit();
    }
    protected function allowedFor($user_type) {
        if($user_type == 'Admin') {
            if(empty($this->user)) {
                $this->permissionDenied();
                return;
            }
            if($this->user['user_type'] != 'A') {
                $this->permissionDenied();
                return;
            }
        } elseif($user_type == 'Customer') {
            if(empty($this->user)) {
                $this->permissionDenied();
                return;
            }
        }
    }
    protected function getUrlParam($param) {
        return $this->getParam($_GET, $param, NULL);
    }
    protected function getRouterParam($param) {
        return $this->getParam($this->routers, $param, NULL);
    }
    protected function getRouterParams($params) {
        return $this->getParams($params, $this->routers);
    }
    protected function getUrlParams($params) {
        return $this->getParams($params, $_GET);
    }
    /**
    *   *** AJAX FUNCTIONS ***
    */
    protected function ajax_groupPopup($req) {
        $html = $this->common_getGroupsAsOption();
        if(!empty($html)) {
            $this->json['success'] = true;
            $this->json['response']['html'] = $html;
        }
    }
    protected function ajax_loadComments($req) {
        $log_id = $this->helpers->str_escape(isset($req['log_id'])?$req['log_id']:'');
        $comment_ids = $this->helpers->str_escape(isset($req['comment_ids'])?$req['comment_ids']:array());
        $c_ids = array();
        
        foreach($comment_ids as $comment_id) {
            $c_ids[] = '\'' . $comment_id . '\'';
        }
        $c_ids = implode(',',$c_ids);
        if(empty($c_ids)) {
            $c_ids = "'0'";
        }
        list($comments, $comments_count) = $this->comments_model->getCommentsOfLog(array(
                'log_id' => $log_id,
                'commented_id' => $c_ids,
                'limit' => '3'
            ));
        $loadCommentNumber = 3;
        $html = '';
        $json = array();
        ob_start();
        if(!empty($comments)) while($comment = $comments->fetch_assoc()) {
            $user = $this->getCached('users', $comment['comment_user']);
            $json[] = array(
                'comment_id' => $comment['comment_id'],
                'comment_text' => $comment['comment_content'],
                'comment_user' => $user['h2d']
            );
            $args = array(
                    'comment' => $comment,
                    'user' => $user,
                );
            $this->common_print_listComment($args);
        }
        $html = ob_get_clean();
        $this->json['response']['html'] = $html;
        $this->json['response']['json'] = $json;
        $this->json['response']['over'] = $comments_count - $comments->num_rows;
        if($comments->num_rows < $loadCommentNumber) $this->json['response']['over'] = false;
        $this->json['success'] = true;
    }
    protected function ajax_addComment($req) {
        $log_id = $this->helpers->str_escape(isset($req['log_id'])?$req['log_id']:'');
        $commentText = htmlspecialchars(isset($req['commentText'])?$req['commentText']:'');
        $commentText2 = $this->helpers->str_escape($commentText);
        $user_id = $this->user['user_id'];
        $comment_title = '';
        if($this->comments_model->insertComment(array(
            'fields' => '`comment_title`, `comment_content`, `comment_user`', 
            'where' => "'$comment_title', '$commentText2', '$user_id'"))) {
            $comment_id = $this->comments_model->getInsertId();
            $this->logsComments_model->insertFLC(array(
                'fields' => '`log_id`, `comment_id`', 
                'where' => "'$log_id', '$comment_id'"));
            $json[] = array(
                'comment_id' => $comment_id,
                'comment_text' => $commentText,
                'comment_user' => $this->user['h2d']
            );
            ob_start();
            $args = array(
                    'comment' => $comment,
                    'user' => $this->user,
                );
            $this->common_print_listComment($args);
            $this->json['response']['html'] = ob_get_clean();
            $this->json['response']['json'] = $json;
            $this->json['success'] = true;
        }
    }
    protected function ajax_deleteComment($req) {
        $this->allowedFor('Admin');
        $log_id = $this->helpers->str_escape(isset($req['log_id'])?$req['log_id']:'');
        $comment_id = $this->helpers->str_escape(isset($req['comment_id'])?$req['comment_id']:'');
        $args = array(
            'where' => "`comment_id` = '$comment_id'");
        $comment = $this->comments_model->getComment($args);
        if(!empty($comment)) {
            $this->comments_model->deleteComment($args);
            $this->logsComments_model->deleteFLC($args);
            $this->json['success'] = true;
        }
    }
    /**
    *   *** POST REQUEST HANDLE ***
    */
    function postReq() {
        $req = $this->helpers->str_unescape($_POST);
        $routers = $this->getRouters();
        $action = isset($req['action'])?$req['action']:'submit';
        $method = $routers['controller'].'_'.$action;
        if(is_callable(array($this, $method))) {
            $this->$method($req);
        } else {
            $noaction = 'ajax_'.$action;
            if(is_callable(array($this, $noaction))) {
                $this->$noaction($req);
            }
        }
        if(!$this->getAllowNext()) echo $this->getJson();
    }
    function index() {}
}