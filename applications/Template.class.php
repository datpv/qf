<?php
if ( ! defined( 'IA' ) ) exit;
/*
    @Name: Application Template
    @Description: Render views, collect of html modules are here
    @Docs:
*/
class Applications_Template extends Applications_Application {
    private 
        $vars = array(),
        $static_url;
    function __construct($registry) {
        parent::__construct($registry);
    }
    function __set($index, $val) {
        $this->vars[$index] = $val;
    }
    function __get($index) {
        return isset( $this->vars[$index] ) ? $this->vars[$index] : null;
    }
    function render($displayType = 'full') {
        $routers = $this->getRouters();
        $file_path = strtolower($routers['controller']) . DIRECTORY_SEPARATOR . $routers['action'];
        $template_name = $this->site->getCache('template_name');
        $template_path = $this->template_path . DIRECTORY_SEPARATOR . $template_name;
        $file = $template_path . DIRECTORY_SEPARATOR . $file_path . '.tpl.php';
        if (!file_exists($file)) {
            $template_name = 'base';
        }
        if($routers['controller'] == 'admin') {
            $template_name = 'admin';
        }
        $template_path = $this->template_path . DIRECTORY_SEPARATOR . $template_name;
        $file = $template_path . DIRECTORY_SEPARATOR . $file_path . '.tpl.php';
        $this->static_url = $this->site_url . '/static/' . $template_name;
        if (!file_exists($file)) {
            $file = $this->template_path . DIRECTORY_SEPARATOR .'notfound.tpl.php';
        }
        
        foreach ($this->vars as $k => $v) {
            $$k = $v;
        }
        if ($displayType == 'single') {
            include ($file);
            return;
        } elseif($displayType == 'message') {
            include ($template_path . DIRECTORY_SEPARATOR . 'message.tpl.php');
            return;
        }
        $header = $template_path . DIRECTORY_SEPARATOR . 'header.tpl.php';
        if (!file_exists($header)) {
            $header = $this->template_path . DIRECTORY_SEPARATOR . 'header.tpl.php';
        }
        $footer = $template_path . DIRECTORY_SEPARATOR . 'footer.tpl.php';
        if (!file_exists($footer)) {
            $footer = $this->template_path . DIRECTORY_SEPARATOR . 'footer.tpl.php';
        }
        include ($header);
        include ($file);
        include ($footer);
    }
    function getPaginate($args) {
        $default = array(
                'base_url' => $this->helpers->get_currentPageUrl(),
                'paged' => 1,
                'paginate_limit' => 15,
                'paged_name' => 'paged',
                'delimiter' => '&'
            );
        $args = array_merge($default, $args);
        $args['total_pages'] = ceil($args['total'] / $args['page_limit']);
        $args['pages'] = $this->pagiArray($args);
        return $this->pagiHtml($args);
    }
    function pagiHtml($args) {
        ob_start();
        ?>
        <div class="pagination">
            <ul id="pagination-flickr">
                <?php
                    if($args['paged'] == 1) {
                    ?>
                        <li class="previous-off">« Previous</li>
                    <?php
                    } else {
                    ?>
                        <li class="previous"><a href="<?php echo $args['base_url'].$args['delimiter'].$args['paged_name'].'='.($args['paged']-1); ?>">« Previous</a></li>
                    <?php
                    }
                    ?>
                <?php
                    foreach ($args['pages'] as $page) {
                        // If page has a link
                        if (isset ($page['url'])) { ?>
                            <li>
                                <a href="<?php echo $page['url']?>">
                                    <?php echo $page['text'] ?>
                                </a>
                            </li>
                        <?php 
                        } else {
                        ?>
                            <li class="active"><?php echo $page['text'] ?></li>
                        <?php
                        }
                    }
                ?>
                <?php
                    if($args['paged'] == $args['total_pages'] || $args['total_pages'] == 0) {
                    ?>
                        <li class="next-off">Next »</li>
                    <?php
                    } else {
                    ?>
                        <li class="next"><a href="<?php echo $args['base_url'].$args['delimiter'].$args['paged_name'].'='.($args['paged']+1); ?>">Next »</a></li>
                    <?php
                    }
                ?>
            </ul>
        </div>
        <?php
        return ob_get_clean();
    }
    function pagiArray($args) {
        // Array to store page link list
        $page_array = array ();
        // Show dots flag - where to show dots?
        $dotshow = true;
        // walk through the list of pages
        for ( $i = 1; $i <= $args['total_pages']; $i ++ ) {
            // If first or last page or the page number falls 
            // within the pagination limit
            // generate the links for these pages
            if ($i == 1 || $i == $args['total_pages'] || ($i >= $args['paged'] - $args['paginate_limit'] &&  $i <= $args['paged'] + $args['paginate_limit']) ) {
                // reset the show dots flag
                $dotshow = true;
                // If it's the current page, leave out the link
                // otherwise set a URL field also
                if ($i != $args['paged'])
                $page_array[$i]['url'] = $args['base_url'] . $args['delimiter'] . $args['paged_name'] . "=" . $i;
                $page_array[$i]['text'] = strval ($i);
            }
            // If ellipses dots are to be displayed
            // (page navigation skipped)
            else if ($dotshow == true) {
                // set it to false, so that more than one 
                // set of ellipses is not displayed
                $dotshow = false;
                $page_array[$i]['text'] = "...";
            }
        }
        return $page_array;
    }
    function getDropdown($args) {
        ob_start();
        ?>
        <div<?php echo $args['wrapper_attr']; ?>>
            <select<?php echo $args['select_attr']; ?>>
        <?php
        if(isset($args['default_option'])) {
        ?>
            <option class="<?php if($args['selected_option'] == '0') echo 'selected'; ?>" value="<?php echo $args['default_option']['key']; ?>"><?php echo $args['default_option']['value']; ?></option>
        <?php
        }
        foreach ($args['options'] as $key => $value) {
        ?>
            <option class="<?php if($args['selected_option'] == $key) echo 'selected'; ?>" value="<?php echo $key; ?>"><?php echo $value; ?></option>
        <?php
        }
        ?>
            </select>
        </div>
        <?php
        $html = ob_get_clean();
        return $html;
    }
    function getOptions($option_results, $selected_id, $key_id) {
        $html = '';
        if(is_array($option_results)) {
            foreach($option_results as $option) {
                $selected = ($selected_id == $option[$key_id])?' selected="selected"':'';
                $html .= '<option value="'.$option[$key_id].'"'.$selected.'>';
                    $html .= $option['theme_name'];
                $html .= '</option>';
            }
        } elseif(is_object($option_results)) {
            while($option = $option_results->fetch_assoc()) {
                $selected = ($selected_id == $option[$key_id])?' selected="selected"':'';
                $html .= '<option value="'.$option[$key_id].'"'.$selected.'>';
                    $html .= $option['theme_name'];
                $html .= '</option>';
            }
        }
        return $html;
    }
}
