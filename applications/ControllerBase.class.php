<?php
if ( ! defined( 'IA' ) ) exit;
/*
    @Name: Application Base Controller
    @Description:
    @Docs:
*/
abstract class Applications_ControllerBase extends Applications_Application {
    protected static
        $caches = array();
    public
        $allowNext = false,
        $json = array(
                'success' => false,
                'response' => array(
                        'html' => '',
                        'message' => ''
                    )
            )
        ;
    function __construct($registry) {
        parent::__construct($registry);
    }
    abstract function index();
    abstract function initiallize();
    abstract function setConfigs();
    function setCache($index, $values) {
        $this->caches[$index] = $values;
    }
    function getCache($index) {
        return $this->caches[$index];
    }
    function setAllowNext($allowNext) {
        $this->allowNext = $allowNext;
    }
    function getAllowNext() {
        return $this->allowNext;
    }
    function setRouters($routers) {
        return $this->registry->router->setRouters($routers);
    }
    function setAction($action) {
        $this->registry->router->setAction($action);
    }
    function setController($controller) {
        $this->registry->router->setController($controller);
    }
}