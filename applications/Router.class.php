<?php
if ( ! defined( 'IA' ) ) exit;
/*
    @Name: Application Router
    @Description: Route website, all set of configs url are here.
    @Docs:
*/
class Applications_Router extends Applications_Application {
    private
        $paged,
        $path,
        $file,
        $controler,
        $action,
        $routers = array(),
        /*
            @Name: controller replace
            @Description: replace a set of controller to another
            @Docs:
                $replace = array(
                    'str_replace_to' => array(
                        'str_to_replace'
                    )
                )
        */
        $cr = array(
            'builder' => array(
                    'build'
                ),
            'forms' => array(
                    'fm',
                    'formsmng'
                ),
            'form' => array(
                )
        ),
        /*
            @Name: action replace
            @Description: replace an action to another
            @Docs:
                $replace = array(
                    'str_replace_to' => array(
                        'str_to_replace'
                    )
                )
        */
        $ar = array(
            
        ),
        /*
            @Name: action prebuild
            @Description: a set of actions can be use by a controller
            @Docs:
                $a_set_action = array(
                    'controller' => array(
                        'action'
                    )
                )
        */
        $ap = array(
            'files' => array(
                    'action1',
                    'action2'
                ),
            'auth' => array(
                    'login',
                    'logout',
                    'signup'
                ),
            'entries' => array(
                    'submit'
                )
        ),
        /*
            @Name: controller unroute
            @Description: Do nothing with these controllers
            @Docs:
                $a_set_unroute = array(
                    'str_to_unroute'
                )
        */
        $ur = array(
            'fields'
        ),
        /*
            @Name: controller - action with no db connect
            @Description: 
                Call these controller only, do nothing with db
                Empty for all action
            @Docs:
                $a_set_noconfig = array(
                    'controller' => array(
                        'actions'
                    )
                )
        */
        $nld = array(
            'files' => array(
                    'action1'
                )
        ),
        /*
            @Name: controller - action with no page methods
            @Description: 
                Call these controller only, do nothing with page methods
                Empty for all action
            @Docs:
                $a_set_noconfig = array(
                    'controller' => array(
                        'actions'
                    )
                )
        */
        $nlpm = array(
            'files' => array()
        );
    function __construct($registry) {
        parent::__construct($registry);
    }
    function setController($controller) {
        $this->controller = $controller;
    }
    function setAction($action) {
        $this->action = $action;
    }
    function setRouters($routers) {
        if(empty($routers)) return;
        $this->routers = $routers;
    }
    function getRouters() {
        return $this->routers;
    }
    /*
        @Name: get url arguments
        @Description: apply url configs, get all url parameters
        @Docs:
    */
    function configRouters() {
        // Router configs
        $rcs = array(
                'do_nothing' => false,
                'no_load_db' => false,
                'no_load_page_methods' => false,
                'is_seo_url' => false,
                'url_name' => ''
            );
        $req = isset($_GET['req'])?$_GET['req']:'';
        $paged = isset($_GET['paged'])?$_GET['paged']:'1';
        if(empty($paged) || !is_numeric($paged) || $paged <= 0) $paged = 1;
        $this->paged = $paged;

        $url_path = explode('/', $req);
        $routers = array();
        $count_path = count($url_path);
        if(!isset($url_path[1])) {
            if(!empty($url_path[0])) {
                if( count( explode( '-', $url_path[0] ) ) > 1 ) {
                    $rcs['is_seo_url'] = true;
                    $rcs['url_name'] = $url_path[0];
                } 
            }
        }
        for($i = 0; $i < $count_path; $i++) {
            if(!empty($url_path[$i])) $routers['pos_'.$i] = $url_path[$i];
        }
        if(!isset($routers['pos_0'])) $routers['pos_0'] = 'index';
        if(!isset($routers['pos_1'])) $routers['pos_1'] = 'index';
        $controller = $routers['pos_0'];
        $this->action = 'index';
        $action = $routers['pos_1'];
        // replace controller
        foreach($this->cr as $ck => $cv) {
            if(in_array($controller, $cv)) {
                $controller = $ck;
                break;
            }
        }
        // replace action
        foreach($this->ar as $ak => $av) {
            if(in_array($action, $av)) {
                $action = $ak;
                break;
            }
        }
        // ignor these controllers
        if(in_array($controller, $this->ur)) {
            $rcs['do_nothing'] = true;
            return $rcs;
        }
        // no load db these controllers
        if(array_key_exists($controller, $this->nld)) {
            if(empty($this->nld[$controller])) $rcs['no_load_db'] = true;
            elseif(in_array($action, $this->nld[$controller])) {
                $rcs['no_load_db'] = true;
            }
        }
        // no load page methods these controllers
        if(array_key_exists($controller, $this->nlpm)) {
            if(empty($this->nlpm[$controller])) $rcs['no_load_page_methods'] = true;
            elseif(in_array($action, $this->nlpm[$controller])) {
                $rcs['no_load_page_methods'] = true;
            }
        }
        // set action prebuild
        if(array_key_exists($controller, $this->ap)) {
            if(in_array($action, $this->ap[$controller])) {
                $am = explode('__', $action);
                $this->action = $am[0];
                if(isset($am[1])) {
                    $routers['pos_special'] = $am[1];
                }
            }
        }
        $this->controller = ucfirst(strtolower($controller));
        $this->path = APPLICATION_PATH . '/controllers';
        $this->file = $this->path.'/'.$this->controller.'.class.php';
        $routers['controller'] = $this->controller;
        $routers['action'] = $this->action;
        $routers['paged'] = $this->paged;
        $this->routers = $routers;
        return $rcs;
    }
    function loader() {
        if(!is_readable($this->file)) {
            $this->controller = 'Notfound';
            $this->action = 'index';
            $this->file = $this->path.'/'.$this->controller.'.class.php';
        }
        $class = 'Applications_Controllers_' . $this->controller;
        $control = new $class($this->registry);
        $action = $this->action;
        if(!is_callable(array($control, $action))) {
            $this->action = $action = 'index';
        }
        $control->initiallize();
        $control->setConfigs();
        $control->getData();
        $next = true;
        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            $next = false;
            $control->isPost = true;
            $control->postReq();
            $next = $control->getAllowNext();
        }
        if(!$next) exit;
        $control->$action();
    }
}