<?php
if(!defined('IA')) exit;
/*
    @Name: Application Parent
    @Description:
    @Docs:
*/
class Applications_Application {
    protected
        $registry,
        $site,
        $mysqli,
        $views,
        $helpers,
        $url,
        $isPost = false,
        $errors = array(),
        $site_url = SITE_URL,
        $base_url = SITE_URL,
        $site_path = SITE_PATH,
        $lib_path = LIB_PATH,
        $include_path = INCLUDE_PATH,
        $application_path = APPLICATION_PATH,
        $template_path = TEMPLATE_PATH,
        $db_prefix = DB_PREFIX;
    function __construct($registry) {
        $this->registry = $registry;
        $this->url = $this->site_url . '/'; // $this->url = $this->site_url . '/index.php?req=';
        $this->site = $registry->site;
        $this->helpers = $registry->helpers;
        $this->mysqli = $registry->mysqli;
        $this->view = $registry->template;
    }
    public function getRouters() {
        return $this->registry->router->getRouters();
    }
    public function getParam($dict, $key, $default = NULL) {
        return isset($dict[$key])?$this->helpers->str_escape($dict[$key]):$default;
    }
    public function getParams($array, $dict) {
        $values = array();
        foreach ($array as $key => $default) {
            $values[] = $this->getParam($dict, $key, $default);
        }
        return $values;
    }
    public function isset_get($value, $default = false) {
        return isset($value) ? $value : $default;
    }
    public function not_empty_get($value, $default = false) {
        return !empty($value) ? $value : $default;
    }
}