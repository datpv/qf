<?php
function fn_put_contents($path, $content, $file_perm = DEFAULT_FILE_PERMISSIONS) {
    $result = file_put_contents($path, $content);
    if ($result !== false) {
        chmod($path, $file_perm);
    }
    return $result; 
}
function getErrorMsg($error_code) {
    $error_msg = 'Some Error Occured!';
    switch ($error_code) {
        case 'mysqli_error':
            $error_msg = 'Failed to connect to MySQL!';
            break;
        case 'database_create':
            $error_msg = 'Create database table error!';
            break;
        case 'database_select':
            $error_msg = 'Select database table error!';
            break;
        case 'file_upload':
            $error_msg = 'File upload error!';
            break;
        case 'file_load':
            $error_msg = 'Could not load SQL file!';
            break;
        case 'write_config':
            $error_msg = 'Write file: config.install.php got error!';
            break;
        case 'write_main_config':
            $error_msg = 'Write file: config.php got error!';
            break;
        default:
            break;
    }
    return $error_msg;
}
function o2a($d) {
    if (is_object($d)) {
        $d = get_object_vars($d);
    }
    if (is_array($d)) {
        return array_map('o2a', $d);
    }
    else {
        return $d;
    }
}
function jeae($arr) {
    global $mysqli;
    return $mysqli->real_escape_string(json_encode($arr));
}
function jd2a($str) {
    $jda = o2a(json_decode($str));
    if(!empty($str) && empty($jda)) {
        $jda = o2a(json_decode(jd_str_handle($str)));
    }
    return $jda;
}
function jd_str_handle($str) {
    $str = str_replace('{\"','{"',$str);
    $str = str_replace('\"}','"}',$str);
    $str = str_replace('\":','":',$str);
    $str = str_replace(':\"',':"',$str);
    $str = str_replace('\",','",',$str);
    $str = str_replace(',\"',',"',$str);
    $str = str_replace('\\\"','"',$str);
    $str = str_replace('\\\n','\n',$str);
    $str = preg_replace('#(\\\){2,}#','',$str);
    $str = str_replace("\'","'",$str);
    return $str;
}