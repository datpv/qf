<?php 
if ( ! defined( 'IAC' ) ) exit; 
// unset cookies
if (isset($_SERVER['HTTP_COOKIE'])) {
    $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
    if(!empty($cookies))
    foreach($cookies as $cookie) {
        $parts = explode('=', $cookie);
        $name = trim($parts[0]);
        setcookie($name, '', time()-1000);
        setcookie($name, '', time()-1000, '/');
    }
}
?><!DOCTYPE html>
<html>
<head>
<title>Install Step 4 - finish</title>
<meta charset="utf-8">
<link rel="stylesheet" href="<?php echo $site_url; ?>/css/style.css" />
</head>
    <body>
        <form class="form" name="installform" action="<?php echo $site_url; ?>/../" method="get">
            <div class="sep">
                <p>Install done. <a href="<?php echo $site_url; ?>/../">Visit Homepage</a></p>
                <p>Or. <a href="<?php echo $site_url; ?>/../admin/">Go to admin</a></p>
            </div>
        </form>
    </body>
</html>
