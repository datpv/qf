<?php 
if ( ! defined( 'IAC' ) ) exit; 
if($_SERVER['REQUEST_METHOD'] == 'POST') {
    $db_host = !empty($_POST['db_host'])?$_POST['db_host']:'localhost';
    $db_user = !empty($_POST['db_user'])?$_POST['db_user']:'root';
    $db_pass = !empty($_POST['db_pass'])?$_POST['db_pass']:'';
    $db_name = !empty($_POST['db_name'])?$_POST['db_name']:'';
    $db_prefix = !empty($_POST['db_prefix'])?$_POST['db_prefix']:'';
    $old_db_prefix = !empty($_POST['old_db_prefix'])?$_POST['old_db_prefix']:'';

    $db_admin_user = !empty($_POST['db_admin_user'])?$_POST['db_admin_user']:'';
    $db_admin_pass = !empty($_POST['db_admin_pass'])?$_POST['db_admin_pass']:'';
    $db_admin_number = !empty($_POST['db_admin_number'])?$_POST['db_admin_number']:'';
    $db_admin_email = !empty($_POST['db_admin_email'])?$_POST['db_admin_email']:'';

    $smtp = array();
    $smtp['host'] = !empty($_POST['smtp_host'])?$_POST['smtp_host']:'';
    $smtp['port'] = !empty($_POST['smtp_port'])?$_POST['smtp_port']:'';
    $smtp['user'] = !empty($_POST['smtp_user'])?$_POST['smtp_user']:'';
    $smtp['pass'] = !empty($_POST['smtp_pass'])?$_POST['smtp_pass']:'';

    $db_file_name = !empty($_POST['db_file_name'])?$_POST['db_file_name']:'';

    $mysqli = new mysqli($db_host, $db_user, $db_pass);
    if ($mysqli->connect_errno) {
        header('Location: index.php?step=1&error=mysqli');
        exit();
    }
    if(!$mysqli->query("CREATE DATABASE IF NOT EXISTS $db_name")) {
        echo '<br>DATABASE: '.$mysqli->error.'<br>';
        header('Location: index.php?step=1&error=database_create');
        exit();
    }
    $mysqli->select_db($db_name) or function() {
        header('Location: index.php?step=1&error=database_select');
        exit();
    };
    $mysqli->query("SET NAMES 'utf8'");
    $mysqli->query("SET CHARACTER SET utf8");

    /*
    * Install By File
    */
    if(!empty($db_file_name)) {
        $file = $site_path .DIRECTORY_SEPARATOR.'database'.DIRECTORY_SEPARATOR.$db_file_name;
    } else {
        if ($_FILES['db_file']['error'] == UPLOAD_ERR_OK               //checks for errors
          && is_uploaded_file($_FILES['db_file']['tmp_name'])) { //checks that file is uploaded
            
        } else {
            header('Location: index.php?step=1&error=file_upload');
            exit();
        }

        $file = $_FILES['db_file']['tmp_name'];
    }
    if (!file_exists($file)) { 
        header('Location: index.php?step=1&error=file_load');
        exit();
    }

    $lines = file($file);

    if ($lines) {
        $sql = '';
        foreach($lines as $line) {
            if ($line && (substr($line, 0, 2) != '--') && (substr($line, 0, 1) != '#')) {
                $sql .= $line;

                if (preg_match('/;\s*$/', $line)) {
                    $sql = str_replace("DROP TABLE IF EXISTS `" . $old_db_prefix, "DROP TABLE IF EXISTS `" . $db_prefix, $sql);
                    $sql = str_replace("CREATE TABLE IF NOT EXISTS `" . $old_db_prefix, "CREATE TABLE `" . $db_prefix, $sql);
                    $sql = str_replace("INSERT INTO `" . $old_db_prefix, "INSERT INTO `" . $db_prefix, $sql);
                    $mysqli->query($sql);
                    if(!empty($mysqli->error)) {
                        echo $mysqli->error;
                    }
                    $sql = '';
                }
            }
        }
    }
    $mysqli->query("CREATE TABLE IF NOT EXISTS `{$db_prefix}configs` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `name` varchar(255) NOT NULL,
        `value` text NOT NULL,
        PRIMARY KEY (`id`)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3");

    $mysqli->query("INSERT INTO `{$db_prefix}configs` (`id`, `name`, `value`) VALUES
        (1, 'template_name', 'classic'),
        (2, 'site_language', 'en')");

    //$mysqli->query("TRUNCATE TABLE `{$db_prefix}users`");

    $db_admin_pass = md5($db_admin_pass);

    $admin = $mysqli->query("UPDATE `{$db_prefix}users` SET `username` = '$db_admin_user', `password` = '$db_admin_pass', `user_email` = '$db_admin_email' WHERE `user_id` = '1'");
    /*
    $mysqli->query("INSERT INTO `{$db_prefix}users` (`user_id`, `username`, `password`, `user_email`, `user_type`, `user_status`) VALUES
        (1, '$db_admin_user', '$db_admin_pass', '$db_admin_email', 'A', 'A')");
    */
    $smtp = array('smtp' => $smtp);
    $smtp = $mysqli->real_escape_string(json_encode($smtp));
    $mysqli->query("INSERT INTO `{$db_prefix}configs` (`name`, `value`) VALUES
        ('site_configs', '$smtp')");

    $config_contents = $default = file_get_contents($site_path . '/config.install.php');

    if (!empty($config_contents)) {
        if (strstr($config_contents, '{DB_HOST}')) {
            $config_contents = str_replace('{DB_HOST}', $db_host, $config_contents);
        }
        if (strstr($config_contents, '{DB_USER}')) {
            $config_contents = str_replace('{DB_USER}', $db_user, $config_contents);
        }
        if (strstr($config_contents, '{DB_PASS}')) {
            $config_contents = str_replace('{DB_PASS}', $db_pass, $config_contents);
        }
        if (strstr($config_contents, '{DB_NAME}')) {
            $config_contents = str_replace('{DB_NAME}', $db_name, $config_contents);
        }
        if (strstr($config_contents, '{DB_PREFIX}')) {
            $config_contents = str_replace('{DB_PREFIX}', $db_prefix, $config_contents);
        }
    }
    if (!fn_put_contents($site_path . '/config.install.php', $config_contents)) {
        fn_put_contents($site_path . '/config.install.php', $default);
        header('Location: index.php?step=1&error=write_config');
        exit();
    }

    $config_contents = $default = file_get_contents($base_path . '/configs.php');
    if (!empty($config_contents)) {
        if (strstr($config_contents, '{DB_HOST}')) {
            $config_contents = str_replace('{DB_HOST}', $db_host, $config_contents);
        }
        if (strstr($config_contents, '{DB_USER}')) {
            $config_contents = str_replace('{DB_USER}', $db_user, $config_contents);
        }
        if (strstr($config_contents, '{DB_PASS}')) {
            $config_contents = str_replace('{DB_PASS}', $db_pass, $config_contents);
        }
        if (strstr($config_contents, '{DB_NAME}')) {
            $config_contents = str_replace('{DB_NAME}', $db_name, $config_contents);
        }
        if (strstr($config_contents, '{DB_PREFIX}')) {
            $config_contents = str_replace('{DB_PREFIX}', $db_prefix, $config_contents);
        }
    }
    if (!fn_put_contents($base_path . '/configs.php', $config_contents)) {
        fn_put_contents($base_path . '/configs.php', $default);
        header('Location: index.php?step=1&error=write_main_config');
        exit();
    }
}
?><!DOCTYPE html>
<html>
<head>
<title>Install Step 2 - analyze variables</title>
<meta charset="utf-8">
<link rel="stylesheet" href="<?php echo $site_url; ?>/css/style.css" />
</head>
    <body>
        <form  class="form" enctype="multipart/form-data" name="installform" action="<?php echo $site_url; ?>/index.php?step=<?php echo $step+1; ?>" method="post">
            <div class="sep">
                <div><p>Connect Host, Create Database, Import Database Content Success.</p></div>
            </div>
            <div class="sep">
                <input value="Next" type="submit" />
            </div>
        </form>
    </body>
</html>
