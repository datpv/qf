<?php if ( ! defined( 'IAC' ) ) exit; ?><!DOCTYPE html>
<html>
<head>
<title>Install Step 1 - set variables</title>
<meta charset="utf-8">
<link rel="stylesheet" href="<?php echo $site_url; ?>/css/style.css" />
</head>
    <body>
        <form  class="form" enctype="multipart/form-data" name="installform" action="<?php echo $site_url; ?>/index.php?step=<?php echo $step+1; ?>" method="post">
            <?php if(!empty($error)) { ?><div class="sep"><p class="error"><?php echo $error; ?></p></div><?php } ?>
            <div class="sep">
                <div><label for="db_host">DB host: </label><input value="localhost" type="text" name="db_host" id="db_host" /></div>
                <div><label for="db_user">DB user: </label><input value="root" type="text" name="db_user" id="db_user" /></div>
                <div><label for="db_pass">DB password: </label><input type="password" name="db_pass" id="db_pass" /></div>
                <div><label for="db_name">DB name: </label><input type="text" name="db_name" id="db_name" /></div>
                <div><label for="db_prefix">DB new prefix: </label><input type="text" name="db_prefix" id="db_prefix" value="new_prefix_" /></div>
                <div><label for="old_db_prefix">DB old prefix: </label><input type="text" name="old_db_prefix" id="old_db_prefix" value="old_prefix_" /></div>
                
                <div><label for="db_file">Upload file.sql: </label><input type="file" name="db_file" id="db_file" /></div>
                <div><label for="db_file_name">OR User uploaded file.sql on /install/database/: </label>
                    <input type="text" name="db_file_name" id="db_file_name" value="hoctudau-qf.sql" />
                </div>
            </div>
            <div class="sep">
                <div><label for="db_admin_user">Admin username: </label><input type="text" name="db_admin_user" id="db_admin_user" value="admin" /></div>
                <div><label for="db_admin_pass">Admin password: </label><input type="text" name="db_admin_pass" id="db_admin_pass" value="123456" /></div>
                <div><label for="db_admin_email">Admin email: </label><input type="text" name="db_admin_email" id="db_admin_email" value="admin@localhost.com" /></div>
                <div><label for="db_admin_number">Admin phone: </label><input type="text" name="db_admin_number" id="db_admin_number" value="0123456789" /></div>
            </div>
            <div class="sep">
                <div><label for="smtp_host">SMTP host: </label><input value="smtp.gmail.com" type="text" name="smtp_host" id="smtp_host" /></div>
                <div><label for="smtp_port">SMTP port: </label><input value="465" type="text" name="smtp_port" id="smtp_port" /></div>
                <div><label for="smtp_user">SMTP user: </label><input value="" type="text" name="smtp_user" id="smtp_user" /></div>
                <div><label for="smtp_pass">SMTP password: </label><input type="text" name="smtp_pass" id="smtp_pass" /></div>
            </div>
            <div class="sep">
                <input value="Next" type="submit" />
            </div>
        </form>
        <script type="text/javascript">
        window.onload = function() {
            var dbf = document.getElementById("db_file");
            var dbfn = document.getElementById("db_file_name");
            dbfn.onclick = function() {
                dbfn.parentNode.className = "";
                dbf.parentNode.className = "disabled";
            }
            dbf.onclick = function() {
                dbf.parentNode.className = "";
                dbfn.parentNode.className = "disabled";
            }
        }
        </script>
    </body>
</html>
