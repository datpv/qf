<?php
define('IAC', '');
define('DEFAULT_FILE_PERMISSIONS', 0666);
define('DEFAULT_DIR_PERMISSIONS', 0777);
include('config.install.php');
include('core/functions.php');
session_start();
$step = !empty($_GET['step'])?$_GET['step']:'1';
$error = !empty($_GET['error'])?$_GET['error']:'';
if(!empty($error)) $error = getErrorMsg($error);
if(!is_numeric($step) && $step < 0) $step = 1;
$file = $site_path.'/step'.$step.'.php';
if(file_exists($file)) include($site_path.'/step'.$step.'.php');
else exit('file notfound!');