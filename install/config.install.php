<?php
$configs = array();
$configs['db']['db_host'] = 'localhost';
$configs['db']['db_name'] = 'hoctudau';
$configs['db']['db_user'] = 'root';
$configs['db']['db_pass'] = '';
$configs['db']['db_prefix'] = 'qf2_';

$site_path = rtrim(realpath(dirname(__FILE__)),DIRECTORY_SEPARATOR);
$base_path = rtrim(str_replace('install', '', $site_path),DIRECTORY_SEPARATOR);
$site_url = rtrim('http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['SCRIPT_NAME']),'/');