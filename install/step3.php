<?php 
if ( ! defined( 'IAC' ) ) exit; 
$mysqli = new mysqli($configs['db']['db_host'], $configs['db']['db_user'], $configs['db']['db_pass'], $configs['db']['db_name']);
if ($mysqli->connect_errno) {
    header('Location: index.php?step=1&error=mysqli');
    exit();
}
$db_prefix = $configs['db']['db_prefix'];
// alter table
$success = $mysqli->query(
"ALTER TABLE `{$db_prefix}users` ADD `ip` VARCHAR( 46 ) NOT NULL DEFAULT '0.0.0.0',
ADD `cookie` VARCHAR( 32 ) NOT NULL ,
ADD `create` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
ADD `h2d` VARCHAR( 20 ) NOT NULL ,
ADD `skype` VARCHAR( 50 ) NOT NULL ,
ADD `fullname` VARCHAR( 50 ) NOT NULL");
$users = $mysqli->query("SELECT * FROM `{$db_prefix}users`");
$errors = array();
while($user = $users->fetch_assoc()) {
    $user_id = $user['user_id'];
    $user_content = $user['user_content'];
    $user_content = jd2a($user_content);
    if(!$user_content || empty($user_content)) continue;
    $fullname = isset($user_content['Fullname'])?$user_content['Fullname']:'';
    $h2d = isset($user_content['NickH2d'])?$user_content['NickH2d']:'';
    $skype = isset($user_content['NickSkype'])?$user_content['NickSkype']:'';
    $updated = $mysqli->query("UPDATE `{$db_prefix}users` 
        SET `h2d` = '$h2d', `skype` = '$skype', `fullname` = '$fullname'
        WHERE `user_id` = '$user_id'");
    if(!$updated) {
        $errors[$user_id] = $mysqli->error;
    }
}
?><!DOCTYPE html>
<html>
<head>
<title>Install Step 3 - confirm no errors</title>
<meta charset="utf-8">
<link rel="stylesheet" href="<?php echo $site_url; ?>/css/style.css" />
</head>
    <body>
        <form  class="form" enctype="multipart/form-data" name="installform" action="<?php echo $site_url; ?>/index.php?step=<?php echo $step+1; ?>" method="post">
            <div class="sep">
                <div><p>Connect To DB Success.</p></div>
                <div><p><?php if($success) echo 'ALTER FIELDS SUCCESS'; ?></p></div>
                <div><?php 
                if(!empty($errors)) {
                    foreach ($errors as $key => $value) {
                        echo '<p>id: ' . $key . ' - ' . $value . '</p>';
                    }
                }; ?></div>
            </div>
            <div class="sep">
                <input value="Next" type="submit"/>
            </div>
        </form>
    </body>
</html>
