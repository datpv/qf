<?php
    /*
    ***********************************************************************
    *************************************************************************
    
    @From:      
    @Author:    Dat Phan
    @Email:     datphan0110@gmail.com
    @Url:       

    **************************************************************************
    ************************************************************************
    */
    // disallow proxy cache
    header('Cache-Control: no-cache, must-revalidate'); // HTTP/1.1
    header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    define('IA','');
    
    // hide errors
    //error_reporting(0);

    include ('configs.php');
    include ('includes/init.php');

    // init the application
    $initiallizer = new Initiallizer($configs);

    // clear any output
    //if (ob_get_level()) ob_end_clean();

    // session start after the db connection complete
    session_start();

    // run
    $initiallizer->run();
    /*$configs['db']['db_host'] = '{DB_HOST}';
    $configs['db']['db_name'] = '{DB_NAME}';
    $configs['db']['db_user'] = '{DB_USER}';
    $configs['db']['db_pass'] = '{DB_PASS}';
    $configs['db']['db_prefix'] = '{DB_PREFIX}';*/