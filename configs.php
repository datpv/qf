<?php
if ( ! defined( 'IA' ) ) exit;
$configs = array();
$configs['db']['db_host'] = 'localhost';
$configs['db']['db_name'] = 'hoctudau';
$configs['db']['db_user'] = 'root';
$configs['db']['db_pass'] = '';
$configs['db']['db_prefix'] = 'qf2_';
$site_path = realpath(dirname(__FILE__));
define('DB_PREFIX', $configs['db']['db_prefix']);
define('SITE_PATH', $site_path);
define('APPLICATION_PATH', $site_path . DIRECTORY_SEPARATOR . 'applications');
define('INCLUDE_PATH', $site_path . DIRECTORY_SEPARATOR . 'includes');
define('LIB_PATH', INCLUDE_PATH . DIRECTORY_SEPARATOR . 'lib');
define('TEMPLATE_PATH', $site_path . DIRECTORY_SEPARATOR . 'templates');
define('SITE_URL', 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['SCRIPT_NAME']));
define('SITE_LANGUAGE','en');
define('IP', $_SERVER['REMOTE_ADDR']);
include(SITE_PATH.'/configs.local.php');